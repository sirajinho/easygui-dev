package misc;

import com.mycompany.easyGUI.MainActivity;

public class MiscMethods {
	
	public final String[] VIEW_ATTRIBUTES = new String[] { "android:id",
			"android:tag", "android:scrollX", "android:scrollY",
			"android:background", "android:padding",
			"android:paddingLeft", "android:paddingTop",
			"android:paddingRight", "android:paddingBottom",
			"android:paddingStart", "android:paddingEnd",
			"android:focusable", "android:focuasbleInTouchMode", //not suppoted
			"android:visibility", "android:scrollbars",
			"android:scrollbarStyle", "android:isScrollContainer",
			"android:fadeScrollbars",
			"android:scrollbarFadeDuration",
			"android:scrollbarDefaultDelayBeforeFade",
			"android:scrollbarSize",
			"android:scrollbarStyle",
			//not supported
			"android:scrollbarThumbHorizontal",
			"android:scrollbarThumbVertical",
			"android:scrollbarTrackHorizontal",
			"android:scrollbarTrackVertical", 
			"android:scrollbarAlwaysDrawHorizontalTrack",
			"android:scrollbarAlwaysDrawVerticalTrack", 
			"android:requiresFadingEdge", 
			 "android:nextFocusLeft",
			"android:nextFocusRight", "android:nextFocusUp",
			"android:nextFocusDown", "android:nextFocusForward",
			"android:clickable", "android:longClickable",
			"android:saveEnabled",//end
			"android:filterTouchesWhenObscured",
			"android:keepScreenOn",
			"android:minHeight",
			"android:minWidth", "android:soundEffectsEnabled",
			"android:hapticFeedbackEnabled",
			"android:contentDescription", "android:onClick",
			"android:overScrollMode", "android:alpha",
			"android:translationX", "android:translationY",
			"android:transformPivotX", "android:transformPivotY",
			"android:rotation", "android:rotationX",
			"android:rotationY", "android:scaleX",
			"android:scaleY", "android:verticalScrollbarPosition",
			"android:layerType", "android:layoutDirection",
			"android:textDirection", "android:textAlignment",
			"android:importantForAccessibility",
			"android:accessibilityLiveRegion", "android:labelFor", 
			"android:fitSystemsWindows", "android:isScrollContainer" };
	
	public final String[] VIEWGROUP_ATTRIBUTES = { 
			"android:layout_width", "android:layout_height",
			"android:animateLayoutChanges", 
			"android:clipChildren" , "android:clipToPadding", "android:clipToPadding",
			"android:animationCache", "android:persistentDrawingCache", 
			"android:alwaysDrawnWithCache", "android:addStatesFromChildren", 
			"android:descendantFocusability", "android:beforeDescendants",
			"android:afterDescendants", "android:blocksDescendants", 
			"android:splitMotionEvents", "android:layoutMode", 
	 };
	
	public final String[] LINEAR_LAYOUT_ATTRIBUTES = {
			"android:baselineAligned",
			"android:baselineAlignedChildIndex",	 
			"android:divider",	 
			"android:gravity",	 
			"android:measureWithLargestChild", 
			"android:orientation", 
			"android:weightSum"
	};
	
	public final String[] TEXTVIEW_ATTRIBUTES = {
			"android:autoLink", "android:cursorVisible", "android:ems", "android:fontFamily",
			"android:freezesText", "android:height", "android:hint", "android:includeFontPadding",
			"android:inputType","android:inputMethod", "android:lines", "android:maxEms", "android:maxHeight","android:maxLines", 
			"android:maxWidth", "android:minEms", "android:minLines", "android:capitalize", "android:digits",
			"android:numeric", "android:password", "android:phoneNumber",
			"android:scrollHorizontally", "android:selectAllOnFocus", "android:shadowColor",
			"android:shadowDx", "android:shadowDy", "android:shadowRadius", "android:text", 
			"android:textAppearance",
			"android:textAllCaps", "android:textColor", "android:textColorHighlight",
			"android:textColorHint", "android:textColorLink", "android:textScaleX", "android:textSize",
			"android:textStyle", "android:width"
	};
	
	public final String[] LAYOUT_PARAMS = {
			"layout_margin","layout_marginLeft", "android:layout_marginTop",
			"android:layout_marginRight", "android:layout_marginBottom",
			"android:layout_marginStart", "android:layout_marginEnd"
	};
	
	public final String[] RELATIVE_LAYOUT_PARAMS = {
			"android:layout_alignParentLeft",
			"android:layout_alignParentTop",
			"android:layout_alignParentRight",
			"android:layout_alignParentBottom",
			"android:layout_centerVertical",
			"android:layout_alignParentEnd",
			"android:layout_alignParentStart",
			"android:layout_centerInParent",
			"android:layout_centerVertical",
			"android:layout_alignLeft",
			"android:layout_alignRight",
			"android:layout_alignBaseline",
			"android:layout_alignBottom",
			"android:layout_alignEnd",
			"android:layout_alignStart",
			"android:layout_alignTop",
			"android:layout_toEndOf",	 
			"android:layout_toLeftOf",	 
			"android:layout_toRightOf", 
			"android:layout_toStartOf"
	};
	
	public final String[] TABLE_LAYOUT = { 
			"android:collapseColumns", 
			"android:shrinkColumns", 
			"android:stretchColumns",
			"android:orientation"
	};
	
	public final String[] SPINNER_ATTRIBUTES = {
				"android:dropDownHorizontalOffset", 
				"android:dropDownSelector",		 
				"android:dropDownVerticalOffset", 
				"android:dropDownWidth", 
				"android:popupBackground", 
				"android:prompt", 
				"android:spinnerMode"
	};
	
	public final String[] ANALOGCLOCK_ATTRIBUTES = {
			"android:dial",		 
			"android:hand_hour",		 
			"android:hand_minute"
	};
	
	public final String[] PROGRESSBAR_ATTRIBUTES = {
	
			"android:animationResolution",
			"android:indeterminate",		 
			"android:indeterminateBehavior", 
			"android:indeterminateDrawable", 
			"android:indeterminateDuration", 
			"android:indeterminateOnly",	 
			"android:interpolator",		 
			"android:max",		 
			"android:maxHeight", 
			"android:maxWidth", 
			"android:minHeight",		 
			"android:minWidth",		 
			"android:mirrorForRtl", 
			"android:progress",	 
			"android:progressDrawable", 
			"android:secondaryProgress"
	};
	
	public final String[] IMAGEVIEW_ATTRIBUTES =  {
			"android:adjustViewBounds",
			"android:baseline",	 
			"android:baselineAlignBottom", 
			"android:cropToPadding", 
			"android:maxHeight", 
			"android:maxWidth", 
			"android:scaleType", 
			"android:src", 
			"android:tint"
			};
	
	public final String[] DATEPICKER_ATTRIBUTES = { 
			"android:calendarViewShown",	 
			"android:endYear",		 
			"android:maxDate",		 
			"android:minDate",		 
			"android:spinnersShown", 
			"android:startYear"
	};
	
	public final String[] FRAMELAYOUT_ATTRIBUTES = {
			"android:foreground", 
			"android:foregroundGravity", 
			"android:measureAllChildren"
	};
	
	public final String[] SCROLLVIEW_ATTRIBUTES =  {
			"android:fillViewport"
	};
	
	public void MiscMethod() {
		
	}
	

	public String getPathTitle(int pos) {
		int j = pos;
		int z = 0;
		String[] objectPath = new String[20];

	//	MainActivity.xmlmanager.getDocumentAndNodeList();
		int size = MainActivity.xmlmanager.objects.size(); 
			while (j != 0) {
				if(j < MainActivity.xmlmanager.objects.size())
					objectPath[z] = MainActivity.xmlmanager.objects.get(j/*size < j ? j : size -1*/);
				j = MainActivity.xmlmanager.parent.get(j);
				z++;
			}
		//}

		String path = MainActivity.xmlmanager.objects.get(0);

		for (int k = z - 1; k >= 0; k--) {

			path = path + " -> " + objectPath[k];
		}

		return path;
	}

}
