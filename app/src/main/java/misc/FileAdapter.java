package misc;


import java.io.File;
import java.util.List;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mycompany.easyGUI.R;

public class FileAdapter extends ArrayAdapter<String> {

    private final Context context;
    private final List<String> Ids;
    private final int rowResourceId;
    private final List<String> path;

    public FileAdapter(Context context, int textViewResourceId, List<String> objects, List<String> path) {

        super(context, textViewResourceId, objects);

        this.context = context;
        this.Ids = objects;
        this.path = path;
        this.rowResourceId = textViewResourceId;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(rowResourceId, parent, false);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.fileicon);
        TextView textView = (TextView) rowView.findViewById(R.id.rowtext);

        textView.setText(Ids.get(position));
        textView.setPadding(15, 0, 0, 0);
		File file = new File(path.get(position));
		Resources res = context.getResources();
		
		if (file.isDirectory())
			imageView.setImageDrawable(res.getDrawable(R.drawable.folder));
		else
			imageView.setImageDrawable(res.getDrawable(R.drawable.file));
		
		
        return rowView;
    }

}