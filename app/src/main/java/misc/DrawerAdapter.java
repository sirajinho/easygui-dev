package misc;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mycompany.easyGUI.R;

public class DrawerAdapter extends ArrayAdapter<String> {

	private final Context context;
	private final List<String> Files;
	private final int rowResourceId;
	public Boolean newWidget = false;
	public View rowView;
	public static ImageView addButton, removeBtn;

	public DrawerAdapter(Context context, int textViewResourceId,
			List<String> objects) {

		super(context, textViewResourceId, objects);

		this.context = context;
		this.Files = objects;
		this.rowResourceId = textViewResourceId;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		rowView = inflater.inflate(rowResourceId, parent, false);

		TextView FileText = (TextView) rowView
				.findViewById(R.id.DrawerFilename);
		ImageView FileImage = (ImageView) rowView
				.findViewById(R.id.DawerFileImage);

		// FileText.setText(Files.get(position));

		FileText.setText(Files.get(position));
		if (Files.get(position).equals("About")) {
			FileImage.setImageDrawable(context.getResources().getDrawable(
					R.drawable.ic_about));

		} else if (Files.get(position).equals("Help")) {
			FileImage.setImageDrawable(context.getResources().getDrawable(
					R.drawable.ic_help));

		} else if (Files.get(position).equals("Rate easyGUI")) {
			FileImage.setImageDrawable(context.getResources().getDrawable(
					R.drawable.ic_rate));
		} else if (Files.get(position).equals("New File")) {
			FileImage.setImageDrawable(context.getResources().getDrawable(
					R.drawable.ic_newfile));
			LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) FileImage
					.getLayoutParams();
			lp.setMargins(30, 0, 0, 0);
			FileImage.setLayoutParams(lp);
		}

		// FileText.setText(Menu.get(position));

		View divider = rowView.findViewById(R.id.drawerDivider);
		LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) divider
				.getLayoutParams();
		lp.setMargins(0, 20, 0, 0);
		divider.setLayoutParams(lp);

		return rowView;
	}

}