package misc;

import java.util.List;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.mycompany.easyGUI.MainActivity;
import com.mycompany.easyGUI.R;
import com.sirajinhoapp.gui.guiObject;

public class ItemAdapter extends ArrayAdapter<String> {

	private final Context context;
	private final List<String> Ids;
	private final int rowResourceId;
	@SuppressWarnings("unused")
	private int position;
	public Boolean newWidget = false;
	public View rowView;
	public static ImageView addButton, removeBtn;

	public ItemAdapter(Context context, int textViewResourceId,
			List<String> objects) {

		super(context, textViewResourceId, objects);

		this.context = context;
		this.Ids = objects;
		this.rowResourceId = textViewResourceId;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		this.position = position;

		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		rowView = inflater.inflate(rowResourceId, parent, false);
		ImageView imageView = (ImageView) rowView.findViewById(R.id.xmlicon);
		TextView textView = (TextView) rowView.findViewById(R.id.xmltext);
		addButton = (ImageView) rowView.findViewById(R.id.addbutton);
		removeBtn = (ImageView) rowView.findViewById(R.id.removebutton);

		// addView.setLayoutParams(new FrameLayout.LayoutParams(40, 40,
		// Gravity.RIGHT));

		textView.setText(Ids.get(position));

		final Resources res = context.getResources();

		guiObject guiobject = new guiObject(position, MainActivity.xmlmanager);
		
		LayoutParams params = new RelativeLayout.LayoutParams(60, 60);
		int k = 0;
		int pos = position;


		while(MainActivity.xmlmanager.parent.get(pos) != null) {
			k += 16;
			pos = MainActivity.xmlmanager.parent.get(pos);
		} 
		params.setMargins(k, 0, 0, 0);
		
		imageView.setLayoutParams(params);

		if (guiobject.isLayout())
			imageView.setImageDrawable(res.getDrawable(R.drawable.layout));
		else
			imageView.setImageDrawable(res.getDrawable(R.drawable.item));
		
		rowView.setTag(Ids.get(position));

		return rowView;
	}

	public View getView() {
		return rowView;
	}
}