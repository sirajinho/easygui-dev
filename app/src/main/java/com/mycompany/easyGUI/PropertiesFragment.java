package com.mycompany.easyGUI;

import java.util.HashSet;

import misc.MiscMethods;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.sirajinhoapp.xml.xmlManager;

public class PropertiesFragment extends Fragment {
	private xmlManager _xmlmanager;
	private int _objectId;
	private String[] attributes;
	private String[] values;
	View rootView;
	ScrollView scrollView;
	HashSet<String> hashSet;
	TableLayout propertiescontent;
	MiscMethods mm;
	int maxAttr;

	public PropertiesFragment() {

	}

	public void setArgs(xmlManager xmlmanager, int objId) {
		_xmlmanager = xmlmanager;
		_objectId = objId;

	}

	@Override
	public View onCreateView(final LayoutInflater inflater,
			ViewGroup container, Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// setRetainInstance(true);

		// MiscMethods mm = new MiscMethods();

		rootView = inflater.inflate(R.layout.fragment_properties, container,
				false);

		scrollView = (ScrollView) rootView.findViewById(R.id.propertiesScroll);

		propertiescontent = (TableLayout) rootView
				.findViewById(R.id.propertiesContent);

		mm = new MiscMethods();

		maxAttr = 0;
		_xmlmanager = MainActivity.xmlmanager;

		if (_xmlmanager != null) {
			createPropTable();
		}

		return rootView;
	}

	public void createPropTable() {
		if (_xmlmanager.attributes.size() != 0
				&& _xmlmanager.attributes != null) {

			hashSet = new HashSet<String>();

			if (_xmlmanager != null) {
				if (_objectId < _xmlmanager.attributes.size()
						&& _objectId < _xmlmanager.values.size()) {
					attributes = _xmlmanager.attributes.get(_objectId).clone();
					values = _xmlmanager.values.get(_objectId).clone();
				}
			}
			if (attributes != null) {
				for (int i = 0; i < attributes.length; i++) {
					hashSet.add(attributes[i]);
				}
			}
			if (_objectId < _xmlmanager.attributes.size()) {
				if (_xmlmanager.attributes.get(_objectId) != null
						&& !hashSet.isEmpty()) {
					maxAttr = _xmlmanager.attributes.get(_objectId).length;

					// LinearLayout linearLayout = (LinearLayout) rootView
					// .findViewById(R.id.propertiesLinear);

					TextView tv = (TextView) rootView
							.findViewById(R.id.propertiesPath);// new
																// TextView(getActivity());
					// MiscMethods mm = new MiscMethods();
					// tv.setLayoutParams(new
					// LayoutParams(LayoutParams.WRAP_CONTENT,
					// LayoutParams.WRAP_CONTENT));
					tv.setMaxLines(5);
					tv.setTextColor(Color.parseColor("#33b5e5"));
					tv.setTextSize(18);
					tv.setText(mm.getPathTitle(_objectId));

					/*
					 * TableRow trow = new TableRow(getActivity());
					 * trow.setHorizontalScrollBarEnabled(false);
					 * trow.setLayoutParams(new LayoutParams(
					 * LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
					 * // trow.addView(tv); propertiescontent.addView(trow);
					 */

					EditText search = (EditText) rootView
							.findViewById(R.id.searchProp);
					search.setVisibility(EditText.VISIBLE);

					for (int i = 0; i < maxAttr; i++) {

						if (attributes[i] == null)
							break;

						TableRow attrName = new TableRow(getActivity());
						attrName.setHorizontalScrollBarEnabled(false);
						attrName.setLayoutParams(new LayoutParams(
								LayoutParams.MATCH_PARENT,
								LayoutParams.MATCH_PARENT));

						TableRow attrVal = new TableRow(getActivity());
						attrVal.setHorizontalScrollBarEnabled(false);
						attrVal.setLayoutParams(new LayoutParams(
								LayoutParams.MATCH_PARENT,
								LayoutParams.MATCH_PARENT));

						TextView t = new TextView(getActivity());
						t.setText(attributes[i]);
						t.setTextSize(20);
						t.setTextColor(Color.BLACK);
						t.setPadding(12, 12, 12, 12);

						final AutoCompleteTextView editText = new AutoCompleteTextView(getActivity());
						if(attributes[i].contains("layout_width") || attributes[i].contains("layout_height")) {
							String[] layout = { "match_parent", "wrap_content", "fill_parent"};

							ArrayAdapter<String> adapter =
									new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, layout);
							editText.setAdapter(adapter);
						}
						if(attributes[i].contains("orientation")) {
							String[] orient = { "vertical", "horizontal"};

							ArrayAdapter<String> adapter =
									new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, orient);
							editText.setAdapter(adapter);
						}
						if(attributes[i].contains("gravity")) {
							String[] gravity = { "top", "bottom", "left", "right", "center_vertical", "fill_vertical", "center_horizontal", "fill_horizontal", "center", "fill", "clip_vertical", "clip_horizontal", "start", "end"};

							ArrayAdapter<String> adapter =
									new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, gravity);
							editText.setAdapter(adapter);

						}
						//	final EditText editText = new EditText(getActivity());
						// editText.setLayoutParams(new
						// LayoutParams(LayoutParams.MATCH_PARENT,
						// LayoutParams.MATCH_PARENT));
						editText.setBackgroundColor(Color.parseColor("#C5DDE6"));
						editText.setText(values[i]);
						editText.setId(i);
						editText.setTag(i);

						editText.setMaxLines(1);
						// e.setWidth(450);
						editText.setHorizontallyScrolling(true);
						editText.setHorizontalScrollBarEnabled(true);
						editText.setHorizontalFadingEdgeEnabled(true);

						editText.addTextChangedListener(new TextWatcher() {

							public void beforeTextChanged(CharSequence s,
									int start, int count, int after) {
							}

							public void onTextChanged(CharSequence s,
									int start, int before, int count) {
							}

							@Override
							public void afterTextChanged(Editable arg0) {
								// TODO Auto-generated method stub
								try {
									String value = editText.getText()
											.toString();
									int attrId = (Integer) editText.getTag();

									/*
									 * if(attributes[attrId].contains("transform"
									 * ) ||
									 * attributes[attrId].contains("scroll") ||
									 * attributes
									 * [attrId].contains("translation") ||
									 * attributes[attrId].contains("padding") ||
									 * attributes[attrId].contains("min") ||
									 * attributes[attrId].contains("alpha") ||
									 * attributes[attrId].contains("Size") ||
									 * attributes[attrId].contains("rotation")
									 * || attributes[attrId].contains("max") ||
									 * attributes[attrId].contains("Width") ||
									 * attributes[attrId].contains("Height")) {
									 * if(!value.endsWith("dp") ||
									 * !value.endsWith("dip")) value = value +
									 * "dp"; }
									 */
									if (MainActivity.xmlmanager.values
											.get(_objectId) != null) {
										MainActivity.xmlmanager
												.changeNodeAttribute(_objectId,
														attributes[attrId],
														value);/*
																 * editText .
																 * getText ( ) .
																 * toString ( )
																 * ) ;
																 */
										String xmlFile = MainActivity.xmlmanager
												.getXMLInput();
										if (MainActivity.editorfragment != null)
											MainActivity.editorfragment
													.refreshShaderEditor(xmlFile);
										MainActivity.PARSE_ERROR.clear();
										getActivity().invalidateOptionsMenu();
									}
								} catch (NullPointerException edittext) {
									edittext.printStackTrace();
									System.err
											.print("Erroor in aftertextchanged");
								}
							}
						});
					
					attrName.addView(t);
					attrVal.addView(editText);
					propertiescontent.addView(attrName);
					propertiescontent.addView(attrVal);
				}
				
				/*
				 * TableRow tr = new TableRow(getActivity());
				 * tr.setHorizontalScrollBarEnabled(false);
				 * tr.setLayoutParams(new
				 * LayoutParams(LayoutParams.WRAP_CONTENT,
				 * LayoutParams.WRAP_CONTENT)); TextView title = new
				 * TextView(getActivity()); title.setTextSize(24);
				 * title.setText("View Attributes");
				 * title.setGravity(Gravity.CENTER_HORIZONTAL);
				 * 
				 * tr.addView(title); propertiescontent.addView(tr);
				 * 
				 * 
				 * ArrayAdapter<String> adapter = new ArrayAdapter<String>(
				 * getActivity(), android.R.layout.simple_list_item_1,
				 * mm.VIEW_ATTRIBUTES); // adapter.setNotifyOnChange(true);
				 * final AutoCompleteTextView attributeText = new
				 * AutoCompleteTextView( getActivity()); final
				 * AutoCompleteTextView valueText = new AutoCompleteTextView(
				 * getActivity()); tr.addView(attributeText);
				 * tr.addView(valueText); propertiescontent.addView(tr);
				 * 
				 * attributeText.setThreshold(1);
				 * attributeText.setAdapter(adapter);
				 * 
				 * /* attributeText.addTextChangedListener(new TextWatcher(){
				 * 
				 * @Override public void afterTextChanged(Editable arg0) {
				 * 
				 * 
				 * }
				 * 
				 * @Override public void beforeTextChanged(CharSequence arg0,
				 * int arg1, int arg2, int arg3) { // TODO Auto-generated method
				 * stub
				 * 
				 * }
				 * 
				 * @Override public void onTextChanged(CharSequence arg0, int
				 * arg1, int arg2, int arg3) { // TODO Auto-generated method
				 * stub
				 * 
				 * } });
				 */
				/*
				 * attributeText .setOnFocusChangeListener((new
				 * View.OnFocusChangeListener() {
				 * 
				 * @Override public void onFocusChange(View arg0, boolean arg1)
				 * { MainActivity.xmlmanager.changeNodeAttribute( _objectId,
				 * attributeText.getText() .toString(), valueText
				 * .getText().toString());
				 * 
				 * } }));
				 */
				int parentid = 0;
				if (_xmlmanager.parent != null && _objectId != 0)
					parentid = _xmlmanager.parent.get(_objectId);

				addAttributes(mm.LAYOUT_PARAMS, "LayoutParams");

				if (_xmlmanager.objects.get(_objectId).equals("RelativeLayout"))
					addAttributes(mm.RELATIVE_LAYOUT_PARAMS, "RelativeLayout");
				else if (_xmlmanager.objects.get(parentid).equals(
						"RelativeLayout"))
					addAttributes(mm.RELATIVE_LAYOUT_PARAMS, "RelativeLayout");

				if (_xmlmanager.objects.get(_objectId).equals("LinearLayout"))
					addAttributes(mm.LINEAR_LAYOUT_ATTRIBUTES, "LinearLayout");
				else if (_xmlmanager.objects.get(_objectId).equals("TableRow"))
					addAttributes(mm.LINEAR_LAYOUT_ATTRIBUTES, "LinearLayout");
				else if (_xmlmanager.objects.get(_objectId).equals(
						"TableLayout")) {
					addAttributes(mm.LINEAR_LAYOUT_ATTRIBUTES, "LinearLayout");
					addAttributes(mm.TABLE_LAYOUT, "TableLayout");
				}

				addAttributes(mm.VIEW_ATTRIBUTES, "View");

				if (_xmlmanager.objects.get(_objectId).equals("DatePicker"))
					addAttributes(mm.DATEPICKER_ATTRIBUTES, "DatePicker");

				if (_xmlmanager.objects.get(_objectId).equals("ProgressBar"))
					addAttributes(mm.PROGRESSBAR_ATTRIBUTES, "ProgressBar");
				else if (_xmlmanager.objects.get(_objectId).equals("SeekBar"))
					addAttributes(mm.PROGRESSBAR_ATTRIBUTES, "SeekBar");
				else if (_xmlmanager.objects.get(_objectId).equals("RatingBar"))
					addAttributes(mm.PROGRESSBAR_ATTRIBUTES, "RatingBar");

				if (_xmlmanager.objects.get(_objectId).equals("FrameLayout"))
					addAttributes(mm.FRAMELAYOUT_ATTRIBUTES, "FrameLayout");
				else if (_xmlmanager.objects.get(_objectId)
						.equals("TimePicker"))
					addAttributes(mm.FRAMELAYOUT_ATTRIBUTES, "FrameLayout");
				else if (_xmlmanager.objects.get(_objectId)
						.equals("DatePicker"))
					addAttributes(mm.FRAMELAYOUT_ATTRIBUTES, "FrameLayout");
				else if (_xmlmanager.objects.get(_objectId)
						.equals("ScrollView"))
					addAttributes(mm.FRAMELAYOUT_ATTRIBUTES, "FrameLayout");
				else if (_xmlmanager.objects.get(_objectId).equals(
						"HorizontalScrollView"))
					addAttributes(mm.FRAMELAYOUT_ATTRIBUTES, "FrameLayout");

				if (_xmlmanager.objects.get(_objectId).equals("ScrollView"))
					addAttributes(mm.SCROLLVIEW_ATTRIBUTES, "ScrollView");
				else if (_xmlmanager.objects.get(_objectId).equals(
						"HorizontalScrollView"))
					addAttributes(mm.SCROLLVIEW_ATTRIBUTES,
							"HorizontalScrollView");

				if (_xmlmanager.objects.get(_objectId).equals("ImageView"))
					addAttributes(mm.IMAGEVIEW_ATTRIBUTES, "ImageView");
				else if (_xmlmanager.objects.get(_objectId).equals(
						"ImageButton"))
					addAttributes(mm.IMAGEVIEW_ATTRIBUTES, "ImageView");

				if (_xmlmanager.objects.get(_objectId).equals("TextView"))
					addAttributes(mm.TEXTVIEW_ATTRIBUTES, "TextView");
				else if (_xmlmanager.objects.get(_objectId).equals("EditText"))
					addAttributes(mm.TEXTVIEW_ATTRIBUTES, "TextView");
				else if (_xmlmanager.objects.get(_objectId).equals("Button"))
					addAttributes(mm.TEXTVIEW_ATTRIBUTES, "TextView");
				else if (_xmlmanager.objects.get(_objectId).equals("CheckBox"))
					addAttributes(mm.TEXTVIEW_ATTRIBUTES, "TextView");
				else if (_xmlmanager.objects.get(_objectId).equals(
						"RadioButton"))
					addAttributes(mm.TEXTVIEW_ATTRIBUTES, "TextView");
				else if (_xmlmanager.objects.get(_objectId).equals("Switch"))
					addAttributes(mm.TEXTVIEW_ATTRIBUTES, "TextView");
				else if (_xmlmanager.objects.get(_objectId).equals(
						"ToggleButton"))
					addAttributes(mm.TEXTVIEW_ATTRIBUTES, "TextView");

				if (_xmlmanager.objects.get(_objectId).equals("Spinner"))
					addAttributes(mm.SPINNER_ATTRIBUTES, "Spinner");
				else if (_xmlmanager.objects.get(_objectId).equals(
						"AnalogClock"))
					addAttributes(mm.ANALOGCLOCK_ATTRIBUTES, "AnalogClock");

				if (_xmlmanager.objects.get(_objectId).equals("AbsoluteLayout"))
					addAttributes(mm.VIEWGROUP_ATTRIBUTES, "ViewGroup");
				else if (_xmlmanager.objects.get(_objectId).equals(
						"FrameLayout"))
					addAttributes(mm.VIEWGROUP_ATTRIBUTES, "ViewGroup");
				else if (_xmlmanager.objects.get(_objectId).equals(
						"DrawerLayout"))
					addAttributes(mm.VIEWGROUP_ATTRIBUTES, "ViewGroup");
				else if (_xmlmanager.objects.get(_objectId)
						.equals("GridLayout"))
					addAttributes(mm.VIEWGROUP_ATTRIBUTES, "ViewGroup");
				else if (_xmlmanager.objects.get(_objectId).equals(
						"LinearLayout"))
					addAttributes(mm.VIEWGROUP_ATTRIBUTES, "ViewGroup");
				else if (_xmlmanager.objects.get(_objectId).equals(
						"RelativeLayout"))
					addAttributes(mm.VIEWGROUP_ATTRIBUTES, "ViewGroup");
				else if (_xmlmanager.objects.get(_objectId)
						.equals("DatePicker"))
					addAttributes(mm.VIEWGROUP_ATTRIBUTES, "ViewGroup");
				else if (_xmlmanager.objects.get(_objectId).equals(
						"ExpandableListView"))
					addAttributes(mm.VIEWGROUP_ATTRIBUTES, "ViewGroup");
				else if (_xmlmanager.objects.get(_objectId).equals(
						"HorizontalScrollView"))
					addAttributes(mm.VIEWGROUP_ATTRIBUTES, "ViewGroup");
				else if (_xmlmanager.objects.get(_objectId).equals("ListView"))
					addAttributes(mm.VIEWGROUP_ATTRIBUTES, "ViewGroup");
				else if (_xmlmanager.objects.get(_objectId).equals(
						"NumberPicker"))
					addAttributes(mm.VIEWGROUP_ATTRIBUTES, "ViewGroup");
				else if (_xmlmanager.objects.get(_objectId)
						.equals("RadioGroup"))
					addAttributes(mm.VIEWGROUP_ATTRIBUTES, "ViewGroup");
				else if (_xmlmanager.objects.get(_objectId)
						.equals("ScrollView"))
					addAttributes(mm.VIEWGROUP_ATTRIBUTES, "ViewGroup");
				else if (_xmlmanager.objects.get(_objectId).equals(
						"TableLayout"))
					addAttributes(mm.VIEWGROUP_ATTRIBUTES, "ViewGroup");
				else if (_xmlmanager.objects.get(_objectId).equals("TableRow"))
					addAttributes(mm.VIEWGROUP_ATTRIBUTES, "ViewGroup");
				else if (_xmlmanager.objects.get(_objectId)
						.equals("TimePicker"))
					addAttributes(mm.VIEWGROUP_ATTRIBUTES, "ViewGroup");

				search.addTextChangedListener(new TextWatcher() {

					@Override
					public void afterTextChanged(Editable s) {
						TableRow find = (TableRow) propertiescontent
								.findViewWithTag("android:" + s.toString());
						if (find != null)
							scrollView.smoothScrollTo(0, (int) find.getY() + 15);

					}

					@Override
					public void beforeTextChanged(CharSequence s, int start,
							int count, int after) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onTextChanged(CharSequence s, int start,
							int before, int count) {
						// TODO Auto-generated method stub

					}
				});
			}
			}
		}
	}

	public void addAttributes(final String[] attributes,
			final String titleParent) {
		TableLayout tableTitle = new TableLayout(getActivity());
		tableTitle.setLayoutParams(new TableLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

		TableRow tr = new TableRow(getActivity());
		tr.setHorizontalScrollBarEnabled(false);
		tr.setLayoutParams(new TableRow.LayoutParams(
				TableRow.LayoutParams.WRAP_CONTENT,
				TableRow.LayoutParams.WRAP_CONTENT));
		// tr.setOrientation(TableRow.HORIZONTAL);

		TextView title = new TextView(getActivity());

		title.setPadding(20, 10, 20, 10);
		title.setTextSize(24);
		title.setText(titleParent);
		title.setTextColor(Color.parseColor("#33b5e5"));

		title.setLayoutParams(new TableRow.LayoutParams(
				TableRow.LayoutParams.WRAP_CONTENT,
				TableRow.LayoutParams.WRAP_CONTENT));
		final ImageView webLink = new ImageView(getActivity());
		webLink.setImageDrawable(getResources().getDrawable(
				R.drawable.ic_action_web_site));

		webLink.setLayoutParams(new TableRow.LayoutParams(
				TableRow.LayoutParams.WRAP_CONTENT,
				TableRow.LayoutParams.WRAP_CONTENT));
		webLink.setPadding(10, 10, 20, 10);
		webLink.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				// TODO Auto-generated method stub

				if (arg1.getAction() == MotionEvent.ACTION_DOWN) {
					webLink.setAlpha((float) 0.3);
					return true;
				} else if (arg1.getAction() == MotionEvent.ACTION_UP) {
					webLink.setAlpha((float) 1.0);
					String url = "http://developer.android.com/develop/index.html#q="
							+ titleParent;
					Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri
							.parse(url));
					startActivity(browserIntent);
					return true;
				} else if (arg1.getAction() == MotionEvent.ACTION_CANCEL) {
					webLink.setAlpha((float) 1.0);
					return true;
				}
				return false;
			}

		});

		tr.addView(title);
		tr.addView(webLink);
		tableTitle.addView(tr);
		propertiescontent.addView(tableTitle);

		for (int i = 0; i < attributes.length; i++) {
			// final int id = i;
			if (!ArrayContains(attributes[i])) {
				TableRow attrName = new TableRow(getActivity());
				attrName.setHorizontalScrollBarEnabled(false);
				attrName.setLayoutParams(new LayoutParams(
						LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

				TableRow attrVal = new TableRow(getActivity());
				attrVal.setHorizontalScrollBarEnabled(false);
				attrVal.setLayoutParams(new LayoutParams(
						LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
				attrVal.setTag(attributes[i]);

				TextView t = new TextView(getActivity());
				t.setText(attributes[i]);
				t.setTextSize(20);
				t.setTextColor(Color.BLACK);
				t.setPadding(12, 12, 12, 12);

				final AutoCompleteTextView editText = new AutoCompleteTextView(getActivity());
				if(attributes[i].contains("gravity")) {
					String[] gravity = { "top", "bottom", "left", "right", "center_vertical", "fill_vertical", "center_horizontal", "fill_horizontal", "center", "fill", "clip_vertical", "clip_horizontal", "start", "end"};

					ArrayAdapter<String> adapter =
							new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, gravity);
					editText.setAdapter(adapter);

				}
				// LayoutParams(LayoutParams.MATCH_PARENT,
				// LayoutParams.MATCH_PARENT));
				editText.setBackgroundColor(Color.parseColor("#C5DDE6"));
				// editText.setText(values[i]);
				// editText.setId(i);

				editText.setMaxLines(1);
				editText.setTag(i);

				editText.addTextChangedListener(new TextWatcher() {

					public void beforeTextChanged(CharSequence s, int start,
							int count, int after) {
					}

					public void onTextChanged(CharSequence s, int start,
							int before, int count) {
					}

					@Override
					public void afterTextChanged(Editable arg0) {
						try {
							String value = arg0.toString();
							int attrId = (Integer) editText.getTag();

							/*
							 * if(attributes[attrId].contains("X") ||
							 * attributes[attrId].contains("Y") ||
							 * attributes[attrId].contains("padding") ||
							 * attributes[attrId].contains("min") ||
							 * attributes[attrId].contains("alpha") ||
							 * attributes[attrId].contains("Size") ||
							 * attributes[attrId].contains("rotation") ||
							 * attributes[attrId].contains("max") ||
							 * attributes[attrId].contains("Width") ||
							 * attributes[attrId].contains("Height")) {
							 * if(!value.endsWith("dp") ||
							 * !value.endsWith("dip")) value = value + "dp"; }
							 */
							if (MainActivity.xmlmanager.values.get(_objectId) != null) {
								MainActivity.xmlmanager
										.changeNodeAttribute(_objectId,
												attributes[attrId], value/*
																		 * editText.
																		 * getText
																		 * ().
																		 * toString
																		 * ()
																		 */);
								String xmlFile = MainActivity.xmlmanager
										.getXMLInput();
								if (MainActivity.editorfragment != null)
									MainActivity.editorfragment
											.refreshShaderEditor(xmlFile);
								MainActivity.PARSE_ERROR.clear();
								getActivity().invalidateOptionsMenu();
							}
						} catch (NullPointerException edittext) {
							edittext.printStackTrace();
							System.err.print("Erroor in aftertextchanged");
						}
					}
				});

				editText.setOnFocusChangeListener(new OnFocusChangeListener() {

					@Override
					public void onFocusChange(View arg0, boolean arg1) {
						// TODO Auto-generated method stub
						/*
						 * String value = ((EditText)arg0).getText().toString();
						 * if((!value.endsWith("dp") || !value.endsWith("dip"))
						 * && !value.isEmpty()) {
						 * ((EditText)arg0).setText(value+"dp"); }
						 */
					}

				});

				attrName.addView(t);
				attrVal.addView(editText);

				propertiescontent.addView(attrName);
				propertiescontent.addView(attrVal);
			}
		}
	}

	public Boolean ArrayContains(String s) {
		Boolean contains = false;
		/*
		 * for(int i = 0; i < array.length; i++) { if(array[i].equals(s))
		 * contains = true; }
		 */
		if (hashSet != null) {
			if (hashSet.contains(s))
				contains = true;
		}
		return contains;
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		/*
		 * for(int i=0;i <_xmlmanager.attributes.get(_objectId).length;i++) {
		 * EditText et = (EditText) rootView.findViewById(i);
		 * MainActivity.xmlmanager.values.get(_objectId)[i] =
		 * et.getText().toString(); }
		 */
	}

}
