package com.mycompany.easyGUI;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import misc.FileAdapter;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

public class FileFragment extends Fragment {

	private List<String> item = null;
	private List<String> path = null;
	private String root;
	private View rootView;
	private ListView listView;
	private TextView myPath;

	// private TextView myPath;

	OnHeadlineSelectedListener mCallback;

	// Container Activity must implement this interface
	public interface OnHeadlineSelectedListener {
		public void onFileSelected(String Filename);


	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception
		try {
			mCallback = (OnHeadlineSelectedListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnHeadlineSelectedListener");
		}
	}

	public static FileFragment newInstance() {
		return new FileFragment();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	//	setRetainInstance(true);

		root = Environment.getExternalStorageDirectory().getPath();

		rootView = inflater.inflate(R.layout.fragment_file, container, false);

		listView = (ListView) rootView.findViewById(R.id.filelist);
		myPath = (TextView) rootView.findViewById(R.id.filepath);

		getDir(root);

		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				final File file = new File(path.get(position));
		
				if (file.isDirectory()) {

					if (file.canRead()) {
						getDir(path.get(position));
					} else {
						new AlertDialog.Builder(getActivity())
								.setIcon(R.drawable.ic_launcher)
								.setTitle(
										"[" + file.getName()
												+ "] folder can't be read!")
								.setPositiveButton("OK", null).show();

					}
				} else {
					AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
						ProgressDialog pd = new ProgressDialog(getActivity());
				
						// String filePath = "";
						@Override
						protected void onPreExecute() {
							pd.setTitle("Loading File...");
							pd.setMessage("Please wait.");
							pd.setCancelable(false);
							pd.setIndeterminate(true);
							pd.show();
						}

						@Override
						protected Void doInBackground(Void... arg0) {
							try {
								Thread.sleep(100);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							return null;
						}

						@Override
						protected void onPostExecute(Void result) {
							mCallback.onFileSelected(file.getPath());
							if (pd != null) {
								pd.dismiss();
							}
						}

					};
					task.execute((Void[]) null);
		
				
				}
			};
		});

		return rootView;

	}

	private void getDir(String dirPath) {
		myPath.setText(dirPath);

		item = new ArrayList<String>();
		path = new ArrayList<String>();
		File f = new File(dirPath);
		File[] files = f.listFiles();

		if (!dirPath.equals(root)) {
			item.add(root);
			path.add(root);
			item.add("../");
			path.add(f.getParent());
		}

		for (int i = 0; i < files.length; i++) {
			File file = files[i];

			if (!file.isHidden() && file.canRead()) {
				if (file.isDirectory()) {
					path.add(file.getPath());
					item.add(file.getName() + "/");
				} else if (file.getName().contains(".xml")) {
					path.add(file.getPath());
					item.add(file.getName());
				}
			}
		}
		FileAdapter fileList = new FileAdapter(getActivity(), R.layout.row,
				item, path);
		listView.setAdapter(fileList);
	}

}