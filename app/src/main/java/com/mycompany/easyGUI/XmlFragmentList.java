package com.mycompany.easyGUI;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import misc.ItemAdapter;
import misc.MiscMethods;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.sirajinhoapp.xml.xmlManager;

@SuppressLint("ValidFragment")
public class XmlFragmentList extends ListFragment {
	private xmlManager _xmlmanager;
	private int _position = 0;
	private int pos = 0;
	OnHeadlineSelectedListener mCallback;
	private ItemAdapter adapter;
	private ListView listView;
	public Button addRoot;

	public XmlFragmentList() {

	}

	// Container Activity must implement this interface
	public interface OnHeadlineSelectedListener {
		public void onArticleSelected(int position);

	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception
		try {
			mCallback = (OnHeadlineSelectedListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnHeadlineSelectedListener");
		}
	}

	@Override
	public void onStop() {
		super.onStop();
		// Toast.makeText(getActivity(), "stop()", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onStart() {
		if (MainActivity.xmlmanager != null &&
				MainActivity.xmlmanager.objects != null) {
			if (MainActivity.xmlmanager.objects.size() == 0) {
				if (this.getListView().getHeaderViewsCount() == 0)
					setAddRootButton();
			}
		}
		super.onStart();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	//	setRetainInstance(true);

		_xmlmanager = MainActivity.xmlmanager;
		if (_xmlmanager != null)
			adapter = new ItemAdapter(getActivity(), R.layout.row_xmlitems,
					getXmlObjects(MainActivity.xmlmanager.objects));

		/** Setting the list adapter for the ListFragment */

		setListAdapter(adapter);

		/*
		 * ImageView imageView = adapter.addView;
		 * 
		 * imageView.setOnTouchListener(new OnTouchListener () {
		 * 
		 * @SuppressLint("NewApi")
		 * 
		 * @Override public boolean onTouch(View view, MotionEvent event) { if
		 * (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
		 * Resources res = getActivity().getResources(); //
		 * popUpAddItemDialog(); Toast.makeText(getActivity(),
		 * "touzch addtimte", Toast.LENGTH_SHORT).show();
		 * 
		 * } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
		 * } return true; } });
		 */
	}

	@Override
	public void onActivityCreated(Bundle savedState) {
		super.onActivityCreated(savedState);

		listView = getListView();

		if (listView != null) {
			listView.setOnItemLongClickListener(new OnItemLongClickListener() {

				@Override
				public boolean onItemLongClick(AdapterView<?> arg0, View view,
						int arg2, long arg3) {
					popUpSurroundDialog(arg2);

					return true;
				}
			});
		}

	}

	public ArrayList<String> getXmlObjects(ArrayList<String> objects) {
		ArrayList<String> objectList = new ArrayList<String>();

		if (objects != null) {
			for (int i = 0; i < objects.size(); i++) {
				objectList.add(objects.get(i));
			}
		}
		return objectList;
	}

	public int getItemPositon() {
		return _position;
	}

	@Override
	public void onListItemClick(ListView l, View v, final int position, long id) {
		// TODO Auto-generated method stub
		// v.setClickable(true);
		// v.setFocusable(true);

		final ImageView addButton = (ImageView) v.findViewById(R.id.addbutton);
		final ImageView removeBtn = (ImageView) v
				.findViewById(R.id.removebutton);
		final ImageView xmlImage = (ImageView) v.findViewById(R.id.xmlicon);
		final TextView xmlText = (TextView) v.findViewById(R.id.xmltext);

		OnTouchListener touchListener = new OnTouchListener() {
			@SuppressLint("NewApi")
			@Override
			public boolean onTouch(View view, MotionEvent event) {
				Resources res = getActivity().getResources();
				switch (event.getAction()) {
				case (android.view.MotionEvent.ACTION_DOWN):
					if (view.equals(removeBtn)) {

						removeBtn.setImageDrawable(res
								.getDrawable(R.drawable.remove_red));

						return true;
					} else if (view.equals(addButton)) {
						addButton.setImageDrawable(res
								.getDrawable(R.drawable.add_red));
						// Toast.makeText(getActivity(), "Pos.: "+pos,
						// Toast.LENGTH_SHORT).show();
						return true;
					}

				case (android.view.MotionEvent.ACTION_UP):
					if (view.equals(removeBtn)) {
						removeBtn.setImageDrawable(res
								.getDrawable(R.drawable.remove));
						// removeBtn.setAlpha((float) 1.0);
						MainActivity.xmlmanager.removeObject(position);
						Toast.makeText(getActivity(), "removed!",
								Toast.LENGTH_SHORT).show();
						MainActivity.xmlmanager.saveNodeModification(MainActivity.xmlmanager.tmpFilename);
						setAddRootButton();
						updateAdapter(getXmlObjects(MainActivity.xmlmanager.objects));
						MainActivity.xmlmanager.getDocumentAndNodeList();
						
						String xmlFile = MainActivity.xmlmanager.getXMLInput();
						((EditorFragment)getActivity().getSupportFragmentManager().findFragmentById(R.id.editorlayout))
								.refreshShaderEditor(xmlFile);
						MainActivity.reloadPropertieFragment(true); 
						
						((GuiFragment)getActivity().getSupportFragmentManager().findFragmentById(R.id.rootLayoutGui)).refreshLayout();
						//MainActivity.xmlmanager.getDocumentAndNodeList();
					
						return true;

					} else if (view.equals(addButton)) {
						addButton.setImageDrawable(res
								.getDrawable(R.drawable.add));
						final int pos = position;
						popUpAddItemDialog(pos);
					}
					return true;
				case (MotionEvent.ACTION_CANCEL):
					if (view.equals(removeBtn)) {
						removeBtn.setImageDrawable(res
								.getDrawable(R.drawable.remove));
						return true;

					} else if (view.equals(addButton))
						addButton.setImageDrawable(res
								.getDrawable(R.drawable.add));
				default:
					break;
				}
				return false;
			}
		}; 
		addButton.setClickable(true);
		removeBtn.setClickable(true);
		// addButton.setFocusableInTouchMode(true);
		// removeBtn.setFocusableInTouchMode(true);

		addButton.setOnTouchListener(touchListener);
		removeBtn.setOnTouchListener(touchListener);

		addButton.setFocusable(false);
		removeBtn.setFocusable(false);
		xmlImage.setFocusable(false);
		xmlText.setFocusable(false);

		mCallback.onArticleSelected(position);

	}

	public void removeAddRootButton() {
		if (this.getListView() != null && MainActivity.FileLoaded()) {
			this.getListView().removeHeaderView(addRoot);
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		// Toast.makeText(getActivity(), "onResume", Toast.LENGTH_SHORT).show();

		adapter = new ItemAdapter(getActivity(), R.layout.row_xmlitems,
				getXmlObjects(MainActivity.xmlmanager.objects));

	//	setListAdapter(null);
		setListAdapter(adapter);
	}

	public void setAddRootButton() {
		if (this.getListView() != null && MainActivity.FileLoaded()
				&& MainActivity.xmlmanager.objects.size() == 0) {
			addRoot = new Button(getActivity());
			addRoot.setText("add Root Element");
			addRoot.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					popUpAddRootDialog();
				}

			});
			setListAdapter(null);
			this.getListView().addHeaderView(addRoot);
	/*		
			FragmentManager fragmentManager = getFragmentManager();
			FragmentTransaction fragmentTransaction = fragmentManager
						.beginTransaction();

			GuiFragment guiFragment = fragmentTransaction. 

				fragmentTransaction.replace(R.id.xmlelements, xmlfragment, "XML");
				
				fragmentTransaction.commitAllowingStateLoss();*/
		}
	}

	public void updateAdapter(List<String> listItems) {
		adapter = new ItemAdapter(getActivity(), R.layout.row_xmlitems,
				listItems);

	//	if (MainActivity.xmlmanager.objects.size() == 0)
	//		setAddRootButton();
		setListAdapter(adapter);
	}

	public void popUpSurroundDialog(final int position) {
		final Dialog dialog = new Dialog(getActivity());
		dialog.setContentView(R.layout.dialog_surround);

		MiscMethods mm = new MiscMethods();
		String path = mm.getPathTitle(position);

		((TextView) dialog.findViewById(android.R.id.title)).setTextSize(
				TypedValue.COMPLEX_UNIT_SP, 15);
		((TextView) dialog.findViewById(android.R.id.title)).setMaxLines(6);
		dialog.setTitle("Surround " + path + " with");
		dialog.show();

		final String[] elements = { "RelativeLayout",
				"LinearLayout (horizontal)", "LinearLayout (vertical)",
				"GridLayout", "FrameLayout", "RadioGroup", "TableLayout",
				"TableRow", "AbsoluteLayout", "ScrollView",
				"HorizontalScrollView" };
		ArrayList<String> listElements = new ArrayList<String>();
		for (int i = 0; i <= elements.length - 1; i++) {
			listElements.add(elements[i]);
		}

		ArrayAdapter<String> listAdapter = new ArrayAdapter<String>(
				getActivity(), android.R.layout.simple_list_item_activated_1,
				listElements);
		final ListView listViewElements = (ListView) dialog
				.findViewById(R.id.surroundlistView);
		listViewElements.setAdapter(listAdapter);
		listViewElements.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

		listViewElements.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				pos = arg2;
				final String layout = elements[pos];

				MainActivity.xmlmanager.surroundElement(position, layout);

				updateAdapter(getXmlObjects(MainActivity.xmlmanager.objects));
				String xmlFile = MainActivity.xmlmanager.getXMLInput();
				MainActivity.editorfragment.refreshShaderEditor(xmlFile);
				MainActivity.PARSE_ERROR.clear();
				getActivity().invalidateOptionsMenu();
				dialog.dismiss();
			}

		});

	}

	public void popUpAddRootDialog() {
		final Dialog dialog = new Dialog(getActivity());
		dialog.setContentView(R.layout.dialog_addrootitem);

		dialog.setTitle("Add Root Element");
		dialog.show();

		final String[] elements = { "ListView", "Spinner", "ExpandableListView",
				"RelativeLayout",
				"LinearLayout (horizontal)", "LinearLayout (vertical)",
				"GridLayout", "FrameLayout", "RadioGroup", "TableLayout",
				"TableRow", "AbsoluteLayout", "ScrollView",
				"HorizontalScrollView", "DrawerLayout", "ViewPager" };
		ArrayList<String> listElements = new ArrayList<String>();
		for (int i = 0; i <= elements.length - 1; i++) {
			listElements.add(elements[i]);
		}

		ArrayAdapter<String> listAdapter = new ArrayAdapter<String>(
				getActivity(), android.R.layout.simple_list_item_activated_1,
				listElements);
		final ListView listViewElements = (ListView) dialog
				.findViewById(R.id.rootitemlistView);
		listViewElements.setAdapter(listAdapter);
		listViewElements.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

		listViewElements.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
					long arg3) {
				// TODO Auto-generated method stub
				removeAddRootButton();
				final String layout = elements[pos];
				MainActivity.xmlmanager.addNode(-1, layout);
				MainActivity.xmlmanager.saveNodeModification(MainActivity.xmlmanager.tmpFilename);
				
			//	MainActivity.xmlmanager.getDocumentAndNodeList();
					
				MainActivity.reloadEditor(MainActivity.xmlmanager.getXMLInput());
				
				//MainActivity.reloadGUIFragment();
			//	MainActivity.reloadPropertieFragment(true);
			//	MainActivity.reloadXmlFragment();
				updateAdapter(getXmlObjects(MainActivity.xmlmanager.objects));
				
				
				//String xmlFile = MainActivity.xmlmanager.getXMLInput();
				//MainActivity.editorfragment.refreshShaderEditor(xmlFile);
			//	MainActivity.xmlmanager.getDocumentAndNodeList();
				MainActivity.PARSE_ERROR.clear();
				
				
				getActivity().invalidateOptionsMenu();

				dialog.dismiss();
			}

		});

	}

	public void popUpAddItemDialog(final int position) {
		final Dialog dialog = new Dialog(getActivity());
		dialog.setContentView(R.layout.dialog_additem);

		MainActivity.xmlmanager.objects.get(position);
		MiscMethods mm = new MiscMethods();
		String path = mm.getPathTitle(position);
		((TextView) dialog.findViewById(android.R.id.title)).setTextSize(
				TypedValue.COMPLEX_UNIT_SP, 15);
		((TextView) dialog.findViewById(android.R.id.title)).setMaxLines(6);
		dialog.setTitle(path);
		dialog.show();

		final String[] elements = { "Button", "Small Button", "BarButton", "ImageView",
				"ToggleButton", "CheckBox", "RadioButton", "SeekBar", "Switch",
				"ImageButton", "Text", "SmallText", "MediumText", "LargeText",
				"ProgressBar", "EditText", "DatePicker", "TimerPicker",
				"AnalogClock", "RatingBar", "NumberPicker", "ListView",
				"ExpandableListView", "Spinner", "RelativeLayout",
				"LinearLayout (horizontal)", "LinearLayout (vertical)",
				"ButtonBar", "GridLayout", "FrameLayout", "RadioGroup", "Button Bar",
				"TableLayout", "TableRow", "AbsoluteLayout", "ScrollView",
				"HorizontalScrollView", "PagerTitleStrip", "ViewPager" };
		ArrayList<String> listElements = new ArrayList<String>();
		for (int i = 0; i <= elements.length - 1; i++) {
			listElements.add(elements[i]);
		}

		EditText elementSearch = (EditText) dialog
				.findViewById(R.id.elementsearch);
		final ListView listViewElements = (ListView) dialog
				.findViewById(R.id.elements);

		ArrayAdapter<String> listAdapter = new ArrayAdapter<String>(
				getActivity(), android.R.layout.simple_list_item_activated_1,
				listElements);

		listViewElements.setAdapter(listAdapter);
		listViewElements.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

		listViewElements.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				pos = arg2;
				final String widget = elements[pos];
		//		Toast.makeText(
		//				getActivity(),
		//				MainActivity.xmlmanager.parent.get(position)
		//						+ " = Parent", Toast.LENGTH_SHORT).show();
				MainActivity.xmlmanager.addNode(position, widget);
				MainActivity.xmlmanager.saveNodeModification(MainActivity.xmlmanager.tmpFilename);
				updateAdapter(getXmlObjects(MainActivity.xmlmanager.objects));
				String xmlFile = MainActivity.xmlmanager.getXMLInput();
				MainActivity.reloadEditor(MainActivity.xmlmanager.getXMLInput());
					MainActivity.PARSE_ERROR.clear();
				((GuiFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.rootLayoutGui)).refreshLayout();
				getActivity().invalidateOptionsMenu();
				dialog.dismiss();
			}

		});

		elementSearch.addTextChangedListener(new TextWatcher() {

			@Override
			public void afterTextChanged(Editable arg0) {
				// final int pos = 0;
				for (int i = 0; i <= elements.length - 1; i++) {
					if (elements[i].toLowerCase(Locale.getDefault()).contains(
							arg0)
							|| elements[i].toLowerCase(Locale.getDefault())
									.equals(arg0)) {
						listViewElements.smoothScrollToPositionFromTop(i, 80);
						listViewElements.setItemChecked(i, true);
						// listViewElements.setItemChecked(i, true);
						break;
					}
				}

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub

			}
		});

	}

}
