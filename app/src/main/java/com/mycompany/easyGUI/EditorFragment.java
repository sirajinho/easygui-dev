package com.mycompany.easyGUI;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.sirajinhoapp.shader.ShaderEditor;

public class EditorFragment extends Fragment  {

	private View rootView;
	private ShaderEditor shadereditor;
	private Boolean visibility = false;


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (container == null)
			return null;
	//	setRetainInstance(true);
		

		rootView = inflater.inflate(R.layout.fragment_editordesign, container,
				false);

		shadereditor = (ShaderEditor) rootView.findViewById(R.id.shaderEditor1);
		shadereditor.setHorizontallyScrolling(false);
		shadereditor.setMovementMethod(new ScrollingMovementMethod());
/*
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_list_item_1, ATTRIBUTES);
		adapter.setNotifyOnChange(true);
		shadereditor.setThreshold(1);
		shadereditor.setAdapter(adapter);
		shadereditor.addTextChangedListener(this);
		shadereditor.showDropDown(); */
		shadereditor.setTextIsSelectable(true);
	//	shadereditor.clearFocus();
		shadereditor.setFocusableInTouchMode(true);
		shadereditor.setFocusable(true);
		
		if(((MainActivity)getActivity()).product1Available && ((MainActivity)getActivity()).product2Available && ((MainActivity)getActivity()).product3Available) {
			shadereditor.setEnabled(false);
		}
		
		shadereditor.addTextChangedListener(new TextWatcher() {

			@Override
			public void afterTextChanged(Editable text) {
				
				if(MainActivity.isFileLoaded(MainActivity.xmlmanager.Filename)
				){		//&& visibility == true) {
					
					MainActivity.xmlmanager.saveFile(MainActivity.xmlmanager.tmpFilename, text.toString());
					//Toast.makeText(getActivity(), MainActivity.xmlmanager.tmpFilename + text.toString(), Toast.LENGTH_LONG).show();
					//MainActivity.xmlmanager.saveNodeModification(MainActivity.xmlmanager.tmpFilename);
				
					MainActivity.xmlmanager.getDocumentAndNodeList();
				//	MainActivity.reloadGUIFragment();
				//	MainActivity.reloadPropertieFragment(true);
				//	MainActivity.reloadXmlFragment();
				
					MainActivity.textChanged = true;
					
					
					
			//		getActivity().invalidateOptionsMenu();
				//	

				}

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub
				
			} 
			
		});
				
		shadereditor.clearFocus();
		shadereditor.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				 InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
				 imm.showSoftInput(shadereditor, InputMethodManager.SHOW_IMPLICIT);
			} 
			
		});
		
		shadereditor.setOnFocusChangeListener( new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View arg0, boolean arg1) {
				// TODO Auto-generated method stub
				if(MainActivity.isFileLoaded(MainActivity.xmlmanager.Filename)) {
				// TO DO
					if(MainActivity.textChanged == true) {
						//MainActivity.xmlmanager.saveNodeModification(MainActivity.xmlmanager.tmpFilename);
						//MainActivity.xmlmanager.getDocumentAndNodeList();
					//	Toast.makeText(getActivity(), MainActivity.xmlmanager.getXMLInput(), Toast.LENGTH_LONG).show();
						MainActivity.reloadXmlFragment();
						MainActivity.reloadPropertieFragment(false);
						((GuiFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.rootLayoutGui)).refreshLayout();
						
						MainActivity.textChanged = false;
					
					}
		
				}				
			} }); 
		
		
	//	shadereditor.setText("Hallo");

		return rootView;
	}
	
	@Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible) {
           visibility = true;
        } else
        	visibility = false;
    }
	
	public void disableEditor() {
		shadereditor.setEnabled(false);
	}
	
	public void enableEditor() {
		shadereditor.setEnabled(true);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}
	
	 @Override
	  public void onResume() {
		 super.onResume();
		 String xmlFile = MainActivity.xmlmanager.getXMLInput();
		 refreshShaderEditor(xmlFile);
		 MainActivity.PARSE_ERROR.clear();
		 getActivity().invalidateOptionsMenu();
	//	 Toast.makeText(getActivity(), "onresume editor", Toast.LENGTH_LONG).show();	
	  }

	public void loadFile(String Filename) throws IOException {

		String strLine[] = new String[500];
		String text = "";
		shadereditor.setText("");
		try {
			int i = 0;
			FileInputStream fstream = new FileInputStream(Filename);
			// Get the object of DataInputStream
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			// Read File Line By Line
			while ((strLine[i] = br.readLine()) != null) {
				text = text + "\n" + strLine[i];
				i++;
			}
			in.close();

			
		} catch (Exception file) {// Catch exception if any
			System.err.println("Error: " + file.getMessage());
		}
		
	
	
		shadereditor.setText(text);
		shadereditor.setTextHighlighted(shadereditor.getText());

	}
	

	public void refreshShaderEditor(String text) {
	/*	if(text.contains("<?xml")) {
			int start = text.indexOf("<?xml");
			int end = 0;
			for(int j = start; j < text.length(); j++) {
				if(text.charAt(j) == '>') {
					end = j+1;
					break;
				}
			}
			text = text.replace(text.substring(start, end), "");
		}*/
		text.replaceAll("\\p{C}", "");
		text.replace("\\t", "");
		shadereditor.setText(text);
		shadereditor.setTextHighlighted(shadereditor.getText());
		if(((MainActivity)getActivity()).product1Available && ((MainActivity)getActivity()).product2Available && ((MainActivity)getActivity()).product1Available) {
		} else {
			shadereditor.setEnabled(true);

		}
	}
	
	public String getEditorContent() {
		return shadereditor.getText().toString();
	}

}
