package com.mycompany.easyGUI;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import misc.MiscMethods;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.DragShadowBuilder;
import android.view.View.OnClickListener;
import android.view.View.OnDragListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleExpandableListAdapter;
import android.widget.Space;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.Toast;

import com.sirajinhoapp.gui.guiObject;
import com.sirajinhoapp.gui.guiObject.LayoutTyp;
import com.sirajinhoapp.xml.xmlManager;

@SuppressLint("UseSparseArrays")
public class GuiFragment extends Fragment {
	ViewGroup layout;
	static ViewGroup[] viewLayouts;
	static View[] views;
	LayoutParams layoutParams;
	LinearLayout ll;
	Boolean newLayout = false;
	TableLayout tl;
	TableRow[] tr;
	LayoutTyp ltyp;
	Boolean newRow = false;
	int lays = 0;

	static Map<Integer, String> objectsids = new HashMap<Integer, String>();
	Map<Integer, ViewGroup> Layouts = new HashMap<Integer, ViewGroup>();
	static guiObject[] guiobjects;
	HashMap<Integer, NodeList> childList = new HashMap<Integer, NodeList>();
	
	private Boolean guiFragmentLoaded = false;

	View rootView;
	xmlManager _xmlmanager;
	Boolean borderMode = false;

	private int emptyWidth = 60, emptyHeight = 60;
	private static float min_x = 1000;
	private static float min_y = 1000;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	//	setRetainInstance(true);

		_xmlmanager = MainActivity.xmlmanager;

		rootView = inflater.inflate(R.layout.fragment_guidesign, container,
				false);

		layout = (LinearLayout) rootView.findViewById(R.id.rootLayoutdesign);
		layout.setOnDragListener(new MyDragListener());

	/*	LinearLayout guiElements = (LinearLayout) rootView
				.findViewById(R.id.guielements);
		for (int i = 0; i <= guiElements.getChildCount() - 1; i++) {
			View v = guiElements.getChildAt(i);
			v.setOnTouchListener(new MyTouchListener());
		}*/

		drawGUI();
		
		guiFragmentLoaded = true;

		return rootView;

	}

	public void drawGUI() {
		if(layout != null) {
			layout.removeAllViewsInLayout();
		}
		iterativAdd(layout, 0);
	}
	
	@Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible) {
           refreshLayout();
        }
    }

	public void setBorderMode(Boolean mode) {
		borderMode = mode;
	}

	public Boolean getBorderMode() {
		return borderMode;
	}

	public ViewGroup iterativAdd(ViewGroup root, int from) {

		if (_xmlmanager != null && _xmlmanager.objects != null) {
			views = new View[_xmlmanager.objects.size()];
			Boolean[] empty = new Boolean[_xmlmanager.objects.size()];

			guiobjects = new guiObject[_xmlmanager.objects.size()];

			for (int j = 0; j < _xmlmanager.objects.size(); j++) {
				guiobjects[j] = new guiObject(j, _xmlmanager);

				if (guiobjects[j].noElement()) { // if is not widget nor layout
					empty[j] = true;
					continue;
				} else {
					empty[j] = false;
				}

				if (guiobjects[j].isLayout()) {
					ViewGroup vg = guiobjects[j].makeLayout(getActivity());
					vg.setTag(j);
					vg.setOnDragListener(new MyDragListener());
				//	vg.setOnLongClickListener(new MyOnLongClickListener());
					vg.setFocusable(true);
					vg.setClickable(true);
					// vg.setId(j);
					vg.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View view) {
							Integer id = (Integer) view.getTag();
							MiscMethods mm = new MiscMethods();
							Toast.makeText(getActivity(), mm.getPathTitle(id),
									Toast.LENGTH_SHORT).show();
						}
					});
					if (borderMode)
						vg.setBackgroundResource(R.drawable.border_gui);
					if (vg != null) {
						Layouts.put(j, vg);
						if (_xmlmanager.parent.get(j) == null)
							root.addView(Layouts.get(j), Layouts.get(j)
									.getLayoutParams());
						else if (empty[_xmlmanager.parent.get(j)])
							root.addView(Layouts.get(j), Layouts.get(j)
									.getLayoutParams());
						else
							Layouts.get(_xmlmanager.parent.get(j)).addView(
									Layouts.get(j),
									Layouts.get(j).getLayoutParams());
					}
				} else if (guiobjects[j].isWidget()) {
					views[j] = guiobjects[j].makeObject(getActivity());
					views[j].setTag(j);
					// views[j].setOnTouchListener(new MyTouchListener());
					if (!getViewName(views[j]).equals("ListView")
							&& !getViewName(views[j]).equals("Spinner")
							&& !getViewName(views[j]).equals("ExpandableListView")) {
						views[j].setOnLongClickListener(new MyOnLongClickListener());
						views[j].setOnClickListener(new OnClickListener() {
							@Override
							public void onClick(View view) {
								Integer id = (Integer) view.getTag();
								MiscMethods mm = new MiscMethods();
								Toast.makeText(getActivity(),
										mm.getPathTitle(id), Toast.LENGTH_SHORT)
										.show();
							}
						});
					}
					else {
						if (getViewName(views[j]).equals("ListView")) {
							((ListView) views[j])
									.setAdapter(getListArrayAdapter());
							((ListView) views[j]).setLongClickable(true);
							((ListView) views[j])
									.setOnItemLongClickListener(new MyOnItemLongClickListener());
						} else if (getViewName(views[j]).equals("Spinner")) {
							((Spinner) views[j])
									.setAdapter(getListArrayAdapter());
							((Spinner) views[j])
								.setOnItemLongClickListener(new MyOnItemLongClickListener());
								}
						else if(getViewName(views[j]).equals("ExpandableListView")) {
							((ExpandableListView) views[j]).setAdapter(getExpandableAdapter());
							((ExpandableListView) views[j]).setOnItemLongClickListener(new MyOnItemLongClickListener());
						}
						
					}

					// Toast.makeText(getActivity(), views[j].toString(),
					// Toast.LENGTH_SHORT).show();
					// views[j].setId(j);
					if (borderMode)
						views[j].setBackgroundResource(R.drawable.border_gui);
					if (views[j] != null) {
						if (_xmlmanager.parent.get(j) != null
								&& !empty[_xmlmanager.parent.get(j)]
								&& Layouts.get(_xmlmanager.parent.get(j)) != null)
							if ((!getViewName(
									Layouts.get(_xmlmanager.parent.get(j)))
									.equals("ScrollView")) &&
									!getViewName(
											Layouts.get(_xmlmanager.parent.get(j)))
											.equals("HorizontalScrollView"))
								Layouts.get(_xmlmanager.parent.get(j)).addView(
										views[j], views[j].getLayoutParams());
							else {
								if (Layouts.get(_xmlmanager.parent.get(j))
										.getChildCount() < 1)
									Layouts.get(_xmlmanager.parent.get(j))
											.addView(views[j],
													views[j].getLayoutParams());
								Toast.makeText(getActivity(), getViewName(Layouts.get(_xmlmanager.parent.get(j))) + " can only append one child!",
										Toast.LENGTH_SHORT).show();
							}

						else if (Layouts.get(_xmlmanager.parent.get(j)) == null) {
							int q = j;
							while (_xmlmanager.parent.get(q) != null) { // finds
																		// layout
																		// parent
																		// parent
								q = _xmlmanager.parent.get(q);
								if (Layouts.get(q) != null)
									break;
							}
							if (Layouts.get(q) != null)
								Layouts.get(q).addView(views[j],
										views[j].getLayoutParams());
							else
								root.addView(views[j],
										views[j].getLayoutParams());
						} else
							root.addView(views[j], views[j].getLayoutParams());
					}
				}
			}
			for (int i = 0; i <= Layouts.size() - 1; i++) {
				ViewGroup vg = Layouts.get(i);
				if (vg != null) {
					if (vg.getChildCount() == 0) {
						resizeEmptyLayout(vg);
						// Toast.makeText(getActivity(), vg.toString() +
						// " !!!!",
						// Toast.LENGTH_SHORT).show();
					}
				}
			}
			for(int i = 0; i < guiobjects.length; i++) {
				if(guiobjects[i] != null && guiobjects[i].isWidget() && views[i] != null) 
					guiobjects[i].setAlgin(views[i]);
			}
		}
		return root;
	}

	public void resizeEmptyLayout(ViewGroup vg) {
		Space space = new Space(getActivity());
		space.setTag("space");
		space.setLayoutParams(new LinearLayout.LayoutParams(emptyWidth,
				emptyHeight));
		vg.addView(space);
		vg.setBackgroundResource(R.drawable.shape);
	}

	@SuppressWarnings("unused")
	private final class MyTouchListener implements OnTouchListener {
		public boolean onTouch(View view, MotionEvent motionEvent) {
			if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
				
				 ClipData data = ClipData.newPlainText("", "");
				 DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
				 view.startDrag(data, shadowBuilder, view, 0);
				 view.setVisibility(View.INVISIBLE);
				 
			/*	Integer id = (Integer) view.getTag();
				MiscMethods mm = new MiscMethods();
				if (mm != null)
					Toast.makeText(getActivity(), mm.getPathTitle(id),
							Toast.LENGTH_SHORT).show(); */
				return true;
			} else {
				return false;
			}
		}
	} 
	
	public class MyOnItemLongClickListener implements  OnItemLongClickListener {

		@Override
		public boolean onItemLongClick(AdapterView<?> arg0, View view,
				int arg2, long arg3) {
			ClipData data = ClipData.newPlainText("", "");
			DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
			view.startDrag(data, shadowBuilder, view, 0);
			view.setVisibility(View.INVISIBLE);		
			return true;
		}
		
	}


	public class MyOnLongClickListener implements View.OnLongClickListener {

		@Override
		public boolean onLongClick(View view) {
			ClipData data = ClipData.newPlainText("", "");
			DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
			view.startDrag(data, shadowBuilder, view, 0);
			view.setVisibility(View.INVISIBLE);
			return true;
		}
	}

	public ArrayAdapter<String> getListArrayAdapter() {
		ArrayList<String> listElements = new ArrayList<String>();
		for (int i = 0; i <= 15; i++) {
			listElements.add("Item " + i);
		}
		ArrayAdapter<String> listAdapter = new ArrayAdapter<String>(
				getActivity(), android.R.layout.simple_list_item_1,
				listElements);
		// ((ListView)view).setAdapter(listAdapter);
		return listAdapter;
	}
	
	public SimpleExpandableListAdapter getExpandableAdapter() {
		
		@SuppressWarnings("unused")
		SimpleExpandableListAdapter mAdapter;
		
		List<Map<String, String>> groupData = new ArrayList<Map<String, String>>();
        List<List<Map<String, String>>> childData = new ArrayList<List<Map<String, String>>>();
        for (int i = 0; i < 20; i++) {
            Map<String, String> curGroupMap = new HashMap<String, String>();
            groupData.add(curGroupMap);
            curGroupMap.put("Name", "Item " + i);
            curGroupMap.put("Group", "Group " +i);
             
            List<Map<String, String>> children = new ArrayList<Map<String, String>>();
            for (int j = 0; j < 5; j++) {
                Map<String, String> curChildMap = new HashMap<String, String>();
                children.add(curChildMap);
               // curChildMap.put(NAME, "Child " + j);
                curChildMap.put("Group", "easyGUI");
            }
            childData.add(children);
        }
        
        return mAdapter = new SimpleExpandableListAdapter(
                getActivity(),
                groupData,
                android.R.layout.simple_expandable_list_item_1,
                new String[] { "Name", "Group" },
                new int[] { android.R.id.text1, android.R.id.text2 },
                childData,
                android.R.layout.simple_expandable_list_item_2,
                new String[] { "Name", "Group" },
                new int[] { android.R.id.text1, android.R.id.text2 }
                );
		
		
	}

	public static void distanceTwoRect(Rect target, Rect start) {
		float startMinX = target.exactCenterX() + start.width();
		float startMinY = target.exactCenterY() + start.height();
		float startMaxX = target.width() - start.width(); // wall.width -
															// ball.radius;
		float startMaxY = target.height() - start.height();

		if (start.exactCenterX() < startMinX) {
			min_x = startMinX;
		} else if (start.exactCenterX() > startMaxX) {
			min_x = startMaxX;
		}
		if (start.exactCenterY() < startMinY) {
			min_y = startMinY;
		} else if (start.centerY() > startMaxY) {
			min_y = startMaxY;
		}
	}

	class MyDragListener implements OnDragListener {
		Drawable enterShape = getResources().getDrawable(
				R.drawable.shape_droptarget);
		Drawable normalShape = getResources().getDrawable(R.drawable.shape);

		@SuppressWarnings("deprecation")
		@SuppressLint("NewApi")
		@Override
		public boolean onDrag(View v, DragEvent event) {
		//	int action = event.getAction();
			switch (event.getAction()) {
			case DragEvent.ACTION_DRAG_STARTED:
				// do nothing
				break;
			case DragEvent.ACTION_DRAG_ENTERED:
				v.setBackgroundDrawable(enterShape);
				break;
			case DragEvent.ACTION_DRAG_EXITED:
				v.setBackgroundDrawable(normalShape);
				break;
			case DragEvent.ACTION_DROP:
				// Dropped, reassign View to ViewGroup

			//	FocusFinder mFocusFinder = FocusFinder.getInstance();
				View view = (View) event.getLocalState();
				ViewGroup owner;
				if (view != null)
					owner = (ViewGroup) view.getParent();
				else
					owner = layout;
				int index = 0;
				// int index = owner.indexOfChild(view);
				// view.requestFocus();
				// View viewFocus = mFocusFinder.findNearestTouchable(owner,
				// (int) view.getPivotX(), (int) view.getPivotY(),
				// View.FOCUS_DOWN);
				// owner.removeView(view);
				// Toast.makeText(getActivity(), String.valueOf(event.getX()),
				// Toast.LENGTH_SHORT).show();
				// view.getPivotX();
				owner.removeView(view);
				Rect targetRect = new Rect();
				Rect startRect = new Rect();

			//	int replaceNode = 0;
				for (int i = 0; i < views.length; i++) {
					if (views[i] == view || views[i] == null)
						continue;
					views[i].getDrawingRect(targetRect);
					view.getDrawingRect(startRect);

					@SuppressWarnings("unused")
					float center_x = min_x;
					@SuppressWarnings("unused")
					float center_y = min_y;

					// Toast.makeText(getActivity(),
					// center_x+" --- "+center_y,
					// Toast.LENGTH_SHORT).show();

					if (targetRect.intersect(startRect)) { // views[i]
						ViewGroup vg = (ViewGroup) views[i].getParent();
						distanceTwoRect(targetRect, startRect);
						if (vg == v) {

							if (min_x < event.getX() || min_y < event.getY()) {
								center_x = min_x;
								center_y = min_y;
								index = vg.indexOfChild(views[i]);
							}

						} else { // diffrent layoutview
							if (min_x < event.getX() || min_y < event.getY()) {
								center_x = min_x;
								center_y = min_y;
								index = vg.indexOfChild(views[i]);
							}
						}
					}
					/*
					 * if(views[i].getGlobalVisibleRect(r)<= event.getX() &&
					 * views[i].getWidth()+views[i].getPivotX() >= event.getX()
					 * && views[i].getPivotY()<= event.getY() &&
					 * views[i].getHeight()+views[i].getPivotY()>= event.getY())
					 * { ViewGroup vg = (ViewGroup) views[i].getParent(); index
					 * = vg.indexOfChild(views[i]);
					 * Toast.makeText(getActivity(), "gefunden!",
					 * Toast.LENGTH_SHORT).show(); }
					 */
				}

				// if(MainActivity.xmlmanager.objects.get((Integer)
				// v.getTag()).contains("Grid"))

				ViewGroup container = (ViewGroup) v;
				if (v == layout) {
					Toast.makeText(getActivity(),
							"Error : No ViewGroup selected!",
							Toast.LENGTH_SHORT).show();
					owner.addView(view);
				} else {
					if (container.getChildCount() == 1) {
						/*
						 * int id = (Integer)container.getTag(); guiObject go =
						 * new guiObject(id, _xmlmanager);
						 * go.setParentsLayoutParams(container);
						 */
						if (container.getChildAt(0).getTag().equals("space"))
							container.removeViewAt(0);

					}
					if (index > container.getChildCount())
						index = container.getChildCount();
					container.addView(view, index);
					Toast.makeText(
							getActivity(),
							getViewName(view) + " added to "
									+ getViewName(container) + "!",
							Toast.LENGTH_SHORT).show();

					if (owner.getChildCount() == 0) {
						resizeEmptyLayout(owner);
					}

					refreshXmlList(container, view, index);

				}

				view.setVisibility(View.VISIBLE);
				break;
			case DragEvent.ACTION_DRAG_ENDED:
				v.setBackgroundDrawable(normalShape);
			default:
				break;
			}
			return true;
		}
	}

	public String getViewName(View view) {
		String ViewName = "";

		/*
		 * if (view != null) { String name = view.toString(); int start = 0, end
		 * = 0, k = 0; for (int i = 0; i <= name.length() - 1; i++) { if
		 * (name.charAt(i) == '.') ++k; if (name.charAt(i) == '.' && k == 2)
		 * start = i + 1; if (name.charAt(i) == '@') end = i; } ViewName =
		 * name.substring(start, end); // Toast.makeText(getActivity(),
		 * ViewName, // Toast.LENGTH_SHORT).show(); }
		 */
		ViewName = MainActivity.xmlmanager.objects.get((Integer) view.getTag());
		return ViewName;
	}

	public void getChildTree(int id) {
		// HashMap<Integer, NodeList> childList = new HashMap<Integer,
		// NodeList>();
		// HashMap<Integer, Node> childs = new HashMap<Integer, Node>();
		Node parent = MainActivity.xmlmanager.ndList.item(id);
		// int i = 0;
		// Toast.makeText(getActivity(), parent.+" ????",
		// Toast.LENGTH_SHORT).show();
		if (parent.hasChildNodes())
			childList.put(id, parent.getChildNodes());
		// childList.
		/*
		 * if(parent.hasChildNodes()) { childList.put(id,
		 * parent.getChildNodes()); Toast.makeText(getActivity(), "Halllo "+ id,
		 * Toast.LENGTH_SHORT).show();
		 * 
		 * for(int i = 0; i <= parent.getChildNodes().getLength()-1; i++) {
		 * if(MainActivity.xmlmanager.ndList.getLength()<= i) {
		 * if(parent.getChildNodes().item(i).hasChildNodes()) { //
		 * childs.put(id+i,
		 * MainActivity.xmlmanager.ndList.item(id+1).getChildNodes()); //
		 * Toast.makeText(getActivity(), "Halllo "+ id+1,
		 * Toast.LENGTH_SHORT).show(); getChildTree(i); }
		 * 
		 * } } }else Toast.makeText(getActivity(), "no childs "+ id+1,
		 * Toast.LENGTH_SHORT).show();
		 */

	}

	public void refreshXmlList(ViewGroup container, View view, int index) {
		// Node[] childNodes = new Node[container.getChildCount()];
		// int[] childId = new int[container.getChildCount()];
		// int[] childChangedPos = new int[container.getChildCount()];
		// Node newNode =
		// MainActivity.xmlmanager.document.createElement("Test");
		// String widget = "";
		// Toast.makeText(getActivity(), "AAAAAAA " +
		// MainActivity.xmlmanager.objects.get(0), Toast.LENGTH_SHORT).show();
		childList.clear();

		int id = (Integer) container.getTag();
	//	getChildTree(id);

		// widget = MainActivity.xmlmanager.objects.get((Integer)
		// view.getTag());//getViewName(view);
		int newId = (Integer) view.getTag();

		// Node parent = MainActivity.xmlmanager.getNodeFromDocument(id);
		// Node oldNode = MainActivity.xmlmanager.getNodeFromDocument(index);
		Node newNode = MainActivity.xmlmanager.getNodeFromDocument(newId);
		/*
		 * for (int j = 0; j < container.getChildCount(); j++) { if
		 * (container.getChildAt(j) == view) { childId[j] =
		 * MainActivity.xmlmanager.objects.size(); childNodes[j] = newNode; }
		 * else { childId[j] = (Integer) container.getChildAt(j).getTag();
		 * childNodes[j] = MainActivity.xmlmanager
		 * .getNodeFromDocument((Integer) container.getChildAt(j) .getTag()); }
		 * }
		 */
		int childindex = id;

		if (container.getChildCount() == 0)
			childindex = id;
		else
			childindex = id + container.indexOfChild(view);

		Toast.makeText(getActivity(), "Index = " + childindex,
				Toast.LENGTH_SHORT).show();

		MainActivity.xmlmanager.addGuiNode(newNode, childindex);

		// childChangedPos = childId;
		// Arrays.sort(childId);
		MainActivity.xmlmanager
				.saveNodeModification(MainActivity.xmlmanager.tmpFilename);
		MainActivity.xmlmanager.getDocumentAndNodeList();

		// String xmlFile = MainActivity.xmlmanager.getXMLInput();
		// MainActivity.editorfragment.refreshShaderEditor("");
		// Toast.makeText(getActivity(), xmlFile, Toast.LENGTH_LONG).show();

		MainActivity.reloadXmlFragment();
		refreshLayout();

	}

	public void refreshLayout() {
		_xmlmanager = MainActivity.xmlmanager;

		if(layout != null) { 
			layout.removeAllViewsInLayout();
				drawGUI();
		}
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
	}
	
	 @Override
	  public void onResume() {
		 super.onResume();
		// refreshLayout();
	 }

}
