package com.mycompany.easyGUI;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class DialogWidgetFragment extends DialogFragment {

	public DialogWidgetFragment() {
		// Empty constructor required for DialogFragment
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.dialog_guielements, container);

		getDialog().setTitle("add Item");

		return view;
	}
}