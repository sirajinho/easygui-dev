package com.mycompany.easyGUI;

import com.android.vending.billing.IInAppBillingService;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.MapBuilder;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import misc.DrawerAdapter;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.NodeList;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.PagerTitleStrip;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.sirajinhoapp.util.IabHelper;
import com.sirajinhoapp.util.IabResult;
import com.sirajinhoapp.util.Inventory;
import com.sirajinhoapp.util.Purchase;
import com.sirajinhoapp.util.SkuDetails;
import com.sirajinhoapp.xml.xmlManager;

public class MainActivity extends FragmentActivity implements
		XmlFragmentList.OnHeadlineSelectedListener,
		FileFragment.OnHeadlineSelectedListener {
	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a
	 * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
	 * will keep every loaded te in memory. If this becomes too memory
	 * intensive, it may be best to switch to a
	 * {@link android.support.v4.app.FragmentStatePagerAdapter}.
	 */
	SectionsPagerAdapter mSectionsPagerAdapter;

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */

	private String onePrice = "";
	private String threePrice = "";
	private String unlimitedPrice = "";

	private ViewPager mViewPager;
	private ListView mDrawerList;
	private DrawerLayout mDrawerLayout;
	private ActionBarDrawerToggle DrawerToggle;
	public static ArrayList<String> DrawerFileNames;
	public static xmlManager xmlmanager;
	public static GuiFragment guifragment;
	public static Fragment xmlfragment;
	public static EditorFragment editorfragment;
	public static PropertiesFragment propertiesfragment;
	public static String FileName;
	static FragmentManager fragmentManager;
	public static Boolean newWidget = false;
	public static Boolean openDialog = false;
	public static NodeList ndList;
	public static ArrayList<String> FileIndex;
	public static String AppPath;
	public static ArrayList<String> PARSE_ERROR;

	public static Boolean textChanged;
	public static String cacheDir;
	public static int FilesOpend;

	public String HOME_DIR;
	private static int xmlItemPos = 0;
	public Context context;
	private int PAGE;
	private IInAppBillingService mService;
	private IabHelper mHelper;

	private static String SKUoneMonth = "4_weeks_unlimited";
	private static String SKUthreeMonth = "3_months_unlimited";
	private static String SKUunlimited = "save_unlimited";

	public boolean product1Available = true;
	public boolean product2Available = true;
	public boolean product3Available = true;

	private long product1Date; // expetion
	private long product2Date;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main);

		String base64EncodedPublicKey = "MIIBIj"
				+ getResources().getString(R.string.kfgia) + "QIDAQAB";

		// compute your public key and store it in base64EncodedPublicKey
		mHelper = new IabHelper(this, base64EncodedPublicKey);

		final IabHelper.QueryInventoryFinishedListener mQueryFinishedListener = new IabHelper.QueryInventoryFinishedListener() {

			@Override
			public void onQueryInventoryFinished(IabResult result, Inventory inv) {
				if (result.isFailure()) {
					// handle error
					return;
				} else {

					// consumeProduct(SKUoneMonth);
					// consumeProduct(SKUthreeMonth);
					// consumeProduct(SKUunlimited);

					if (inv.hasPurchase(SKUoneMonth)) {
						product1Date = inv.getPurchase(SKUoneMonth)
								.getPurchaseTime();

						if (isProductExpirienced(SKUoneMonth, inv)) {
							consumeProduct(SKUoneMonth);
							product1Available = true;

						} else {
							product1Available = false;
							editorfragment = (EditorFragment) getSupportFragmentManager()
									.findFragmentById(R.id.editorlayout);
							editorfragment.enableEditor();
						}

					}
					if (inv.hasPurchase(SKUthreeMonth)) {
						product2Date = inv.getPurchase(SKUthreeMonth)
								.getPurchaseTime();

						if (isProductExpirienced(SKUthreeMonth, inv)) {
							consumeProduct(SKUthreeMonth);
							product2Available = true;

						} else {
							product2Available = false;
							editorfragment = (EditorFragment) getSupportFragmentManager()
									.findFragmentById(R.id.editorlayout);
							editorfragment.enableEditor();
						}
					}
					if (inv.hasPurchase(SKUunlimited)) {
						product3Available = false;
						editorfragment = (EditorFragment) getSupportFragmentManager()
								.findFragmentById(R.id.editorlayout);
						editorfragment.enableEditor();

					}

					onePrice = inv.getSkuDetails(SKUoneMonth).getPrice();
					threePrice = inv.getSkuDetails(SKUthreeMonth).getPrice();
					unlimitedPrice = inv.getSkuDetails(SKUunlimited).getPrice();
				}
				invalidateOptionsMenu();
			}
			// updat
		};

		mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
			@Override
			public void onIabSetupFinished(IabResult result) {
				if (!result.isSuccess()) {
					// Oh noes, there was a problem.
					Log.d("in-app", "Problem setting up In-app Billing: "
							+ result);
				} else {
					List<String> additionalSkuList = new ArrayList<String>();
					additionalSkuList.add(SKUoneMonth);
					additionalSkuList.add(SKUthreeMonth);
					additionalSkuList.add(SKUunlimited);
					mHelper.flagEndAsync();
					mHelper.queryInventoryAsync(true, additionalSkuList,
							mQueryFinishedListener);
				}
			}
		});

		Intent serviceIntent = new Intent(
				"com.android.vending.billing.InAppBillingService.BIND");
		serviceIntent.setPackage("com.android.vending");
		bindService(serviceIntent, mServiceConn, Context.BIND_AUTO_CREATE);

		fragmentManager = getSupportFragmentManager();

		if (savedInstanceState == null
		/*
		 * || !savedInstanceState.containsKey("XMLMANAGER") ||
		 * !savedInstanceState.containsKey("DRAWERFILES") ||
		 * !savedInstanceState.containsKey("FILEINDEX") ||
		 * !savedInstanceState.containsKey("FILESLOADED")
		 */) {
			xmlmanager = new xmlManager();
			DrawerFileNames = new ArrayList<String>();
			FileIndex = new ArrayList<String>();
			FilesOpend = 0;

			DrawerFileNames.add("New File");
			DrawerFileNames.add("About");
			DrawerFileNames.add("Help");
			DrawerFileNames.add("easyGUI BOOST!");
			DrawerFileNames.add("Rate easyGUI");
			// Toast.makeText(getApplicationContext(), "null Main",
			// Toast.LENGTH_LONG).show();
			// Create the adapter that will return a fragment for each of the
			// three
			// primary sections of the app.
			mSectionsPagerAdapter = new SectionsPagerAdapter(
					getSupportFragmentManager());

			// Set up the ViewPager with the sections adapter.
			mViewPager = (ViewPager) findViewById(R.id.pager);
			mViewPager.setOffscreenPageLimit(3);

			mViewPager.setAdapter(mSectionsPagerAdapter);
			mViewPager.setId(R.id.pager);

			mViewPager.setOnPageChangeListener(new OnPageChangeListener() {

				@Override
				public void onPageSelected(int index) {
					if (index == 0) {
						((EditorFragment) getSupportFragmentManager()
								.findFragmentById(R.id.editorlayout))
								.refreshShaderEditor(xmlmanager.getXMLInput());
					} else if (index == 1) {
						((GuiFragment) getSupportFragmentManager()
								.findFragmentById(R.id.rootLayoutGui))
								.refreshLayout();
						((EditorFragment) getSupportFragmentManager()
								.findFragmentById(R.id.editorlayout))
								.refreshShaderEditor(xmlmanager.getXMLInput());

					} else if (index == 2) {
						((GuiFragment) getSupportFragmentManager()
								.findFragmentById(R.id.rootLayoutGui))
								.refreshLayout();
						// reloadGUIFragment();
					}

				}

				@Override
				public void onPageScrolled(int arg0, float arg1, int arg2) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onPageScrollStateChanged(int index) {

				}
			});
		} else if (savedInstanceState != null) {
			// this.xmlmanager = (xmlManager) savedInstanceState
			// .getParcelable("XMLMANAGER");
			DrawerFileNames = savedInstanceState
					.getStringArrayList("DRAWERFILES");
			FileIndex = savedInstanceState.getStringArrayList("FILEINDEX");
			FilesOpend = savedInstanceState.getInt("FILESLOADED");
			FileName = savedInstanceState.getString("FILENAME");
			int id = getFileIndex(FileName);
			if (FileIndex.size() > id)
				MainActivity.xmlmanager = readObject(FileIndex.get(id));
			else {
				xmlmanager = new xmlManager();
				xmlmanager.empty = true;
			}
			boolean[] products = new boolean[3];
			products = savedInstanceState.getBooleanArray("PRODUCTSAVAILABLE");

			product1Available = products[0];
			product2Available = products[1];
			product3Available = products[2];

			PARSE_ERROR = savedInstanceState.getStringArrayList("PARSEERROR");
			int page = savedInstanceState.getInt("PAGE");
			if (!xmlmanager.empty) {
				xmlmanager.getDocumentAndNodeList();
				getActionBar().setTitle(xmlmanager.File);
			}
			fragmentManager = getSupportFragmentManager();

			// reloadPropertieFragment(false);
			// reloadXmlFragment();
			// mSectionsPagerAdapter = new
			// SectionsPagerAdapter(getSupportFragmentManager());

			// setPagerAdapterNew(page);
			// mViewPager.setAdapter(mSectionsPagerAdapter);
			guifragment = (GuiFragment) getSupportFragmentManager()
					.findFragmentById(R.id.rootLayoutGui);
			guifragment.refreshLayout();
			mViewPager = (ViewPager) findViewById(R.id.pager);
			mViewPager.setOffscreenPageLimit(3);
			mSectionsPagerAdapter = new SectionsPagerAdapter(
					getSupportFragmentManager());
			mViewPager.setAdapter(mSectionsPagerAdapter);

			mViewPager.setCurrentItem(page);

			if (fragmentManager.findFragmentByTag("XML") != null) { // test
				xmlfragment = getSupportFragmentManager().findFragmentByTag(
						"XML");
				getSupportFragmentManager().beginTransaction()
						.replace(R.id.xmlelements, xmlfragment, "XML").commit();
				// openDialog = false;
			}

			// mViewPager.setCurrentItem(page);
			// Toast.makeText(getApplicationContext(), "restorexml Main",
			// Toast.LENGTH_LONG).show();
		}

		context = this.getApplicationContext();

		cacheDir = getCacheDir().getAbsolutePath();

		createHomeDir();

		textChanged = false;

		PARSE_ERROR = new ArrayList<String>();

		AppPath = getApplicationInfo().dataDir;

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.left_drawer);

		mDrawerList.setAdapter(new ArrayAdapter<String>(this,
				R.layout.drawer_list_item, DrawerFileNames));

		DrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.ic_drawer, R.string.app_name, R.string.app_name) {

			public void onDrawerClosed(View view) {
				super.onDrawerClosed(view);
				if (xmlmanager.File != "")
					getActionBar().setTitle(xmlmanager.File);
				else
					getActionBar().setTitle("easyGUI");
				invalidateOptionsMenu();
				// mDrawerLayout.showContextMenu();
			}

			@SuppressWarnings("unused")
			public void onDrawerOpend(View view) {
				super.onDrawerOpened(view);
				getActionBar().setTitle("easyGUI");
				invalidateOptionsMenu();
				// mDrawerLayout.showContextMenu();
			}
		};

		mDrawerLayout.setDrawerListener(DrawerToggle);
		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
				GravityCompat.START);

		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);

		mDrawerList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
					long arg3) {
				// Toast.makeText(getApplicationContext(),
				// readObject(pos).Filename, Toast.LENGTH_SHORT).show();
				if (pos < FilesOpend) {
					AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
						ProgressDialog pd = new ProgressDialog(
								MainActivity.this);

						// String filePath = "";
						@Override
						protected void onPreExecute() {
							pd.setTitle("Loading File...");
							pd.setMessage("Please wait.");
							pd.setCancelable(false);
							pd.setIndeterminate(true);
							pd.show();
						}

						@Override
						protected Void doInBackground(Void... arg0) {
							try {
								Thread.sleep(100);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							return null;
						}

						@Override
						protected void onPostExecute(Void result) {
							if (pd != null) {
								pd.dismiss();
							}
						}

					};
					task.execute((Void[]) null);
					loadFile(DrawerFileNames.get(pos));
					mViewPager.setCurrentItem(1);
					// setPagerAdapterNew(1);
					// mViewPager.setCurrentItem(1);

				} else if (pos == DrawerFileNames.size() - 5) {
					popUpNewFile();
				} else if (pos == DrawerFileNames.size() - 4) {
					popUpAboutDialog();
				} else if (pos == DrawerFileNames.size() - 3) {
					popUpHelpDialog();
				} else if (pos == DrawerFileNames.size() - 2) {
					popUpBuyItemDialog();
				} else if (pos == DrawerFileNames.size() - 1) {
					final Uri uri = Uri.parse("market://details?id="
							+ getApplicationContext().getPackageName());
					final Intent rateAppIntent = new Intent(Intent.ACTION_VIEW,
							uri);

					if (getPackageManager().queryIntentActivities(
							rateAppIntent, 0).size() > 0) {
						startActivity(rateAppIntent);
					}
				}

			}
		});

		mDrawerList.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View view,
					final int pos, long arg3) {
				if (pos < FilesOpend) {
					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
							MainActivity.this);

					// set title
					alertDialogBuilder.setTitle("Delete File ?");

					// set dialog message
					alertDialogBuilder
							.setMessage(
									"Should I delete "
											+ DrawerFileNames.get(pos) + " ?")
							.setCancelable(false)
							.setPositiveButton("Yes",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog, int id) {
											xmlmanager
													.deleteFile(DrawerFileNames
															.get(pos));
											removeFileFromDrawerList(DrawerFileNames
													.get(pos));
											refreshDrawerList();

											if (FilesOpend > 0) {
												loadFile(DrawerFileNames
														.get(FilesOpend - 1));
												mViewPager.setCurrentItem(1);
												setPagerAdapterNew(1);
												// mViewPager.setCurrentItem(1);
											} else {
												xmlmanager = new xmlManager();

												String xmlFile = xmlmanager
														.getXMLInput();

												if (/* editorfragment != null */
												fragmentManager
														.findFragmentByTag("EDITOR") != null)
													MainActivity.editorfragment
															.refreshShaderEditor(xmlFile);

												reloadXmlFragment();
												reloadPropertieFragment(false);

												if (/* guifragment != null */
												fragmentManager
														.findFragmentByTag("GUI") != null)
													((GuiFragment) fragmentManager
															.findFragmentByTag("GUI"))
															.refreshLayout();
												// ((GuiFragment)
												// guifragment).refreshLayout();
												mViewPager.setCurrentItem(1);
												setPagerAdapterNew(1);
												// mViewPager.setCurrentItem(1);

											}
										}
									})
							.setNegativeButton("No",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog, int id) {
											dialog.cancel();
										}
									});

					// create alert dialog
					AlertDialog alertDialog = alertDialogBuilder.create();

					// show it
					alertDialog.show();
				}
				return false;
			}
		});

		refreshDrawerList();

	}

	public void consumeProduct(String sku) {
		Bundle ownedItems = null;
		try {
			ownedItems = mService.getPurchases(3, getPackageName(), "inapp",
					null);
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		int responseCode = ownedItems.getInt("RESPONSE_CODE");
		if (responseCode != 0) {

		}
		// Check response

		// Get the list of purchased items
		if (ownedItems != null) {
			ArrayList<String> purchaseDataList = ownedItems
					.getStringArrayList("INAPP_PURCHASE_DATA_LIST");
			for (String purchaseData : purchaseDataList) {
				JSONObject o = null;
				try {
					o = new JSONObject(purchaseData);
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				String purchaseToken = o.optString("token",
						o.optString("purchaseToken"));
				String productSKU = "";
				try {
					productSKU = o.getString("productId");
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				int response;
				if (productSKU.equals(SKUoneMonth)
						|| productSKU.equals(SKUthreeMonth)) {

					try {
						response = mService.consumePurchase(3,
								getPackageName(), purchaseToken);
						editorfragment = (EditorFragment) getSupportFragmentManager()
								.findFragmentById(R.id.editorlayout);
						editorfragment.disableEditor();

						Toast.makeText(getApplicationContext(),
								"Your license is expired!", Toast.LENGTH_LONG)
								.show();
					} catch (RemoteException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this); // Add this method.
	}

	@Override
	public void onStop() {
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this); // Add this method.
	}

	public void refreshDrawerList() {
		mDrawerList.setAdapter(new DrawerAdapter(this,
				R.layout.drawer_list_item, DrawerFileNames));
	}

	public void addFileToDrawerList(String Filename) {
		DrawerFileNames.add(FilesOpend, Filename);
		FileIndex.add(FilesOpend, xmlmanager.File);
		FilesOpend++;
	}

	public void removeFileFromDrawerList(String Filename) {
		int id = 0;
		for (int i = 0; i < DrawerFileNames.size(); i++) {
			if (DrawerFileNames.get(i).equals(Filename))
				id = i;
		}
		FileIndex.remove(id);
		DrawerFileNames.remove(id);
		--FilesOpend;
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		DrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		DrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {

		// outState.putParcelable("XMLMANAGER", xmlmanager);

		outState.putStringArrayList("DRAWERFILES", DrawerFileNames);
		outState.putStringArrayList("FILEINDEX", FileIndex);
		outState.putInt("FILESLOADED", FilesOpend);
		outState.putString("FILENAME", FileName);
		outState.putStringArrayList("PARSEERROR", PARSE_ERROR);
		outState.putInt("PAGE", mViewPager.getCurrentItem());
		boolean[] products = new boolean[3];

		products[0] = product1Available;
		products[1] = product2Available;
		products[2] = product3Available;

		outState.putBooleanArray("PRODUCTSAVAILABLE", products);
		super.onSaveInstanceState(outState);

	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);

		// this.xmlmanager = (xmlManager) savedInstanceState
		// .getParcelable("XMLMANAGER");
		DrawerFileNames = savedInstanceState.getStringArrayList("DRAWERFILES");
		FileIndex = savedInstanceState.getStringArrayList("FILEINDEX");
		FilesOpend = savedInstanceState.getInt("FILESLOADED");
		FileName = savedInstanceState.getString("FILENAME");
		boolean[] products = new boolean[3];
		products = savedInstanceState.getBooleanArray("PRODUCTSAVAILABLE");

		product1Available = products[0];
		product2Available = products[1];
		product3Available = products[2];

		int id = getFileIndex(FileName);
		if (FileIndex.size() > id)
			this.xmlmanager = readObject(FileIndex.get(id));
		PARSE_ERROR = savedInstanceState.getStringArrayList("PARSEERROR");
		PAGE = savedInstanceState.getInt("PAGE");

		if (!xmlmanager.empty)
			xmlmanager.getDocumentAndNodeList();

		// loadFile(FileName);
		// reloadPropertieFragment(false);
		// reloadXmlFragment();

		// mSectionsPagerAdapter = new SectionsPagerAdapter(
		// getSupportFragmentManager());

		// setPagerAdapterNew(PAGE);

		mViewPager.setCurrentItem(PAGE);
		// Toast.makeText(getApplicationContext(), "onRestore Main",
		// Toast.LENGTH_LONG).show();

	}

	@Override
	public void onAttachedToWindow() {
		// mViewPager.setCurrentItem(PAGE);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (mHelper != null)
			mHelper.dispose();

		if (mService != null) {
			unbindService(mServiceConn);
		}
	}

	public static void reloadEditor(String code) {
		FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		System.out.println("fragment realodxml props");

		if (/* xmlfragment != null */fragmentManager
				.findFragmentById(R.id.editorlayout) != null) {
			((EditorFragment) fragmentManager
					.findFragmentById(R.id.editorlayout))
					.refreshShaderEditor(code);

		}
	}

	public static void reloadXmlFragment() {

		FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		System.out.println("fragment realodxml props");

		if (/* xmlfragment != null */fragmentManager.findFragmentByTag("XML") != null) {
			xmlfragment = new XmlFragmentList();
			fragmentTransaction.replace(R.id.xmlelements, xmlfragment, "XML");
			fragmentTransaction.commitAllowingStateLoss();
		}
	}

	public static void reloadGUIFragment() {

		FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();

		if (/* guifragment != null */fragmentManager.findFragmentByTag("GUI") != null) {
			guifragment = new GuiFragment();
			fragmentTransaction.replace(R.id.rootLayoutdesign, guifragment,
					"GUI");
			fragmentTransaction.addToBackStack("GUI");
			fragmentTransaction.commit();
		}
	}

	public static void reloadPropertieFragment(Boolean removed) {
		FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();

		if (/* propertiesfragment != null */fragmentManager
				.findFragmentByTag("PROPS") != null) {
			// Toast.makeText(propertiesfragment.getActivity(), "reaload porp",
			// Toast.LENGTH_LONG).show();

			propertiesfragment = new PropertiesFragment();
			if (removed)
				propertiesfragment.setArgs(xmlmanager, 0);
			else
				propertiesfragment.setArgs(xmlmanager, xmlItemPos);

			fragmentTransaction.replace(R.id.properties, propertiesfragment,
					"PROPS");
			fragmentTransaction.commitAllowingStateLoss();
			System.out.println("fragment xml realoadprops");
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (DrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		// Handle item selection
		// switch (item.getItemId()) {
		// case R.id.border:
		if (item.getItemId() == R.id.border) {
			if (fragmentManager.findFragmentByTag("GUI") != null) { // test
				if (((GuiFragment) ((GuiFragment) getSupportFragmentManager()
						.findFragmentById(R.id.rootLayoutGui))).getBorderMode()) {
					((GuiFragment) ((GuiFragment) getSupportFragmentManager()
							.findFragmentById(R.id.rootLayoutGui)))
							.setBorderMode(false);
					((GuiFragment) ((GuiFragment) getSupportFragmentManager()
							.findFragmentById(R.id.rootLayoutGui)))
							.refreshLayout();
				} else {
					((GuiFragment) getSupportFragmentManager()
							.findFragmentById(R.id.rootLayoutGui))
							.setBorderMode(true);
					((GuiFragment) getSupportFragmentManager()
							.findFragmentById(R.id.rootLayoutGui))
							.refreshLayout();

				}
			}
			// break;
		}
		// case
		else if (item.getItemId() == R.id.parse_errors) {
			// setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
			popUpParseErrors();
			// break;
		}
		// case
		else if (item.getItemId() == R.id.open) {
			mViewPager.setCurrentItem(1);
			// FragmentManager fragmentManager = getSupportFragmentManager();
			FragmentTransaction fragmentTransaction = getSupportFragmentManager()
					.beginTransaction();

			if (openDialog == false) {
				if (getSupportFragmentManager().findFragmentById(
						R.id.xmlelements) == null) {
					FileFragment fragment = new FileFragment();
					fragmentTransaction.add(R.id.xmlelements, fragment, "FILE");
					fragmentTransaction.addToBackStack("FILE");
					fragmentTransaction.commitAllowingStateLoss();
					// fragmentTransaction.commit();
				} else {
					fragmentTransaction.replace(R.id.xmlelements,
							new FileFragment(), "FILE");
					fragmentTransaction.addToBackStack("FILE");
					fragmentTransaction.commitAllowingStateLoss();

				}
				openDialog = true;

			} else {
				// if
				// (getSupportFragmentManager().findFragmentById(R.id.xmlelements)
				// != null) { // test
				if (getSupportFragmentManager().findFragmentById(
						R.id.xmlelements) == null) {
					XmlFragmentList xmlfragment = new XmlFragmentList();
					fragmentTransaction.add(R.id.xmlelements, xmlfragment,
							"XML");
					fragmentTransaction.addToBackStack("XML");
					fragmentTransaction.commitAllowingStateLoss();
					// fragmentTransaction.commit();
				} else {
					fragmentTransaction.replace(R.id.xmlelements,
							new XmlFragmentList(), "XML");
					fragmentTransaction.addToBackStack("XML");
					fragmentTransaction.commitAllowingStateLoss();

				}
				openDialog = false;
				// }
			}
			// break;
		}
		// case
		else if (item.getItemId() == R.id.save) {
			if (isFileLoaded(xmlmanager.Filename)) {
				// xmlmanager.saveNodeModification(xmlmanager.Filename);
				// xmlmanager.saveNodeModification(xmlmanager.tmpFilename);
				if (getSupportFragmentManager().findFragmentById(
						R.id.editorlayout) != null) { // test
					xmlmanager.saveFile(xmlmanager.Filename,
							((EditorFragment) fragmentManager
									.findFragmentById(R.id.editorlayout))
									.getEditorContent());
					// editorfragment.);
					xmlmanager.saveFile(xmlmanager.tmpFilename,
							((EditorFragment) fragmentManager
									.findFragmentById(R.id.editorlayout))
									.getEditorContent());
					Toast.makeText(getApplicationContext(),
							xmlmanager.Filename + " saved!", Toast.LENGTH_SHORT)
							.show();
				} else {
					String xml = xmlmanager.getXMLInput();
					xmlmanager.saveFile(xmlmanager.Filename, xml);
					Toast.makeText(getApplicationContext(),
							xmlmanager.Filename + " saved!", Toast.LENGTH_SHORT)
							.show();
				}
			}
			// break;
		}

		// default:
		else
			return true;
		// }
		return true;
	}

	public void setPagerAdapterNew(int page) {
		mSectionsPagerAdapter = new SectionsPagerAdapter(
				getSupportFragmentManager());

		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);

		if (page != -1) {
		}
		// Toast.makeText(getApplication(), "setnewPager", Toast.LENGTH_LONG)
		// .show();
	}

	@Override
	public void onFileSelected(final String Filename) {
		FileName = Filename;

		loadFile(Filename);

		refreshDrawerList();
		setPagerAdapterNew(-1);
		mViewPager.setCurrentItem(1);

		Toast.makeText(getApplicationContext(),
				FileIndex.get(getFileIndex(Filename)) + " opend !",
				Toast.LENGTH_SHORT).show();

	}

	public void loadFile(final String Filename) {
		if (xmlmanager != null) {
			if (!xmlmanager.empty) {
				xmlmanager.saveNodeModification(xmlmanager.tmpFilename);
				saveObject(xmlmanager);
			}
		}

		if (isFileLoaded(Filename)) {
			xmlmanager = readObject(FileIndex.get(getFileIndex(Filename)));
			/*
			 * String xmlFile = xmlmanager.getXMLInput();
			 * xmlmanager.copyFile(xmlmanager.Filename, xmlmanager.tmpFilename);
			 * xmlmanager.saveNodeModification(xmlmanager.tmpFilename);
			 */
			this.FileName = Filename;

			xmlmanager.getDocumentAndNodeList();
			// xmlmanager.saveNodeModification(xmlmanager.tmpFilename);

			// Toast.makeText(getApplicationContext(), xmlmanager.tmpFilename,
			// Toast.LENGTH_SHORT).show();
			// Toast.makeText(getApplicationContext(),
			// "Index : "+FileIndex.get(getFileIndex(Filename)),
			// Toast.LENGTH_SHORT).show();
		} else {
			xmlmanager = new xmlManager(Filename);
			// Toast.makeText(getApplicationContext(), xmlmanager.getXMLInput(),
			// Toast.LENGTH_SHORT).show();
			xmlmanager.saveNodeModification(xmlmanager.tmpFilename);
			xmlmanager.saveNodeModification(xmlmanager.Filename);
			addFileToDrawerList(Filename);
			this.FileName = Filename;
			/*
			 * try { ((EditorFragment) editorfragment).loadFile(FileName); }
			 * catch (IOException editor) { editor.printStackTrace();
			 * editor.getLocalizedMessage(); }
			 */
		}

		String xmlFile = xmlmanager.getXMLInput();
		Log.i("xml", "xmlFile" + xmlFile);
		// if (getSupportFragmentManager().findFragmentByTag("EDITOR") != null)
		// //test
		// EditorFragment ef = (EditorFragment)
		// getSupportFragmentManager().findFragmentById(R.id.editorlayout);
		// ef.refreshShaderEditor(xmlFile);
		// MainActivity.editorfragment.refreshShaderEditor(xmlFile);
		reloadXmlFragment();
		reloadPropertieFragment(false);
		((EditorFragment) getSupportFragmentManager().findFragmentById(
				R.id.editorlayout)).refreshShaderEditor(xmlmanager
				.getXMLInput());
		((GuiFragment) getSupportFragmentManager().findFragmentById(
				R.id.rootLayoutGui)).refreshLayout();
		// if (getSupportFragmentManager().findFragmentById(R.id.rootLayoutGui)
		// != null)
		// ((GuiFragment) getSupportFragmentManager().findFragmentById(
		// R.id.rootLayoutGui)).refreshLayout();
		// ((GuiFragment) guifragment).refreshLayout();

		getActionBar().setTitle(xmlmanager.File);

		saveObject(xmlmanager);

	}

	public static Boolean isFileLoaded(String Filename) {
		Boolean loaded = false;

		for (int i = 0; i <= FilesOpend; i++) {
			if (DrawerFileNames.get(i).equals(Filename))
				loaded = true;
		}
		return loaded;
	}

	public static Boolean FileLoaded() {
		if (FilesOpend > 0)
			return true;
		else
			return false;
	}

	@Override
	public void onBackPressed() {
		if (FileLoaded()) {
			popUpDiscardChangesDialog();
		} else {
			finish();
		}
	}

	public void popUpDiscardChangesDialog() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				MainActivity.this);

		// set title
		alertDialogBuilder.setTitle("Exit easyGUI ?");

		// set dialog message
		alertDialogBuilder
				.setMessage("Make sure you saved your XML Layout Files.")
				.setPositiveButton("i saved it!",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
								finish();
							}
						})
				.setNeutralButton("return to save files!",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});
		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}

	public static int getFileIndex(String Filename) {
		int id = 0;

		for (int i = 0; i <= FilesOpend; i++) {
			if (DrawerFileNames.get(i).equals(Filename)) {
				id = i;
				break;
			}
		}
		return id;
	}

	@Override
	public void onArticleSelected(int position) {

		xmlItemPos = position;
		// FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction fragmentTransaction = getSupportFragmentManager()
				.beginTransaction();

		if (getSupportFragmentManager().findFragmentById(R.id.properties) != null) { // test
			propertiesfragment = new PropertiesFragment();
			((PropertiesFragment) propertiesfragment).setArgs(xmlmanager,
					position);
			fragmentTransaction.replace(R.id.properties, propertiesfragment,
					"PROPS");

			fragmentTransaction.commitAllowingStateLoss();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// super.onPrepareOptionsMenu(menu);

		if (!PARSE_ERROR.isEmpty()) {
			// menu.removeItem(R.id.parse_errors);
			menu.getItem(2).setIcon(R.drawable.ic_action_error_warning);
			// Toast.makeText(getApplicationContext(),
			// "xmlInput" + xmlmanager.getXMLInput(), Toast.LENGTH_LONG)
			// .show();
			// menu.add(1, R.id.parse_errors, Menu.FIRST,
			// "").setIcon(R.drawable.ic_action_error_warning)
			// .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		}
		if (product1Available && product2Available && product3Available) {
			menu.getItem(1).setEnabled(false);
		}
		return super.onPrepareOptionsMenu(menu);

	}

	@Override
	public void onResume() {
		super.onResume();
		invalidateOptionsMenu();
		refreshDrawerList();
		// Toast.makeText(getApplicationContext(), "onResume Main",
		// Toast.LENGTH_LONG).show();

	}

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		private FragmentManager fm;

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
			this.fm = fm;
		}

		@Override
		public Fragment getItem(int position) {
			// getItem is called to instantiate the fragment for the given page.
			// Return a DummySectionFragment (defined as a static inner class
			// below) with the page number as its lone argument.
			// FragmentManager fragmentManager = getSupportFragmentManager();
			/*
			 * if ((position + 1) == 1) { editorfragment = new EditorFragment();
			 * FragmentTransaction fragmentTransaction = fm. beginTransaction();
			 * fragmentTransaction.add(R.id.editorlayout, //test replace
			 * editorfragment, "EDITOR");
			 * fragmentTransaction.addToBackStack("EDITOR");
			 * 
			 * fragmentTransaction.commit(); fm.executePendingTransactions();
			 * return editorfragment;
			 * 
			 * } /* else if((position+1)==2) { Fragment fragment = new
			 * DummySectionFragment(); Bundle args = new Bundle();
			 * args.putInt(DummySectionFragment.ARG_SECTION_NUMBER, 2);
			 * fragment.setArguments(args); return fragment; }
			 */
			/*
			 * else if ((position + 1) == 3) { guifragment = new GuiFragment();
			 * return guifragment; }
			 */
			Fragment fragment = new DummySectionFragment(fm);
			Bundle args = new Bundle();
			args.putInt(DummySectionFragment.ARG_SECTION_NUMBER, position + 1);
			fragment.setArguments(args);
			return fragment;
		}

		@Override
		public int getCount() {
			// Show 3 total pages.
			return 3;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			super.destroyItem(container, position, object);
			if (fm != null) {

				if (position == 0) {
					fm.beginTransaction()
							.remove(fm.findFragmentById(R.id.editorlayout))
							.commit();
				} else if (position == 1) {
					fm.beginTransaction()
							.remove(fm.findFragmentById(R.id.properties))
							.commit();
					fm.beginTransaction()
							.remove(fm.findFragmentById(R.id.xmlelements))
							.commit();

				} else if (position == 2) {

					fm.beginTransaction()
							.remove(fm.findFragmentById(R.id.rootLayoutGui))
							.commit();
				}
			}
		}

		@Override
		public CharSequence getPageTitle(int position) {
			Locale l = Locale.getDefault();
			switch (position) {
			case 0:
				return getString(R.string.title_section1).toUpperCase(l);
			case 1:
				return getString(R.string.title_section2).toUpperCase(l);
			case 2:
				return getString(R.string.title_section3).toUpperCase(l);
			}
			return null;
		}

	}

	/**
	 * A dummy fragment representing a section of the app, but that simply
	 * displays dummy text.
	 */
	public static class DummySectionFragment extends Fragment {
		/**
		 * The fragment argument representing the section number for this
		 * fragment.
		 */
		public static final String ARG_SECTION_NUMBER = "section_number";
		public FragmentManager fm;

		public DummySectionFragment(FragmentManager fm) {
			this.fm = fm;
		}

		public DummySectionFragment() {

		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView;
			rootView = inflater.inflate(R.layout.fragment_main_dummy,
					container, false);

			// setRetainInstance(true);

			// if (fm == null) {
			fm = getActivity().getSupportFragmentManager();
			// }

			if (getArguments().getInt(ARG_SECTION_NUMBER) == 1) {
				rootView = inflater.inflate(R.layout.fragment_editor,
						container, false);
				// if (savedInstanceState == null) {
				boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
				// FragmentManager fragmentManager;

				// fragmentManager = getFragmentManager();
				if (savedInstanceState == null) {
					FragmentTransaction fragmentTransaction = fm
							.beginTransaction();
					// if (fm.findFragmentById(R.id.editorlayout) == null) {
					Log.i("editor", "editor fragment try to add");
					editorfragment = new EditorFragment();
					fragmentTransaction.add(R.id.editorlayout, // test
																// replace
							editorfragment, "EDITOR");
					fragmentTransaction.addToBackStack("EDITOR");

					fragmentTransaction.commit();
					// }
				}
				// fm.executePendingTransactions();
				// }
				// }

			} else if (getArguments().getInt(ARG_SECTION_NUMBER) == 2) {
				rootView = inflater.inflate(R.layout.fragment_xml, container,
						false);
				boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
				// FragmentManager fragmentManager;
				// if (tabletSize)
				// fragmentManager = getFragmentManager();
				// else
				// fragmentManager = getSupportFragmentManager();
				FragmentTransaction fragmentTransaction = fm.beginTransaction();

				if (savedInstanceState == null) {
					// if (propertiesfragment == null && xmlfragment == null) {
					System.out.println("fragment xml props");
					propertiesfragment = new PropertiesFragment();
					propertiesfragment.setArgs(xmlmanager, 0);
					fragmentTransaction.add(R.id.properties, // replace
							propertiesfragment, "PROPS");
					xmlfragment = new XmlFragmentList();
					fragmentTransaction.add(R.id.xmlelements, xmlfragment,
							"XML");
					// Toast.makeText(getActivity(), "222",
					// Toast.LENGTH_SHORT).show();

					fragmentTransaction.commit();
					// }
				} else {
					reloadPropertieFragment(true);
					reloadXmlFragment();
				}

			} else if (getArguments().getInt(ARG_SECTION_NUMBER) == 3) {
				rootView = inflater.inflate(R.layout.fragment_gui, container,
						false);
				// if (savedInstanceState == null) {
				boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
				// FragmentManager fragmentManager;
				/*
				 * if (tabletSize) fragmentManager = getFragmentManager(); else
				 * fragmentManager = getFragmentManager();
				 */
				FragmentTransaction fragmentTransaction = fm.beginTransaction();

				if (savedInstanceState == null) {
					guifragment = new GuiFragment();
					// Toast.makeText(getActivity(), "new GuiFragmet",
					// Toast.LENGTH_SHORT).show();
					fragmentTransaction.add(R.id.rootLayoutGui, // replace
							guifragment, "GUI");
					fragmentTransaction.addToBackStack("GUI");

					fragmentTransaction.commit();

				}
			}
			// }

			return rootView;
		}
	}

	private void saveObject(xmlManager xmlm) {

		try {
			FileOutputStream fileOut = new FileOutputStream(getCacheDir()
					+ "/easyGUI" + xmlm.File + ".ser");
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(xmlm);
			out.close();
			fileOut.close();
			System.out.println("Serialized data is saved in " + getCacheDir()
					+ "/easyGUI" + xmlm.File + ".ser");
			// FilesOpend++;
		} catch (IOException i) {
			i.printStackTrace();
		}
	}

	public xmlManager readObject(String File) {
		xmlManager xmlm = null;
		try {
			InputStream file = new FileInputStream(getCacheDir() + "/easyGUI"
					+ File + ".ser");
			InputStream buffer = new BufferedInputStream(file);
			ObjectInput input = new ObjectInputStream(buffer);

			try {
				xmlm = (xmlManager) input.readObject();
				System.out.println("Read Ser data " + File);
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			input.close();
		} catch (IOException i) {
			i.printStackTrace();
		}
		return xmlm;
	}

	public void popUpHelpDialog() {
		final Dialog dialog = new Dialog(this);
		dialog.setContentView(R.layout.dialog_help);
		Button close = (Button) dialog.findViewById(R.id.closehelp);
		close.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.cancel();

			}

		});

		dialog.setTitle("Help");
		dialog.show();
	}

	public void popUpAboutDialog() {
		final Dialog dialog = new Dialog(this);
		dialog.setContentView(R.layout.dialog_about);
		Button close = (Button) dialog.findViewById(R.id.aboutclose);
		TextView info = (TextView) dialog.findViewById(R.id.about_info);
		String versionName = "2";
		try {
			versionName = context.getPackageManager().getPackageInfo(
					context.getPackageName(), 0).versionName;
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		info.setText("easyGUI " + versionName + " "
				+ context.getResources().getString(R.string.easy_gui_info));
		close.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.cancel();

			}

		});

		dialog.setTitle("About");
		dialog.show();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.d("inapp", "onActivityResult(" + requestCode + "," + resultCode
				+ "," + data);

		// Pass on the activity result to the helper for handling
		if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
			// not handled, so handle it ourselves (here's where you'd
			// perform any handling of activity results not related to in-app
			// billing...
			super.onActivityResult(requestCode, resultCode, data);
		} else {
			Log.d("inapp", "onActivityResult handled by IABUtil.");
		}
	}

	@SuppressLint("NewApi")
	public void popUpBuyItemDialog() {
		final Dialog dialog = new Dialog(this);
		dialog.setContentView(R.layout.dialog_buy_items);
		Button close = (Button) dialog.findViewById(R.id.buy_item_close);
		Button oneMonth = (Button) dialog.findViewById(R.id.btn_buy_1_month);
		Button threeMonths = (Button) dialog
				.findViewById(R.id.btn_buy_3_months);
		Button unlimited = (Button) dialog.findViewById(R.id.btn_buy_unlimited);

		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:s",
				Locale.getDefault());
		String purchaseDate1 = "";
		String purchaseDate2 = "";

		if (!product1Available) {
			oneMonth.setBackground(getResources().getDrawable(
					R.drawable.border_gui));
			oneMonth.setClickable(false);
			purchaseDate1 = df.format(new Date(product1Date));
		}
		if (!product2Available) {
			threeMonths.setBackground(getResources().getDrawable(
					R.drawable.border_gui));
			threeMonths.setClickable(false);
			purchaseDate2 = df.format(new Date(product2Date));
		}
		if (!product3Available) {
			unlimited.setBackground(getResources().getDrawable(
					R.drawable.border_gui));
			unlimited.setClickable(false);
		}

		oneMonth.setText(oneMonth.getText().toString() + " " + onePrice + " "
				+ purchaseDate1);
		threeMonths.setText(threeMonths.getText().toString() + " " + threePrice
				+ " " + purchaseDate2);
		unlimited
				.setText(unlimited.getText().toString() + " " + unlimitedPrice);

		final SessionIdentifierGenerator consumerKeyGen = new SessionIdentifierGenerator();

		final IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {

			@Override
			public void onIabPurchaseFinished(IabResult result, Purchase info) {
				if (result.isFailure()) {
					Log.d("in_app", "Error purchasing: " + result);

					return;
				} else if (info.getSku().equals(SKUoneMonth)) {
					product1Available = false;
					purchaseCompleted(onePrice, SKUoneMonth, "1 month save",
							info.getToken());
					Toast.makeText(getApplicationContext(),
							"4 Weeks unlimited!", Toast.LENGTH_LONG).show();
					Calendar c = Calendar.getInstance();
					product1Date = c.getTimeInMillis();
					editorfragment = (EditorFragment) getSupportFragmentManager()
							.findFragmentById(R.id.editorlayout);
					editorfragment.enableEditor();

				} else if (info.getSku().equals(SKUthreeMonth)) {
					product2Available = false;
					purchaseCompleted(threePrice, SKUthreeMonth,
							"3 months save", info.getToken());
					Toast.makeText(getApplicationContext(),
							"3 Months unlimited!", Toast.LENGTH_LONG).show();
					Calendar c = Calendar.getInstance();
					product2Date = c.getTimeInMillis();
					editorfragment = (EditorFragment) getSupportFragmentManager()
							.findFragmentById(R.id.editorlayout);
					editorfragment.enableEditor();

				} else if (info.getSku().equals(SKUunlimited)) {
					product3Available = false;
					purchaseCompleted(unlimitedPrice, SKUunlimited,
							"unlimited save", info.getToken());
					Toast.makeText(getApplicationContext(), "PREMIUM!",
							Toast.LENGTH_LONG).show();
					editorfragment = (EditorFragment) getSupportFragmentManager()
							.findFragmentById(R.id.editorlayout);
					editorfragment.enableEditor();
				}
				invalidateOptionsMenu();
				dialog.dismiss();
			}
		};

		close.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.cancel();

			}

		});

		oneMonth.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String key = consumerKeyGen.nextSessionId();
				if (mHelper != null) {
					mHelper.flagEndAsync();
					mHelper.launchPurchaseFlow(MainActivity.this, SKUoneMonth,
							10001, mPurchaseFinishedListener, key);
				} else {
					Toast.makeText(getApplicationContext(), "mHelper null",
							Toast.LENGTH_LONG).show();

				}
			}

		});
		threeMonths.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String key = consumerKeyGen.nextSessionId();
				if (mHelper != null) {
					mHelper.flagEndAsync();
					mHelper.launchPurchaseFlow(MainActivity.this,
							SKUthreeMonth, 10002, mPurchaseFinishedListener,
							key);
				}
			}

		});
		unlimited.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String key = consumerKeyGen.nextSessionId();
				if (mHelper != null) {
					mHelper.flagEndAsync();
					mHelper.launchPurchaseFlow(MainActivity.this, SKUunlimited,
							10003, mPurchaseFinishedListener, key);
				}
			}

		});
		dialog.setTitle("easyGUI BOOST!");
		if (product3Available) {
			dialog.show();
		} else {
			Toast.makeText(getApplicationContext(),
					"You have already the unlimited version!",
					Toast.LENGTH_LONG).show();
		}
	}

	public void popUpParseErrors() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				MainActivity.this);

		// set title
		alertDialogBuilder.setTitle("XML Parsing Errors");

		// set dialog message
		String errorList = "";
		for (int i = 0; i < PARSE_ERROR.size(); i++) {
			errorList = errorList + PARSE_ERROR.get(i) + "\n\n";
		}
		errorList = errorList
				+ "\nLast correct version of your XML File is saved!"
				+ " Press recreate to retrieve the last correct version.";
		alertDialogBuilder
				.setMessage(errorList)
				.setPositiveButton("close",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						})
				.setNeutralButton("recreate",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								String xmlFile = MainActivity.xmlmanager
										.getXMLInput();
								if (fragmentManager.findFragmentByTag("EDITOR") != null) // test
									((EditorFragment) fragmentManager
											.findFragmentByTag("EDITOR"))
											.refreshShaderEditor(xmlFile);
								// MainActivity.editorfragment
								// .refreshShaderEditor(xmlFile);
								MainActivity.PARSE_ERROR.clear();
								invalidateOptionsMenu();
								dialog.cancel();
							}
						});
		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}

	public void popUpNewFile() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				MainActivity.this);

		alertDialogBuilder.setTitle("New File");

		final EditText newFilename = new EditText(this);
		newFilename.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.WRAP_CONTENT));
		newFilename.setHint("enter new Filename...");

		alertDialogBuilder
				.setView(newFilename)
				.setPositiveButton("close",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						})
				.setNeutralButton("open",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								String name = newFilename.getText().toString();
								if (FileExist(name)) {
									Toast.makeText(getApplication(),
											"File already exists!",
											Toast.LENGTH_SHORT).show();
								} else {
									File newFile;
									String root = Environment
											.getExternalStorageDirectory()
											.getPath();
									String home = root + "/easyGUI/layout";
									if (name.endsWith(".xml"))
										newFile = new File(home + "/" + name);
									else {
										newFile = new File(home + "/" + name
												+ ".xml");
										// Toast.makeText(getParent(), HOME_DIR
										// + "/" + name + ".xml",
										// Toast.LENGTH_SHORT).show();
									}
									try {
										if (newFile.createNewFile()) {
											Toast.makeText(getApplication(),
													"File created!",
													Toast.LENGTH_SHORT).show();

											PrintWriter writer = new PrintWriter(
													newFile.getAbsolutePath(),
													"UTF-8");
											writer.print("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
											writer.flush();
											writer.close();

											loadFile(newFile.getAbsolutePath());

											String xmlFile = MainActivity.xmlmanager
													.getXMLInput();
											if (fragmentManager
													.findFragmentByTag("EDITOR") != null)
												((EditorFragment) fragmentManager
														.findFragmentByTag("EDITOR"))
														.refreshShaderEditor(xmlFile);

											// MainActivity.editorfragment
											// .refreshShaderEditor(xmlFile);

											xmlmanager.getDocumentAndNodeList();
											reloadXmlFragment();

											refreshDrawerList();
											mViewPager.setCurrentItem(1);
											// setPagerAdapterNew(1);
											// mViewPager.setCurrentItem(1);

										} else {
											Toast.makeText(
													getApplication(),
													"Could not create File ! File already exists.",
													Toast.LENGTH_SHORT).show();
											// dialog.dismiss();
											dialog.cancel();
										}
									} catch (IOException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}

								}

								/*
								 * String xmlFile =
								 * MainActivity.xmlmanager.getXMLInput();
								 * MainActivity
								 * .editorfragment.refreshShaderEditor(xmlFile);
								 * MainActivity.PARSE_ERROR.clear();
								 */
								invalidateOptionsMenu();
								// dialog.cancel();
							}
						});
		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();

	}

	public Boolean FileExist(String Filename) {
		// String root = Environment.getExternalStorageDirectory().getPath();
		File file = new File(HOME_DIR + "/" + Filename);

		if (file.exists())
			return true;
		else
			return false;
	}

	public void createHomeDir() {
		String root = Environment.getExternalStorageDirectory().getPath();
		String home = root + "/easyGUI/layout";
		File homeDir = new File(home);

		if (homeDir.exists())
			return;
		else
			homeDir.mkdirs();
		HOME_DIR = homeDir.getAbsolutePath();
		Toast.makeText(getApplication(), "home dir" + HOME_DIR,
				Toast.LENGTH_SHORT).show();
	}

	ServiceConnection mServiceConn = new ServiceConnection() {
		@Override
		public void onServiceDisconnected(ComponentName name) {
			mService = null;
		}

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			mService = IInAppBillingService.Stub.asInterface(service);
		}
	};

	public void purchaseCompleted(String price, String SKU, String product,
			String transactionId) {
		NumberFormat format = NumberFormat.getCurrencyInstance();
		Number number = null;
		try {
			number = format.parse(price);
		} catch (ParseException e) {

			e.printStackTrace();
		}

		EasyTracker.getInstance(this).send(MapBuilder.createItem(transactionId, // (String)
																				// Transaction
																				// ID
				product, // (String) Product name
				SKU, // (String) Product SKU
				"save xml files", // (String) Product category
				Double.valueOf(number.toString()), // (Double) Product price
				1L, // (Long) Product quantity
				Currency.getInstance(Locale.getDefault()).getCurrencyCode()) // (String)
																				// Currency
																				// code
				.build());
	}

	/*
	 * private class GuiElementsTask extends AsyncTask<Void, Void, Boolean> {
	 * AlertDialog.Builder builder; AlertDialog alertDialog; View layout;
	 * 
	 * protected void onPreExecute() { LayoutInflater inflater =
	 * (LayoutInflater) MainActivity.this
	 * .getSystemService(LAYOUT_INFLATER_SERVICE); layout =
	 * inflater.inflate(R.layout.dialog_guielements, (ViewGroup)
	 * findViewById(R.id.rootGuiElements));
	 * 
	 * builder = new AlertDialog.Builder(MainActivity.this);
	 * builder.setView(layout); alertDialog = builder.create();
	 * alertDialog.show(); }
	 * 
	 * protected Boolean doInBackground(final Void unused) { Button b = (Button)
	 * layout.findViewById(R.id.button1); b.setOnClickListener(new
	 * OnClickListener() {
	 * 
	 * @Override public void onClick(View arg0) { // TODO Auto-generated method
	 * stub
	 * 
	 * } });
	 * 
	 * return null; }
	 * 
	 * protected void onPostExecute(final Boolean result) {
	 * 
	 * if (this.alertDialog.isShowing()) { this.alertDialog.dismiss(); }
	 * 
	 * 
	 * }
	 * 
	 * @Override protected Boolean doInBackground(Void... arg0) { // TODO
	 * Auto-generated method stub return null; } }
	 */
	public final class SessionIdentifierGenerator {
		private SecureRandom random = new SecureRandom();

		public String nextSessionId() {
			return new BigInteger(130, random).toString(32);
		}
	}

	public boolean isProductExpirienced(String purchase, Inventory inv) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
				Locale.getDefault());
		boolean isExpirienced = false;
		Calendar c = Calendar.getInstance();

		if (purchase.equals(SKUoneMonth)) {
			long purchaseTime = inv.getPurchase(SKUoneMonth).getPurchaseTime();
			c.add(Calendar.MONTH, -1);
			// c.add(Calendar.MINUTE, -1);
			String currentDateStr = df.format(c.getTime());

			Date forWeeksAgoDate = null;

			try {
				forWeeksAgoDate = df.parse(currentDateStr);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (forWeeksAgoDate.getTime() > purchaseTime) {
				isExpirienced = true;
			} else {
				isExpirienced = false;
			}
		}
		if (purchase.equals(SKUthreeMonth)) {
			long purchaseTime = inv.getPurchase(SKUthreeMonth)
					.getPurchaseTime();
			c.add(Calendar.MONTH, -3);
			// c.add(Calendar.MINUTE, -1);
			String currentDateStr = df.format(c.getTime());

			Date threeMonthsAgoDate = null;

			try {
				threeMonthsAgoDate = df.parse(currentDateStr);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (threeMonthsAgoDate.getTime() > purchaseTime) {
				isExpirienced = true;
			} else {
				isExpirienced = false;
			}
		}

		return isExpirienced;

	}

}
