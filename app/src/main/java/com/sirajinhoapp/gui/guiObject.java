package com.sirajinhoapp.gui;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.InputType;
import android.text.method.DigitsKeyListener;
import android.text.method.PasswordTransformationMethod;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewStub;
import android.widget.AbsoluteLayout;
import android.widget.AnalogClock;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.GridLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.ToggleButton;

import com.mycompany.easyGUI.MainActivity;
import com.mycompany.easyGUI.R;
import com.sirajinhoapp.xml.xmlManager;

@SuppressWarnings("deprecation")
public class guiObject {

	int Objectid;
	String object;
	String[] attribute;
	String[] value;
	xmlManager _xmlmanager;

	private EditText et;
	private RadioButton rd;
	private CheckBox cb;
	private TextView tv;
	private SeekBar sb;
	private ToggleButton tb;
	private ImageView iv;
	private AnalogClock ac;
	private RatingBar rb;
	private ListView lv;
	private TimePicker tp;

	public enum LayoutTyp {
		LinearLayout, RadioGroup, PagerTitleStrip, ScrollView, ButtonBar, HorizontalScrollView, DrawerLayout, ViewPager, FrameLayout, TableLayout, GridLayout, AbsoluteLayout, RelativeLayout, TableRow, NoLayout
	};

	public enum WidgetTyp {
		Button, ImageView, ToggleButton, CheckBox, RadioButton, SeekBar, Switch, ImageButton, TextView, ProgressBar, EditText, DatePicker, TimerPicker, RatingBar, NumberPicker, ListView, ExpandableListView, Spinner, BarButton, ViewStub, AnalogClock
	};

	public LayoutParams parentLayout;
	// index
	public LinearLayout.LayoutParams LinearParent; // 0
	public RelativeLayout.LayoutParams RelativeParent; // 1
	public GridLayout.LayoutParams GridParent; // 2
	public FrameLayout.LayoutParams FrameParent; // 3
	public TableLayout.LayoutParams TableParent; // 4
	public TableRow.LayoutParams TableRowParent; // 5

	public int LAYOUT_INDEX = 0;

	public LayoutTyp layoutTyp;

	public guiObject(int id, xmlManager xmlmanager) {
		Objectid = id;

		_xmlmanager = xmlmanager;

		if (id < xmlmanager.objects.size()) {
			object = xmlmanager.objects.get(id);
			attribute = xmlmanager.attributes.get(id);
			value = xmlmanager.values.get(id);

			for (int i = 0; i < value.length; i++) {
				if (value[i].toLowerCase(Locale.getDefault()).equals("true")
						|| value[i].toLowerCase(Locale.getDefault()).equals(
								"false"))
					value[i] = value[i].toLowerCase(Locale.getDefault());
			}
		}

		/*
		 * for(int i = 0; i < attribute.length; i++) { attribute[i] =
		 * attribute[i].toLowerCase(); }
		 */

	}

	public Integer[] getLayoutParams() {
		Integer[] params = new Integer[2];

		params[0] = -10; // width
		params[1] = -10;

		if (attribute.length != 0) {
			for (int j = 0; j < attribute.length; j++) {

				if (attribute[j].equals("android:layout_width")) {
					if (value[j].equals("wrap_content"))
						params[0] = LayoutParams.WRAP_CONTENT;
					else if (value[j].equals("fill_parent"))
						params[0] = LayoutParams.FILL_PARENT;
					else if (value[j].equals("match_parent"))
						params[0] = LayoutParams.MATCH_PARENT;
					else if (value[j].contains("dp")
							|| value[j].contains("dip")) {
						Pattern pattern = Pattern.compile("\\d+");
						Matcher matcher = pattern.matcher(value[j]);
						matcher.find();
						String inputInt = matcher.group();
						params[0] = Integer.valueOf(inputInt);
					}
				} else if (attribute[j].equals("android:layout_height")) {
					if (value[j].equals("wrap_content"))
						params[1] = LayoutParams.WRAP_CONTENT;
					else if (value[j].equals("fill_parent"))
						params[1] = LayoutParams.FILL_PARENT;
					else if (value[j].equals("match_parent"))
						params[1] = LayoutParams.MATCH_PARENT;
					else if (value[j].contains("dp")
							|| value[j].contains("dip")) {
						Pattern pattern = Pattern.compile("\\d+");
						Matcher matcher = pattern.matcher(value[j]);
						matcher.find();
						String inputInt = matcher.group();
						params[1] = Integer.valueOf(inputInt);
					}
				}

			}
		}
		return params;
	}

	public String getObjectsContent() {
		String content = new String();

		for (int i = 0; i < attribute.length; i++) {
			if (attribute[i].equals("android:text")) {
				content = value[i];
				break;
			} else
				content = null;
		}
		return content;
	}

	public LayoutTyp getLayoutTyp() {
		if (object != null) {
			if (object.equals("LinearLayout"))
				return LayoutTyp.LinearLayout;
			else if (object.equals("FrameLayout"))
				return LayoutTyp.FrameLayout;
			else if (object.equals("RelativeLayout"))
				return LayoutTyp.RelativeLayout;
			else if (object.equals("TableLayout"))
				return LayoutTyp.TableLayout;
			else if (object.equals("GridLayout"))
				return LayoutTyp.GridLayout;
			else if (object.equals("TableRow"))
				return LayoutTyp.TableRow;
			else if (object.equals("RadioGroup"))
				return LayoutTyp.RadioGroup;
			else if (object.equals("ScrollView"))
				return LayoutTyp.ScrollView;
			else if (object.equals("android.support.v4.widget.DrawerLayout"))
				return LayoutTyp.DrawerLayout;
			else if (object.equals("AbsoluteLayout"))
				return LayoutTyp.AbsoluteLayout;
			else if (object.equals("android.support.v4.view.ViewPager"))
				return LayoutTyp.ViewPager;
			else if (object.equals("HorizontalScrollView"))
				return LayoutTyp.HorizontalScrollView;
			else if (object.equals("ButtonBar"))
				return LayoutTyp.ButtonBar;
			else if (object.equals("android.support.v4.view.PagerTitleStrip"))
				return LayoutTyp.PagerTitleStrip;

		}
		return null;

	}

	public Boolean isLayout() {
		if (getLayoutTyp() != null)
			return true;
		else
			return false;
	}

	public Boolean isEmpty() {
		if (getObjectsContent() != null)
			return false;
		else
			return true;
	}

	public Boolean noElement() {
		if (getWidgetTyp() == null && getLayoutTyp() == null)
			return true;
		else
			return false;
	}

	public Boolean isWidget() {
		if (getWidgetTyp() != null)
			return true;
		else
			return false;
	}

	public WidgetTyp getWidgetTyp() {
		if (object.equals("Button"))
			return WidgetTyp.Button;
		else if (object.equals("EditText"))
			return WidgetTyp.EditText;
		else if (object.equals("TextView"))
			return WidgetTyp.TextView;
		else if (object.equals("RadioButton"))
			return WidgetTyp.RadioButton;
		else if (object.equals("CheckBox"))
			return WidgetTyp.CheckBox;
		else if (object.equals("SeekBar"))
			return WidgetTyp.SeekBar;
		else if (object.equals("ProgressBar"))
			return WidgetTyp.ProgressBar;
		else if (object.equals("DatePicker"))
			return WidgetTyp.DatePicker;
		else if (object.equals("AnalogClock"))
			return WidgetTyp.AnalogClock;
		else if (object.equals("ToggleButton"))
			return WidgetTyp.ToggleButton;
		else if (object.equals("DatePicker"))
			return WidgetTyp.DatePicker;
		else if (object.equals("RatingBar"))
			return WidgetTyp.RatingBar;
		else if (object.equals("Spinner"))
			return WidgetTyp.Spinner;
		else if (object.equals("ImageView"))
			return WidgetTyp.ImageView;
		else if (object.equals("ListView"))
			return WidgetTyp.ListView;
		else if (object.equals("Switch"))
			return WidgetTyp.Switch;
		else if (object.equals("TimePicker"))
			return WidgetTyp.TimerPicker;
		else if (object.equals("NumberPicker"))
			return WidgetTyp.NumberPicker;
		else if (object.equals("ExpandableListView"))
			return WidgetTyp.ExpandableListView;
		else if (object.equals("BarButton"))
			return WidgetTyp.BarButton;
		else if (object.equals("ViewStub"))
			return WidgetTyp.ViewStub;
		else if (object.equals("ImageButton"))
			return WidgetTyp.ImageButton;
		else
			return null;
	}

	public void test() {
		for (int i = 0; i < MainActivity.ndList.getLength(); i++) {

		}
	}

	public class ParentLayoutTyp<T> {

		public LinearLayout.LayoutParams LinearParent; // 0
		public RelativeLayout.LayoutParams RelativeParent; // 1
		public GridLayout.LayoutParams GridParent; // 2
		public FrameLayout.LayoutParams FrameParent; // 3
		public TableLayout.LayoutParams TableParent; // 4
		public TableRow.LayoutParams TableRowParent; // 5

		public T parentParams;

		public void set(T t) {
			parentParams = t;
		}

		public T get() {
			return parentParams;
		};
	}

	@SuppressLint("NewApi")
	public void findAndSetAttributes(View view) {
		int padding[] = new int[6];

		padding[0] = view.getPaddingLeft();
		padding[1] = view.getPaddingRight();
		padding[2] = view.getPaddingTop();
		padding[3] = view.getPaddingBottom();
		padding[4] = view.getPaddingStart();
		padding[5] = view.getPaddingEnd();

		int left = 0;
		int top = 0;
		int right = 0;
		int bottom = 0;
		int end = 0;
		int start = 0;
		int margin = 0;

		Boolean setPadding = false;
		
		Float dx = 0f;//((TextView) view).getShadowDx();
		Float dy = 0f;//((TextView) view).getShadowDy();
		Float radius = 0f;//((TextView) view).getShadowRadius();
		int color = Color.BLACK;//((TextView) view).getShadowColor();


		for (int i = 0; i < attribute.length; i++) {
			// Orientation Attribute (LinearLayout)->TableLay, TableRow
			// view.setTag(, tag)

			if (attribute[i].equals("android:id")) {
				value[i] = value[i].trim();
				if (!_xmlmanager.viewIds.containsValue(value[i]) && !value[i].isEmpty()) {
					_xmlmanager.viewIds.put(value[i], _xmlmanager.viewStart
							+ _xmlmanager.viewCounter);
					view.setId(_xmlmanager.viewStart + _xmlmanager.viewCounter);
					++_xmlmanager.viewCounter;
				}
			}
			if (attribute[i].equals("android:orientation")) {
				if (getLayoutTyp() == LayoutTyp.LinearLayout
						|| getLayoutTyp() == LayoutTyp.TableLayout
						|| getLayoutTyp() == LayoutTyp.TableRow) {
					if (getLayoutTyp() == LayoutTyp.TableRow) {
						if (value[i].equals("vertical")) {
							((TableRow) view)
									.setOrientation(LinearLayout.VERTICAL);
						} else if (value[i].equals("horizontal")) {
							((TableRow) view)
									.setOrientation(LinearLayout.HORIZONTAL);
						}
					} else if (getLayoutTyp() == LayoutTyp.LinearLayout) {
						if (value[i].equals("vertical")) {
							((LinearLayout) view)
									.setOrientation(LinearLayout.VERTICAL);
						} else if (value[i].equals("horizontal")) {
							((LinearLayout) view)
									.setOrientation(LinearLayout.HORIZONTAL);
						} else if (getLayoutTyp() == LayoutTyp.TableLayout) {
							if (value[i].equals("vertical")) {
								((TableLayout) view)
										.setOrientation(LinearLayout.VERTICAL);
							} else if (value[i].equals("horizontal"))
								((TableLayout) view)
										.setOrientation(LinearLayout.HORIZONTAL);
						}
					}
				}
			}
			// Weight Layout Param for LinearLayout and Children
			else if (attribute[i].equals("android:layout_weight")) {
				if (layoutTyp == LayoutTyp.LinearLayout
						|| getLayoutTyp() == LayoutTyp.LinearLayout) {
					LayoutParams lp = view.getLayoutParams();
					view.setLayoutParams(new LinearLayout.LayoutParams(
							lp.width, lp.height, getIntValue(value[i])));
				}
			}
			// LinearLayour Attributes
			else if (attribute[i].equals("android:baselineAligned")) {
				if (getLayoutTyp() == LayoutTyp.LinearLayout) {
					if (value[i].equals("true"))
						((LinearLayout) view).setBaselineAligned(true);
					else if (value[i].equals("false"))
						((LinearLayout) view).setBaselineAligned(false);
				}
			}

			else if (attribute[i].equals("android:measureWithLargestChild")) {
				if (getLayoutTyp() == LayoutTyp.LinearLayout) {
					if (value[i].equals("true"))
						((LinearLayout) view)
								.setMeasureWithLargestChildEnabled(true);
					else if (value[i].equals("false"))
						((LinearLayout) view)
								.setMeasureWithLargestChildEnabled(false);
				}
			}

			else if (attribute[i].equals("android:baselineAlignedChildIndex")) {
				if (layoutTyp == LayoutTyp.LinearLayout
						|| getLayoutTyp() == LayoutTyp.LinearLayout) {
					int index = getIntValue(value[i]);
					int childcount = ((LinearLayout) view).getChildCount();
					if (index < childcount && index >= 0)
						((LinearLayout) view)
								.setBaselineAlignedChildIndex(index);
				}
				// Checken
			}

			else if (attribute[i].equals("android:layout_margin")) {
				margin = getPaddingValue(value[i]);
				left = margin;
				top = margin;
				right = margin;
				bottom = margin;
			} else if (attribute[i].equals("android:layout_marginLeft")) {
				left = getPaddingValue(value[i]);
			} else if (attribute[i].equals("android:layout_marginRight")) {
				right = getPaddingValue(value[i]);
			} else if (attribute[i].equals("android:layout_marginTop")) {
				top = getPaddingValue(value[i]);
			} else if (attribute[i].equals("android:layout_marginBottom")) {
				bottom = getPaddingValue(value[i]);
			} else if (attribute[i].equals("android:layout_marginStart")) {
				start = getPaddingValue(value[i]);
				// lp.setMarginStart(start);
			} else if (attribute[i].equals("android:layout_marginEnd")) {
				end = getPaddingValue(value[i]);
				// lp.setMarginEnd(end);
			}

			if (left != 0 || top != 0 || right != 0 || bottom != 0
					|| start != 0 || end != 0) {
				if (layoutTyp == LayoutTyp.LinearLayout) {
					LinearLayout.LayoutParams layp = (LinearLayout.LayoutParams) view
							.getLayoutParams();
					LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
							layp.width, layp.height, layp.weight);

					if (left != 0 || top != 0 || right != 0 || bottom != 0)
						lp.setMargins(left, top, right, bottom);
					else if (start != 0)
						lp.setMarginStart(start);
					else if (end != 0)
						lp.setMarginEnd(end);
					// lp.gravity = layp.gravity;

					view.setLayoutParams(lp);
				} else if (layoutTyp == LayoutTyp.RelativeLayout) {
					RelativeLayout.LayoutParams layp = (RelativeLayout.LayoutParams) view
							.getLayoutParams();
					RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
							layp.width, layp.height);

					if (left != 0 || top != 0 || right != 0 || bottom != 0)
						lp.setMargins(left, top, right, bottom);
					else if (start != 0)
						lp.setMarginStart(start);
					else if (end != 0)
						lp.setMarginEnd(end);
					view.setLayoutParams(lp);
				} else if (layoutTyp == LayoutTyp.FrameLayout) {
					FrameLayout.LayoutParams layp = (FrameLayout.LayoutParams) view
							.getLayoutParams();
					FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(
							layp.width, layp.height);

					if (left != 0 || top != 0 || right != 0 || bottom != 0)
						lp.setMargins(left, top, right, bottom);
					else if (start != 0)
						lp.setMarginStart(start);
					else if (end != 0)
						lp.setMarginEnd(end);
					view.setLayoutParams(lp);
				} else if (layoutTyp == LayoutTyp.GridLayout) {
					// GridLayout.LayoutParams layp = (GridLayout.LayoutParams)
					// view.getLayoutParams();
					GridLayout.LayoutParams lp = new GridLayout.LayoutParams();

					if (left != 0 || top != 0 || right != 0 || bottom != 0)
						lp.setMargins(left, top, right, bottom);
					else if (start != 0)
						lp.setMarginStart(start);
					else if (end != 0)
						lp.setMarginEnd(end);
					view.setLayoutParams(lp);
				}
			}

			// ViewGroup
			/*
			 * else if (isLayout()) { if
			 * (attribute[i].equals("android:clipChildren")) { if
			 * (value[i].equals("true")) ((ViewGroup)
			 * view).setClipChildren(true); else if (value[i].equals("false"))
			 * ((ViewGroup) view).setClipChildren(false); } else if
			 * (attribute[i].equals("android:clipToPadding")) { if
			 * (value[i].equals("true")) ((ViewGroup)
			 * view).setClipToPadding(true); else if (value[i].equals("false"))
			 * ((ViewGroup) view).setClipToPadding(false); } }
			 */
			// RelativeLayout
			/*
			 * else if (attribute[i].equals("android:layout_alginParentLeft")) {
			 * if (value[i].equals("true"))
			 * setAlginParent(RelativeLayout.ALIGN_PARENT_LEFT, view); } else if
			 * (attribute[i].equals("android:layout_alginParentTop")) { if
			 * (value[i].equals("true"))
			 * setAlginParent(RelativeLayout.ALIGN_PARENT_TOP, view); } else if
			 * (attribute[i].equals("android:layout_alginParentRight")) { if
			 * (value[i].equals("true"))
			 * setAlginParent(RelativeLayout.ALIGN_PARENT_RIGHT, view); } else
			 * if (attribute[i].equals("android:layout_alginParentBottom")) { if
			 * (value[i].equals("true"))
			 * setAlginParent(RelativeLayout.ALIGN_PARENT_BOTTOM, view); }
			 */

			// gravity for Layouts
			if (getLayoutTyp() == LayoutTyp.LinearLayout
					|| getLayoutTyp() == LayoutTyp.TableLayout
					|| getLayoutTyp() == LayoutTyp.TableRow
					|| getLayoutTyp() == LayoutTyp.FrameLayout
					|| getLayoutTyp() == LayoutTyp.GridLayout) {
				if (attribute[i].equals("android:gravity")) {
					if (value[i].equals("center_vertical"))
						setGravity(Gravity.CENTER_VERTICAL, (ViewGroup) view);
					else if (value[i].equals("center_horizontal"))
						setGravity(Gravity.CENTER_HORIZONTAL, (ViewGroup) view);
					else if (value[i].equals("bottom"))
						setGravity(Gravity.BOTTOM, (ViewGroup) view);
					else if (value[i].equals("center"))
						setGravity(Gravity.CENTER, (ViewGroup) view);
					else if (value[i].equals("left"))
						setGravity(Gravity.LEFT, (ViewGroup) view);
					else if (value[i].equals("right"))
						setGravity(Gravity.RIGHT, (ViewGroup) view);
					else if (value[i].equals("display_clip_horizontal"))
						setGravity(Gravity.DISPLAY_CLIP_HORIZONTAL,
								(ViewGroup) view);
					else if (value[i].equals("display_clip_vertical"))
						setGravity(Gravity.DISPLAY_CLIP_VERTICAL,
								(ViewGroup) view);
					else if (value[i].equals("clip_horizontal"))
						setGravity(Gravity.CLIP_HORIZONTAL, (ViewGroup) view);
					else if (value[i].equals("center_vertical"))
						setGravity(Gravity.CLIP_VERTICAL, (ViewGroup) view);
					else if (value[i].equals("end"))
						setGravity(Gravity.END, (ViewGroup) view);
					else if (value[i].equals("fill"))
						setGravity(Gravity.FILL, (ViewGroup) view);
					else if (value[i].equals("fill_horizontal"))
						setGravity(Gravity.FILL_HORIZONTAL, (ViewGroup) view);
					else if (value[i].equals("fill_vertical"))
						setGravity(Gravity.FILL_VERTICAL, (ViewGroup) view);
					else if (value[i].equals("top"))
						setGravity(Gravity.TOP, (ViewGroup) view);
					else if (value[i].equals("start"))
						setGravity(Gravity.START, (ViewGroup) view);
					else if (value[i].equals("no_gravity"))
						setGravity(Gravity.NO_GRAVITY, (ViewGroup) view);
					else if (value[i].equals("vertical_gravity_mask"))
						setGravity(Gravity.VERTICAL_GRAVITY_MASK,
								(ViewGroup) view);
					else if (value[i].equals("horizonzal_gravity_mask"))
						setGravity(Gravity.HORIZONTAL_GRAVITY_MASK,
								(ViewGroup) view);
				}
				if (getLayoutTyp() == LayoutTyp.TableLayout) {
					if (attribute[i].equals("android:shrinkColumns")) {
						if (value[i].equals("true"))
							((TableLayout) view).setShrinkAllColumns(true);
					} else if (attribute[i].equals("android:stretchColumns")) {
						if (value[i].equals("true"))
							((TableLayout) view).setStretchAllColumns(true);
					} else if (attribute[i].equals("android:collapseColumns")) {
						// if()
					}
				}
			} else if (attribute[i].equals("android:layout_gravity")) {
				view.getLayoutParams();

				if (value[i].equals("center")) {
					// if(LAYOUT_INDEX == 0)
					// ((LinearLayout.LayoutParams)lp).gravity = Gravity.CENTER;
					setLayoutGravity(Gravity.CENTER, view);
					// else if(LAYOUT_INDEX == 3)
					// ((FrameLayout.LayoutParams)parentLayout).gravity =
					// Gravity.CENTER;
					// view.setLayoutParams(lp);
				} else if (value[i].equals("right")) {
					// if(LAYOUT_INDEX == 0)
					setLayoutGravity(Gravity.RIGHT, view);
					// else if(LAYOUT_INDEX == 3)
					// ((FrameLayout.LayoutParams) lp).gravity = Gravity.RIGHT;
					// view.setLayoutParams(lp);
				} else if (value[i].equals("left")) {
					// if(LAYOUT_INDEX == 0)
					setLayoutGravity(Gravity.LEFT, view);
					// else if(LAYOUT_INDEX == 3)
					// ((FrameLayout.LayoutParams) lp).gravity = Gravity.LEFT;
					// view.setLayoutParams(lp);
				} else if (value[i].equals("bottom")) {
					setLayoutGravity(Gravity.BOTTOM, view);
					// view.setLayoutParams(lp);
				} else if (value[i].equals("top")) {
					setLayoutGravity(Gravity.TOP, view);
					// view.setLayoutParams(lp);
				} else if (value[i].equals("end")) {
					setLayoutGravity(Gravity.END, view);
					// view.setLayoutParams(lp);
				} else if (value[i].equals("start")) {
					setLayoutGravity(Gravity.START, view);
					// view.setLayoutParams(lp);
				} else if (value[i].equals("center_horizontal")) {
					setLayoutGravity(Gravity.CENTER_HORIZONTAL, view);
					// view.setLayoutParams(lp);
				} else if (value[i].equals("center_vertical")) {
					setLayoutGravity(Gravity.CENTER_VERTICAL, view);
					// view.setLayoutParams(lp);
				} else if (value[i].equals("clip_horizontal")) {
					setLayoutGravity(Gravity.CLIP_HORIZONTAL, view);
					// view.setLayoutParams(lp);
				} else if (value[i].equals("clip_vertical")) {
					setLayoutGravity(Gravity.CLIP_VERTICAL, view);
					// view.setLayoutParams(lp);
				} else if (value[i].equals("fill")) {
					setLayoutGravity(Gravity.FILL, view);
					// view.setLayoutParams(lp);
				} else if (value[i].equals("fill_horizontal")) {
					setLayoutGravity(Gravity.FILL_HORIZONTAL, view);
					// view.setLayoutParams(lp);
				} else if (value[i].equals("fill_vertical")) {
					setLayoutGravity(Gravity.FILL_VERTICAL, view);
					// view.setLayoutParams(lp);
				} else if (value[i].equals("display_clip_horizontal")) {
					setLayoutGravity(Gravity.DISPLAY_CLIP_HORIZONTAL, view);
					// view.setLayoutParams(lp);
				} else if (value[i].equals("display_clip_vertical")) {
					setLayoutGravity(Gravity.DISPLAY_CLIP_VERTICAL, view);
					// view.setLayoutParams(lp);
				} else if (value[i].equals("horizontal_gravity_mask")) {
					setLayoutGravity(Gravity.HORIZONTAL_GRAVITY_MASK, view);
					// view.setLayoutParams(lp);
				} else if (value[i].equals("no_gravity")) {
					setLayoutGravity(Gravity.NO_GRAVITY, view);
					// view.setLayoutParams(lp);
				}
				// Relative LayoutParams
			} else if (attribute[i].equals("android:layout_alignParentLeft")) {
				if (attribute[i].equals("true")) {
					setAlginParent(RelativeLayout.ALIGN_PARENT_LEFT, view);
				}
			} else if (attribute[i].equals("android:layout_alignParentTop")) {
				if (attribute[i].equals("true")) {
					setAlginParent(RelativeLayout.ALIGN_PARENT_TOP, view);
				}
			} else if (attribute[i].equals("android:layout_alignParentRight")) {
				if (value[i].equals("true")) {
					setAlginParent(RelativeLayout.ALIGN_PARENT_RIGHT, view);
				}
			} else if (attribute[i]
					.contains("android:layout_alignParentBottom")) {
				if (value[i].equals("true")) {
					setAlginParent(RelativeLayout.ALIGN_PARENT_BOTTOM, view);
				}
			} else if (attribute[i].equals("android:layout_centerVertical")) {
				if (value[i].equals("true")) {
					setAlginParent(RelativeLayout.CENTER_VERTICAL, view);
				}
			} else if (attribute[i].equals("android:layout_alignParentEnd")) {
				if (attribute[i].equals("true")) {
					setAlginParent(RelativeLayout.ALIGN_PARENT_END, view);
				}
			} else if (attribute[i].equals("android:layout_alignParentStart")) {
				if (attribute[i].equals("true")) {
					setAlginParent(RelativeLayout.ALIGN_PARENT_START, view);
				}
			} else if (attribute[i].equals("android:layout_centerInParent")) {
				if (value[i].equals("true")) {
					setAlginParent(RelativeLayout.CENTER_IN_PARENT, view);
				}
			} else if (attribute[i].contains("android:layout_centerVertical")) {
				if (value[i].equals("true")) {
					setAlginParent(RelativeLayout.CENTER_VERTICAL, view);
				}
			}
			/*
			 * else if (attribute[i].equals("android:layout_alignLeft")) { int
			 * id = _xmlmanager.viewIds.get(value[i]);
			 * setParentAlgin(RelativeLayout.ALIGN_LEFT, view, id);
			 * 
			 * } else if(attribute[i].equals("android:layout_toRightOf")) { int
			 * id = _xmlmanager.viewIds.get(value[i]); if(id != 0)
			 * setParentAlgin(RelativeLayout.RIGHT_OF, view, id);
			 * 
			 * }
			 */
			/*
			 * (value[i].equals("true")) {
			 * setAlginParent(RelativeLayout.ALIGN_LEFT, view); } } else if
			 * (attribute[i].equals("android:layout_alignRight")) { if
			 * (value[i].equals("true")) {
			 * setAlginParent(RelativeLayout.ALIGN_RIGHT, view); } } else if
			 * (attribute[i].contains("android:layout_alignStart")) { if
			 * (value[i].equals("true")) {
			 * setAlginParent(RelativeLayout.ALIGN_START, view); } } else if
			 * (attribute[i].equals("android:layout_alignTop")) { if
			 * (value[i].equals("true")) {
			 * setAlginParent(RelativeLayout.ALIGN_TOP, view); } }
			 */

			else if (attribute[i].equals("android:backgroundColor")
					|| attribute[i].equals("android:background")) {
				int c = Color.WHITE;
				if (value[i].contains("#")) {
					// value[i].replace("#", "");
					try {
						c = Color.parseColor("#FFFFFF");
					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					}
					;
				} else if (value[i].contains("@color")) {
					if (value[i].contains("red"))
						c = Color.RED;
					else if (value[i].contains("black"))
						c = Color.BLACK;
					else if (value[i].contains("blue"))
						c = Color.BLUE;
					else if (value[i].contains("yellow"))
						c = Color.YELLOW;
					else if (value[i].contains("green"))
						c = Color.GREEN;
					else if (value[i].contains("grey"))
						c = Color.GRAY;
					else if (value[i].contains("transparent"))
						c = Color.TRANSPARENT;
					else
						c = Color.WHITE;
				} else
					c = Color.WHITE;
				view.setBackgroundColor(c);
			}
			/*
			 * else if(attribute[i].equals("android:id")) {
			 * objectsids.put(Objectid, value[i]); }
			 */
			/*
			 * else if(n[i].equals("android:layout_below")) { for(int
			 * k=0;k<guiActivity.objectsids.size();k++) {
			 * if(v[i].equals(guiActivity.objectsids.get(k))) {
			 * setPosition(RelativeLayout.BELOW, view,
			 * guiActivity.views[k].getId()); break; } } }
			 */

			/*
			 * Views Attributes
			 */
			else if (attribute[i].equals("android:padding")) {
				padding[0] = getPaddingValue(value[i]);
				padding[1] = getPaddingValue(value[i]);
				padding[2] = getPaddingValue(value[i]);
				padding[3] = getPaddingValue(value[i]);
				setPadding = true;
			}
			if (attribute[i].equals("android:paddingLeft")) {
				padding[0] = getPaddingValue(value[i]);
				setPadding = true;
			} else if (attribute[i].equals("android:paddingRight")) {
				padding[1] = getPaddingValue(value[i]);
				setPadding = true;
			} else if (attribute[i].equals("android:paddingTop"))
				padding[2] = getPaddingValue(value[i]);
			else if (attribute[i].equals("android:paddingBottom"))
				padding[3] = getPaddingValue(value[i]);
			else if (attribute[i].equals("android:paddingStart"))
				padding[4] = getPaddingValue(value[i]);
			else if (attribute[i].equals("android:paddingEnd"))
				padding[4] = getPaddingValue(value[i]);
			else if (attribute[i].equals("android:visibility")) {
				if (value[i].equals("gone"))
					view.setVisibility(View.GONE);
				else if (value[i].equals("visible"))
					view.setVisibility(View.VISIBLE);
				else if (value[i].equals("invisible"))
					view.setVisibility(View.INVISIBLE);
			} else if (attribute[i].equals("android:textSize")) {
				setViewsTextSize(view, getSizeValue(value[i]));
			} /*
			 * else if (attribute[i].equals("android:layout_width")) { int w =
			 * getPaddingValue(value[i]); setViewsWidth(view, w); } else if
			 * (attribute[i].equals("android:layout_height")) { int w =
			 * getPaddingValue(value[i]); setViewsHeight(view, w); }
			 */else if (attribute[i].equals("android:alpha")) {
				Float alpha = (float) 1.0;
				alpha = getFloatValue(value[i]);
				view.setAlpha(alpha);
			} else if (attribute[i].equals("android:minHeight")) {
				int mh = getPaddingValue(value[i]);
				view.setMinimumHeight(mh);
			} else if (attribute[i].equals("android:minWidth")) {
				int mw = getPaddingValue(value[i]);
				view.setMinimumWidth(mw);
			} else if (attribute[i].equals("android:alpha")) {
				Float f = (float) 1.0;
				f = getFloatValue(value[i]);
				view.setAlpha(f);
			} else if (attribute[i].equals("android:rotation")) {
				Float f = (float) 1.0;
				f = getFloatValue(value[i]);
				view.setRotation(f);
			} else if (attribute[i].equals("android:rotationX")) {
				Float f = (float) 1.0;
				f = getFloatValue(value[i]);
				view.setRotationX(f);
			} else if (attribute[i].equals("android:rotationY")) {
				Float f = (float) 1.0;
				f = getFloatValue(value[i]);
				view.setRotationY(f);
			} else if (attribute[i].equals("android:scaleX")) {
				Float f = (float) 1.0;
				f = getFloatValue(value[i]);
				view.setScaleX(f);
			} else if (attribute[i].equals("android:scaleY")) {
				Float f = (float) 1.0;
				f = getFloatValue(value[i]);
				view.setScaleY(f);
			} else if (attribute[i].equals("android:scrollX")) {
				int f = 1;
				f = getIntValue(value[i]);
				view.setScrollX(f);
			} else if (attribute[i].equals("android:scrollY")) {
				int f = 1;
				f = getIntValue(value[i]);
				view.setScrollY(f);
			} else if (attribute[i].equals("android:translationX")) {
				Float f = (float) 1.0;
				f = getFloatValue(value[i]);
				view.setTranslationX(f);
			} else if (attribute[i].equals("android:translationY")) {
				Float f = (float) 1.0;
				f = getFloatValue(value[i]);
				view.setTranslationY(f);
			} else if (attribute[i].equals("android:transformPivotX")) {
				Float f = (float) 1.0;
				f = getFloatValue(value[i]);
				view.setPivotX(f);
			} else if (attribute[i].equals("android:transformPivotY")) {
				Float f = (float) 1.0;
				f = getFloatValue(value[i]);
				view.setPivotY(f);
			} else if (attribute[i].equals("android:fadeScrollbars")) {
				if (value[i].equals("true"))
					view.setScrollbarFadingEnabled(true);
				else if (value[i].equals("false"))
					view.setScrollbarFadingEnabled(false);
			} else if (attribute[i].equals("android:isScrollContainer")) {
				if (value[i].equals("true"))
					view.setScrollContainer(true);
				else if (value[i].equals("false"))
					view.setScrollContainer(false);
			} else if (attribute[i].equals("android:keepScreenOn")) {
				if (value[i].equals("true"))
					view.setKeepScreenOn(true);
				else if (value[i].equals("false"))
					view.setKeepScreenOn(false);
			} else if (attribute[i].equals("android:contentDescription")) {
				view.setContentDescription(value[i]);
			} else if (attribute[i].equals("android:fitsSystemWindows")) {
				if (value[i].equals("true"))
					view.setFitsSystemWindows(true);
				else if (value[i].equals("false"))
					view.setFitsSystemWindows(false);
			} else if (attribute[i].equals("android:textAlignment")) {
				if (value[i].equals("inherit"))
					view.setTextAlignment(View.TEXT_ALIGNMENT_INHERIT);
				else if (value[i].equals("gravity"))
					view.setTextAlignment(View.TEXT_ALIGNMENT_GRAVITY);
				else if (value[i].equals("textStart"))
					view.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
				else if (value[i].equals("textEnd"))
					view.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_END);
				else if (value[i].equals("center"))
					view.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
				else if (value[i].equals("viewStart"))
					view.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
				else if (value[i].equals("viewEnd"))
					view.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
				else
					view.setTextAlignment(View.TEXT_ALIGNMENT_INHERIT);
			} else if (attribute[i].equals("android:textDirection")) {
				if (value[i].equals("inherit"))
					view.setTextDirection(View.TEXT_DIRECTION_INHERIT);
				else if (value[i].equals("firstStrong"))
					view.setTextDirection(View.TEXT_DIRECTION_FIRST_STRONG);
				else if (value[i].equals("anyRtl"))
					view.setTextDirection(View.TEXT_DIRECTION_ANY_RTL);
				else if (value[i].equals("rtl"))
					view.setTextDirection(View.TEXT_DIRECTION_RTL);
				else if (value[i].equals("locale"))
					view.setTextDirection(View.TEXT_DIRECTION_LOCALE);
				else
					view.setTextDirection(View.TEXT_DIRECTION_INHERIT);
			} else if (attribute[i].equals("android:maxLines")) {
				WidgetTyp wt = getWidgetTyp();
				if (wt == WidgetTyp.Button || wt == WidgetTyp.TextView
						|| wt == WidgetTyp.CheckBox || wt == WidgetTyp.EditText
						|| wt == WidgetTyp.Switch
						|| wt == WidgetTyp.RadioButton
						|| wt == WidgetTyp.ToggleButton)
					((TextView) view).setMaxLines(getIntValue(value[i]));
			} else if (attribute[i].equals("android:soundEffectsEnabled")) {
				if (value[i].equals("true"))
					view.setSoundEffectsEnabled(true);
				else if (value[i].equals("false"))
					view.setSoundEffectsEnabled(false);
				// TextView Attributes
			} else if (getWidgetTyp() == WidgetTyp.Button
					|| getWidgetTyp() == WidgetTyp.TextView
					|| getWidgetTyp() == WidgetTyp.EditText
					|| getWidgetTyp() == WidgetTyp.CheckBox
					|| getWidgetTyp() == WidgetTyp.RadioButton
					|| getWidgetTyp() == WidgetTyp.Switch
					|| getWidgetTyp() == WidgetTyp.ToggleButton) {

		
				if (attribute[i].equals("android:shadowColor")) {
					try {
						color = Color.parseColor(value[i]);

					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					}
				} else if (attribute[i].equals("android:shadowDx")) {
					dx = getFloatValue(value[i]);
				} else if (attribute[i].equals("android:shadowDy")) {
					dy = getFloatValue(value[i]);
				} else if (attribute[i].equals("android:shadowRadius")) {
					radius = getFloatValue(value[i]);
				} else if (attribute[i].equals("android:textAppearance")) {
					if (value[i].equals("?android:attr/textAppearanceSmall"))
						((TextView) view).setTextAppearance(view.getContext(),
								android.R.style.TextAppearance_Small);
					else if (value[i]
							.equals("?android:attr/textAppearanceMedium"))
						((TextView) view).setTextAppearance(view.getContext(),
								android.R.style.TextAppearance_Medium);
					else if (value[i]
							.equals("?android:attr/textAppearanceLarge"))
						((TextView) view).setTextAppearance(view.getContext(),
								android.R.style.TextAppearance_Large);
				} else if (attribute[i].equals("android:gravity")) {
					if (value[i].equals("center_vertical"))
						((TextView) view).setGravity(Gravity.CENTER_VERTICAL);
					else if (value[i].equals("center_horizontal"))
						((TextView) view).setGravity(Gravity.CENTER_HORIZONTAL);
					else if (value[i].equals("bottom"))
						((TextView) view).setGravity(Gravity.BOTTOM);
					else if (value[i].equals("center"))
						((TextView) view).setGravity(Gravity.CENTER);
					else if (value[i].equals("left"))
						((TextView) view).setGravity(Gravity.LEFT);
					else if (value[i].equals("right"))
						((TextView) view).setGravity(Gravity.RIGHT);
					else if (value[i].equals("display_clip_horizontal"))
						((TextView) view)
								.setGravity(Gravity.DISPLAY_CLIP_HORIZONTAL);
					else if (value[i].equals("display_clip_vertical"))
						((TextView) view)
								.setGravity(Gravity.DISPLAY_CLIP_VERTICAL);
					else if (value[i].equals("clip_horizontal"))
						((TextView) view).setGravity(Gravity.CLIP_HORIZONTAL);
					else if (value[i].equals("center_vertical"))
						((TextView) view).setGravity(Gravity.CLIP_VERTICAL);
					else if (value[i].equals("end"))
						((TextView) view).setGravity(Gravity.END);
					else if (value[i].equals("fill"))
						((TextView) view).setGravity(Gravity.FILL);
					else if (value[i].equals("fill_horizontal"))
						((TextView) view).setGravity(Gravity.FILL_HORIZONTAL);
					else if (value[i].equals("fill_vertical"))
						((TextView) view).setGravity(Gravity.FILL_VERTICAL);
					else if (value[i].equals("top"))
						((TextView) view).setGravity(Gravity.TOP);
					else if (value[i].equals("start"))
						((TextView) view).setGravity(Gravity.START);
					else if (value[i].equals("no_gravity"))
						((TextView) view).setGravity(Gravity.NO_GRAVITY);
					else if (value[i].equals("vertical_gravity_mask"))
						((TextView) view)
								.setGravity(Gravity.VERTICAL_GRAVITY_MASK);
					else if (value[i].equals("horizonzal_gravity_mask"))
						((TextView) view)
								.setGravity(Gravity.HORIZONTAL_GRAVITY_MASK);
				} else if (attribute[i].equals("android:autoLink")) {
					int auto = 0;
					if (value[i].equals("none"))
						auto = 0;
					else if (value[i].equals("web"))
						auto = 1;
					else if (value[i].equals("email"))
						auto = 2;
					else if (value[i].equals("phone"))
						auto = 3;
					else if (value[i].equals("map"))
						auto = 4;
					else if (value[i].equals("all"))
						auto = 5;
					((TextView) view).setAutoLinkMask(auto);
				} else if (attribute[i].equals("android:cursorVisible")) {
					if (value[i].equals("true"))
						((TextView) view).setCursorVisible(true);
					else if (value[i].equals("false"))
						((TextView) view).setCursorVisible(false);
				} else if (attribute[i].equals("android:ems")) {
					((TextView) view).setEms(getIntValue(value[i]));
				} else if (attribute[i].equals("android:fontFamily")) {
					Typeface type = Typeface.DEFAULT;
					// Typeface typ = Typeface.create(null, Typ);
					if (value[i].equals("default_bold"))
						type = Typeface.DEFAULT_BOLD;
					else if (value[i].equals("monospace"))
						type = Typeface.MONOSPACE;
					else if (value[i].equals("sans_serif"))
						type = Typeface.SANS_SERIF;
					else if (value[i].equals("serif"))
						type = Typeface.SERIF;
					((TextView) view).setTypeface(type);
				} else if (attribute[i].equals("android:freezesText")) {
					if (value[i].equals("true"))
						((TextView) view).setFreezesText(true);
					else if (value[i].equals("false"))
						((TextView) view).setFreezesText(false);
				} else if (attribute[i].equals("android:height")) {
					((TextView) view).setHeight(getIntValue(value[i]));
				} else if (attribute[i].equals("android:hint")) {
					((TextView) view).setHint(value[i]);
				} else if (attribute[i].equals("android:includeFontPadding")) {
					if (value[i].equals("true"))
						((TextView) view).setIncludeFontPadding(true);
					else if (value[i].equals("false"))
						((TextView) view).setIncludeFontPadding(false);
				} else if (attribute[i].equals("android:inputType")) {
					// To DO
				} else if (attribute[i].equals("android:lines")) {
					((TextView) view).setLines(getIntValue(value[i]));
				} else if (attribute[i].equals("android:maxEms")) {
					((TextView) view).setMaxEms(getIntValue(value[i]));
				} else if (attribute[i].equals("android:maxHeight")) {
					((TextView) view).setMaxHeight(getIntValue(value[i]));
				} else if (attribute[i].equals("android:maxLines")) {
					((TextView) view).setMaxLines(getIntValue(value[i]));
				} else if (attribute[i].equals("android:maxWidth")) {
					((TextView) view).setMaxWidth(getIntValue(value[i]));
				} else if (attribute[i].equals("android:minEms")) {
					((TextView) view).setMinEms(getIntValue(value[i]));
				} else if (attribute[i].equals("android:minHeight")) {
					((TextView) view).setMinHeight(getIntValue(value[i]));
				} else if (attribute[i].equals("android:minLines")) {
					((TextView) view).setMinLines(getIntValue(value[i]));
				} else if (attribute[i].equals("android:minWidth")) {
					((TextView) view).setMinHeight(getIntValue(value[i]));
				} else if (attribute[i].equals("android:digits")) {
					if (value[i].equals("true"))
						((TextView) view).setKeyListener(DigitsKeyListener
								.getInstance("0123456789"));
				} else if (attribute[i].equals("android:capitalize")) {
					if (value[i].equals("true"))
						((TextView) view)
								.setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
				} else if (attribute[i].equals("android:password")) {
					if (value[i].equals("true"))
						((TextView) view)
								.setTransformationMethod(PasswordTransformationMethod
										.getInstance());
				} else if (attribute[i].equals("android:numeric")) {
					if (value[i].equals("true"))
						((TextView) view)
								.setInputType(InputType.TYPE_CLASS_NUMBER
										| InputType.TYPE_NUMBER_FLAG_DECIMAL
										| InputType.TYPE_NUMBER_FLAG_SIGNED);
				} else if (attribute[i].equals("android:scrollHorizontally")) {
					if (value[i].equals("true"))
						((TextView) view).setHorizontallyScrolling(true);
					else if (value[i].equals("false"))
						((TextView) view).setHorizontallyScrolling(false);
				} else if (attribute[i].equals("android:selectAllOnFocus")) {
					if (value[i].equals("true"))
						((TextView) view).setSelectAllOnFocus(true);
					else if (value[i].equals("false"))
						((TextView) view).setSelectAllOnFocus(false);
				} else if (attribute[i].equals("android:textColor")) {
					int c = Color.BLACK;
					if (value[i].contains("#")) {
						try {
							c = Color.parseColor(value[i]);
						} catch (IllegalArgumentException e) {
							e.printStackTrace();
						}
						;
					} else if (value[i].contains("@color")) {
						if (value[i].contains("red"))
							c = Color.RED;
						else if (value[i].contains("black"))
							c = Color.BLACK;
						else if (value[i].contains("blue"))
							c = Color.BLUE;
						else if (value[i].contains("yellow"))
							c = Color.YELLOW;
						else if (value[i].contains("green"))
							c = Color.GREEN;
						else if (value[i].contains("gray"))
							c = Color.GRAY;
						else if (value[i].contains("transparent"))
							c = Color.TRANSPARENT;
						else
							c = Color.WHITE;
					}
					// setViewsTextColor(view, c);
					((TextView) view).setTextColor(c);
				} else if (attribute[i].equals("android:textScaleX")) {
					((TextView) view).setTextScaleX(getFloatValue(value[i]));
				} else if (attribute[i].equals("android:textColorHighlight")) {
					try {
						((TextView) view).setHighlightColor(Color
								.parseColor(value[i]));

					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					}
				} else if (attribute[i].equals("android:textColorHint")) {
					try {
						((TextView) view).setHintTextColor(Color
								.parseColor(value[i]));

					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					}
				} else if (attribute[i].equals("android:textColorLink")) {
					try {
						((TextView) view).setLinkTextColor(Color
								.parseColor(value[i]));

					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					}
				} else if (attribute[i].equals("android:width")) {
					((TextView) view).setWidth(getIntValue(value[i]));
				} else if (attribute[i].equals("android:textAllCaps")) {
					if (value[i].equals("true"))
						((TextView) view).setAllCaps(true);
					else if (value[i].equals("false"))
						((TextView) view).setAllCaps(false);
				}
				((TextView) view).setShadowLayer(radius, dx, dy, color);
			} else if (getWidgetTyp() == WidgetTyp.Spinner) {
				if (attribute[i].equals("android:dropDownHorizontalOffset")) {
					((Spinner) view)
							.setDropDownHorizontalOffset(getIntValue(value[i]));
				} else if (attribute[i]
						.equals("android:dropDownVerticalOffset")) {
					((Spinner) view)
							.setDropDownVerticalOffset(getIntValue(value[i]));
				} else if (attribute[i].equals("android:dropDownWidth")) {
					((Spinner) view).setDropDownWidth(getIntValue(value[i]));
				}
			} else if ((getWidgetTyp() == WidgetTyp.ProgressBar
					|| getWidgetTyp() == WidgetTyp.RatingBar || getWidgetTyp() == WidgetTyp.SeekBar)) {

				if (attribute[i].equals("android:progress")) {
					((ProgressBar) view).setProgress(getIntValue(value[i]));
				} else if (attribute[i].equals("android:indeterminate")) {
					if (value[i].equals("true"))
						((ProgressBar) view).setIndeterminate(true);
				} else if (attribute[i].equals("android:max")) {
					((ProgressBar) view).setMax(getIntValue(value[i]));
				} else if (attribute[i].equals("android:secondaryProgress")) {
					((ProgressBar) view)
							.setSecondaryProgress(getIntValue(value[i]));
				}
			} else if ((getWidgetTyp() == WidgetTyp.ImageView || getWidgetTyp() == WidgetTyp.ImageButton)) {
				if (attribute[i].equals("android:baseline")) {
					((ImageView) view).setBaseline(getIntValue(value[i]));
				} else if (attribute[i].equals("android:baselineAlignBottom")) {
					if (value[i].equals("true")) {
						((ImageView) view).setBaselineAlignBottom(true);
					}
				} else if (attribute[i].equals("android:cropToPadding")) {
					if (value[i].equals("true")) {
						((ImageView) view).setCropToPadding(true);
					}
				} else if (attribute[i].equals("android:maxHeight")) {
					((ImageView) view).setMaxHeight(getIntValue(value[i]));
				} else if (attribute[i].equals("android:maxWidth")) {
					((ImageView) view).setMaxWidth(getIntValue(value[i]));
				}
			} else if ((getWidgetTyp() == WidgetTyp.DatePicker)) {
				if (attribute[i].equals("android:calendarViewShown")) {
					if (value[i].equals("true")) {
						((DatePicker) view).setCalendarViewShown(true);
					}
				} else if (attribute[i].equals("android:maxDate")) {
					((DatePicker) view).setMaxDate(getDateValue(value[i]));
				} else if (attribute[i].equals("android:minDate")) {
					((DatePicker) view).setMinDate(getDateValue(value[i]));
				} else if (attribute[i].equals("android:spinnersShown")) {
					if (value[i].equals("true")) {
						((DatePicker) view).setSpinnersShown(true);
					} else if (value[i].equals("false")) {
						((DatePicker) view).setSpinnersShown(false);
					}
				}

			}
			if ((layoutTyp == LayoutTyp.FrameLayout || getLayoutTyp() == LayoutTyp.FrameLayout)
					|| getWidgetTyp() == WidgetTyp.DatePicker
					|| getWidgetTyp() == WidgetTyp.TimerPicker
					|| getLayoutTyp() == LayoutTyp.ScrollView
					|| getLayoutTyp() == LayoutTyp.HorizontalScrollView) {
				if (attribute[i].equals("android:foregroundGravity")) {
					if (value[i].equals("center_vertical"))
						((FrameLayout) view)
								.setForegroundGravity(Gravity.CENTER_VERTICAL);
					else if (value[i].equals("center_horizontal"))
						((FrameLayout) view)
								.setForegroundGravity(Gravity.CENTER_HORIZONTAL);
					else if (value[i].equals("bottom"))
						((FrameLayout) view)
								.setForegroundGravity(Gravity.BOTTOM);
					else if (value[i].equals("center"))
						((FrameLayout) view)
								.setForegroundGravity(Gravity.CENTER);
					else if (value[i].equals("left"))
						((FrameLayout) view).setForegroundGravity(Gravity.LEFT);
					else if (value[i].equals("right"))
						((FrameLayout) view)
								.setForegroundGravity(Gravity.RIGHT);
					else if (value[i].equals("display_clip_horizontal"))
						((FrameLayout) view)
								.setForegroundGravity(Gravity.DISPLAY_CLIP_HORIZONTAL);
					else if (value[i].equals("display_clip_vertical"))
						((FrameLayout) view)
								.setForegroundGravity(Gravity.DISPLAY_CLIP_VERTICAL);
					else if (value[i].equals("clip_horizontal"))
						((FrameLayout) view)
								.setForegroundGravity(Gravity.CLIP_HORIZONTAL);
					else if (value[i].equals("center_vertical"))
						((FrameLayout) view)
								.setForegroundGravity(Gravity.CLIP_VERTICAL);
					else if (value[i].equals("end"))
						((FrameLayout) view).setForegroundGravity(Gravity.END);
					else if (value[i].equals("fill"))
						((FrameLayout) view).setForegroundGravity(Gravity.FILL);
					else if (value[i].equals("fill_horizontal"))
						((FrameLayout) view)
								.setForegroundGravity(Gravity.FILL_HORIZONTAL);
					else if (value[i].equals("fill_vertical"))
						((FrameLayout) view)
								.setForegroundGravity(Gravity.FILL_VERTICAL);
					else if (value[i].equals("top"))
						((FrameLayout) view).setForegroundGravity(Gravity.TOP);
					else if (value[i].equals("start"))
						((FrameLayout) view)
								.setForegroundGravity(Gravity.START);
					else if (value[i].equals("no_gravity"))
						((FrameLayout) view)
								.setForegroundGravity(Gravity.NO_GRAVITY);
					else if (value[i].equals("vertical_gravity_mask"))
						((FrameLayout) view)
								.setForegroundGravity(Gravity.VERTICAL_GRAVITY_MASK);
					else if (value[i].equals("horizonzal_gravity_mask"))
						((FrameLayout) view)
								.setForegroundGravity(Gravity.HORIZONTAL_GRAVITY_MASK);
				}

			} else if (attribute[i].equals("android:fillViewport")) {
				if (getLayoutTyp() == LayoutTyp.ScrollView
						|| getLayoutTyp() == LayoutTyp.HorizontalScrollView) {
					if (value[i].equals("true"))
						((ScrollView) view).setFillViewport(true);
					else if (value[i].equals("false"))
						((ScrollView) view).setFillViewport(false);
				}

			}

			/*
			 * else if(getLayoutTyp() == LayoutTyp.ButtonBar)) {
			 * if(attribute[i].equals("style")) {
			 * if(value[i].equals("?android:attr/buttonBarStyle"))
			 * ((LinearLayout) view).set } }
			 */

		}
		if (setPadding == true)
			view.setPadding(padding[0], padding[2], padding[1], padding[3]);
		else
			view.setPaddingRelative(padding[4], padding[2], padding[5],
					padding[3]);

	}

	@SuppressLint("NewApi")
	public void setShadowLayer(TextView view) {
		Float dx = view.getShadowDx();
		Float dy = view.getShadowDy();
		Float radius = view.getShadowRadius();
		int color = view.getShadowColor();

		for (int i = 0; i < attribute.length; i++) {
			if (attribute[i].equals("android:shadowColor")) {
				try {
					color = Color.parseColor(value[i]);

				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				}
			} else if (attribute[i].equals("android:shadowDx")) {
				dx = getFloatValue(value[i]);
			} else if (attribute[i].equals("android:shadowDy")) {
				dy = getFloatValue(value[i]);
			} else if (attribute[i].equals("android:shadowRadius")) {
				radius = getFloatValue(value[i]);
			}
		}

		view.setShadowLayer(radius, dx, dy, color);
	}

	@SuppressLint("InlinedApi")
	public void setAlgin(View view) {
		for (int i = 0; i < attribute.length; i++) {
			if (layoutTyp == LayoutTyp.RelativeLayout) {
				if (attribute[i].equals("android:layout_alignLeft")) {
					value[i] = value[i].trim();
					if(!value[i].isEmpty()) {
						int id = _xmlmanager.viewIds.get(value[i]);
						if (id != 0)
							setParentAlgin(RelativeLayout.ALIGN_LEFT, view, id);
					}
				}
				else if (attribute[i].equals("android:layout_alignRight")) {
					value[i] = value[i].trim();
					if(!value[i].isEmpty()) {
						int id = _xmlmanager.viewIds.get(value[i]);
						if (id != 0)
							setParentAlgin(RelativeLayout.ALIGN_RIGHT, view, id);
					}
				}
				else if (attribute[i].equals("android:layout_alignBaseline")) {
					value[i] = value[i].trim();
					if(!value[i].isEmpty()) {
						int id = _xmlmanager.viewIds.get(value[i]);
						if (id != 0)
							setParentAlgin(RelativeLayout.ALIGN_BASELINE, view, id);
					}
				}
				else if (attribute[i].equals("android:layout_alignEnd")) {
					value[i] = value[i].trim();
					if(!value[i].isEmpty()) {
						int id = _xmlmanager.viewIds.get(value[i]);
						if (id != 0)
							setParentAlgin(RelativeLayout.ALIGN_END, view, id);
					}
				}
				else if (attribute[i].equals("android:layout_alignStart")) {
					value[i] = value[i].trim();
					if(!value[i].isEmpty()) {
						int id = _xmlmanager.viewIds.get(value[i]);
						if (id != 0)
							setParentAlgin(RelativeLayout.ALIGN_START, view, id);
					}
				}
				else if (attribute[i].equals("android:layout_alignTop")) {
					value[i] = value[i].trim();
					if(!value[i].isEmpty()) {
						int id = _xmlmanager.viewIds.get(value[i]);
						if (id != 0)
							setParentAlgin(RelativeLayout.ALIGN_TOP, view, id);
					}
				}
				else if (attribute[i].equals("android:layout_alignBottom")) {
					value[i] = value[i].trim();
					if(!value[i].isEmpty()) {
						int id = _xmlmanager.viewIds.get(value[i]);
						if (id != 0)
							setParentAlgin(RelativeLayout.ALIGN_BOTTOM, view, id);
					}
				}
				else if (attribute[i].equals("android:layout_toRightOf")) {
					value[i] = value[i].trim();
					if(!value[i].isEmpty()) {
						int id = _xmlmanager.viewIds.get(value[i]);
						if (id != 0)
							setParentAlgin(RelativeLayout.RIGHT_OF, view, id);
					}
				}
				else if (attribute[i].equals("android:layout_toLeftOf")) {
					value[i] = value[i].trim();
					if(!value[i].isEmpty()) {
						int id = _xmlmanager.viewIds.get(value[i]);
						if (id != 0)
							setParentAlgin(RelativeLayout.LEFT_OF, view, id);
					}
				}
				else if (attribute[i].equals("android:layout_toEndOf")) {
					value[i] = value[i].trim();
					if(!value[i].isEmpty()) {
						int id = _xmlmanager.viewIds.get(value[i]);
						if (id != 0)
							setParentAlgin(RelativeLayout.END_OF, view, id);
					}
				}
				else if (attribute[i].equals("android:layout_toStartOf")) {
					value[i] = value[i].trim();
					if(!value[i].isEmpty()) {
						int id = _xmlmanager.viewIds.get(value[i]);
						if (id != 0)
							setParentAlgin(RelativeLayout.START_OF, view, id);
					}
				}
			}
		}
	}

	@SuppressLint("NewApi")
	public void setMargins(View view) {

		int left = 0;
		int top = 0;
		int right = 0;
		int bottom = 0;
		int margin = 0;

		for (int i = 0; i < attribute.length; i++) {

			if (attribute[i].equals("android:layout_margin")) {
				margin = getPaddingValue(value[i]);
				left = margin;
				top = margin;
				right = margin;
				bottom = margin;
			} else if (attribute[i].equals("android:layout_marginLeft")) {
				left = getPaddingValue(value[i]);
			} else if (attribute[i].equals("android:layout_marginRight")) {
				right = getPaddingValue(value[i]);
			} else if (attribute[i].equals("android:layout_marginTop")) {
				top = getPaddingValue(value[i]);
			} else if (attribute[i].equals("android:layout_marginBottom")) {
				bottom = getPaddingValue(value[i]);
			} else if (attribute[i].equals("android:layout_marginStart")) {
				getPaddingValue(value[i]);
			} else if (attribute[i].equals("android:layout_marginEnd")) {
				getPaddingValue(value[i]);
			}
		}
		if (layoutTyp == LayoutTyp.LinearLayout) {
			LinearLayout.LayoutParams layp = (LinearLayout.LayoutParams) view
					.getLayoutParams();
			LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
					layp.width, layp.height, layp.weight);

			lp.setMargins(left, top, right, bottom);
			lp.gravity = layp.gravity;

			view.setLayoutParams(lp);
		} else if (layoutTyp == LayoutTyp.RelativeLayout) {
			RelativeLayout.LayoutParams layp = (RelativeLayout.LayoutParams) view
					.getLayoutParams();
			RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
					layp.width, layp.height);

			lp.setMargins(left, top, right, bottom);

			view.setLayoutParams(lp);
		}

		// else if(layoutTyp == LayoutTyp.RelativeLayout)
		// ((RelativeLayout.LayoutParams)lp).gravity = Gravity.NO_GRAVITY;

		// if(setPadding == true)
		// lp.setMargins(left, top, right, bottom);
		// view.setLayoutParams(lp);
	}

	public Boolean getBoolValue(String s) {
		Boolean b = true;
		if (s.equals("false"))
			b = false;

		return b;
	}

	@SuppressLint("SimpleDateFormat")
	public long getDateValue(String s) {
		long Longdate = 0;
		SimpleDateFormat format = new SimpleDateFormat("mm-dd-yyyy");
		try {
			Date date = format.parse(s);
			Longdate = date.getTime();

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Longdate;
	}

	public Integer getIntValue(String s) {
		Integer f = 1;
		try {
			f = Integer.parseInt(s);
		} catch (NumberFormatException e) {
			return f;
		}
		return f;
	}

	public Float getFloatValue(String s) {
		Float f = (float) 0.0;
		try {
			f = Float.parseFloat(s);
		} catch (NumberFormatException e) {
			return f;
		}
		return f;
	}

	public Float getSizeValue(String s) {
		Float v = (float) 18.0;
		try {
			if (s.contains("sp")) {
				String f = s.replace("sp", "");
				v = Float.parseFloat(f);
			} else
				v = Float.parseFloat(s);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}

		return v;
	}

	@SuppressLint("NewApi")
	public void setPadding(View view) {
		int padding[] = new int[6];

		padding[0] = view.getPaddingLeft();
		padding[1] = view.getPaddingRight();
		padding[2] = view.getPaddingTop();
		padding[3] = view.getPaddingBottom();
		padding[4] = view.getPaddingStart();
		padding[5] = view.getPaddingEnd();

		Boolean setPadding = false;

		for (int i = 0; i < attribute.length; i++) {
			if (attribute[i].equals("android:padding")) {
				padding[0] = getPaddingValue(value[i]);
				padding[1] = getPaddingValue(value[i]);
				padding[2] = getPaddingValue(value[i]);
				padding[3] = getPaddingValue(value[i]);
				setPadding = true;
			}
			if (attribute[i].equals("android:paddingLeft")) {
				padding[0] = getPaddingValue(value[i]);
				setPadding = true;
			} else if (attribute[i].equals("android:paddingRight")) {
				padding[1] = getPaddingValue(value[i]);
				setPadding = true;
			} else if (attribute[i].equals("android:paddingTop"))
				padding[2] = getPaddingValue(value[i]);
			else if (attribute[i].equals("android:paddingBottom"))
				padding[3] = getPaddingValue(value[i]);
			else if (attribute[i].equals("android:paddingStart"))
				padding[4] = getPaddingValue(value[i]);
			else if (attribute[i].equals("android:paddingEnd"))
				padding[4] = getPaddingValue(value[i]);
		}
		if (setPadding == true)
			view.setPadding(padding[0], padding[2], padding[1], padding[3]);
		else
			view.setPaddingRelative(padding[4], padding[2], padding[5],
					padding[3]);
	}

	/*
	 * public void setPadding(ViewGroup vg) { int padding[] = new int[4];
	 * 
	 * padding[0] = vg.getPaddingLeft(); padding[1] = vg.getPaddingRight();
	 * padding[2] = vg.getPaddingTop(); padding[3] = vg.getPaddingBottom();
	 * 
	 * for (int i = 0; i < attribute.length; i++) { if
	 * (attribute[i].equals("android:paddingLeft")) padding[0] =
	 * getPaddingValue(value[i]); else if
	 * (attribute[i].equals("android:paddingRight")) padding[1] =
	 * getPaddingValue(value[i]); else if
	 * (attribute[i].equals("android:paddingTop")) padding[2] =
	 * getPaddingValue(value[i]); else if
	 * (attribute[i].equals("android:paddingBottom")) padding[3] =
	 * getPaddingValue(value[i]); } vg.setPadding(padding[0], padding[2],
	 * padding[1], padding[3]);
	 * 
	 * }
	 */

	// verbesrn
	public int getPaddingValue(String s) {
		int value = 1;
		try {
			if (s.contains("dip")) {
				String f = s.replace("dip", "");
				value = Integer.parseInt(f);
			} else if (s.contains("dp")) {
				String f = s.replace("dp", "");
				value = Integer.parseInt(f);
			} else
				value = Integer.parseInt(s);
		} catch (NumberFormatException e) {
			e.printStackTrace();
	//		MainActivity.PARSE_ERROR.add(e.getMessage());
		}
		return value;
	}

	public void setViewsTextColor(View view, int color) {
		if (object.equals("Button"))
			((Button) view).setTextColor(color);
		else if (object.equals("EditText"))
			((EditText) view).setTextColor(color);
		else if (object.equals("TextView"))
			((TextView) view).setTextColor(color);
		else if (object.equals("RadioButton"))
			((RadioButton) view).setTextColor(color);
		else if (object.equals("CheckBox"))
			((CheckBox) view).setTextColor(color);
		else if (object.equals("ToggleButton"))
			((ToggleButton) view).setTextColor(color);
		else
			return;

	}

	public void setViewsWidth(View view, int width) {
		if (object.equals("Button"))
			((Button) view).setWidth(width);
		else if (object.equals("EditText"))
			((EditText) view).setWidth(width);
		else if (object.equals("TextView"))
			((TextView) view).setWidth(width);
		else if (object.equals("RadioButton"))
			((RadioButton) view).setWidth(width);
		else if (object.equals("CheckBox"))
			((CheckBox) view).setWidth(width);
		else if (object.equals("ToggleButton"))
			((ToggleButton) view).setWidth(width);
		else
			return;

	}

	public void setViewsHeight(View view, int height) {
		if (object.equals("Button"))
			((Button) view).setHeight(height);
		else if (object.equals("EditText"))
			((EditText) view).setHeight(height);
		else if (object.equals("TextView"))
			((TextView) view).setHeight(height);
		else if (object.equals("RadioButton"))
			((RadioButton) view).setHeight(height);
		else if (object.equals("CheckBox"))
			((CheckBox) view).setHeight(height);
		else if (object.equals("ToggleButton"))
			((ToggleButton) view).setHeight(height);
		else
			return;

	}

	public void setViewsTextSize(View view, Float size) {
		if (object.equals("Button"))
			((Button) view).setTextSize(size);
		else if (object.equals("EditText"))
			((EditText) view).setTextSize(size);
		else if (object.equals("TextView"))
			((TextView) view).setTextSize(size);
		else if (object.equals("RadioButton"))
			((RadioButton) view).setTextSize(size);
		else if (object.equals("CheckBox"))
			((CheckBox) view).setTextSize(size);
		else if (object.equals("ToggleButton"))
			((ToggleButton) view).setTextSize(size);
		else
			return;

	}

	public void setLayoutAttributes(ViewGroup vg) {

		for (int i = 0; i < attribute.length; i++) {
			if (attribute[i].equals("android:orientation")) {
				if (value[i].equals("vertical")) {
					if (getLayoutTyp() == LayoutTyp.LinearLayout)
						((LinearLayout) vg).setOrientation(1);
				} else if (value[i].equals("horizontal")) {
					if (getLayoutTyp() == LayoutTyp.LinearLayout)
						((LinearLayout) vg).setOrientation(0);
				}
			} else if (attribute[i].equals("android:layout_gravity")
			/* || attribute[i].equals("android:gravity") */) {
				if (value[i].equals("center")) {
					setLayoutGravity(Gravity.CENTER, vg);
				} else if (value[i].equals("center_horizontal")) {
					setLayoutGravity(Gravity.CENTER_HORIZONTAL, vg);
				} else if (value[i].equals("bottom")) {
					setLayoutGravity(Gravity.BOTTOM, vg);
				} else if (value[i].equals("top")) {
					setLayoutGravity(Gravity.TOP, vg);
				}

			} else if (attribute[i].equals("android:gravity")) {
				if (value[i].equals("center_vertical"))
					setGravity(Gravity.CENTER_VERTICAL, vg);
				else if (value[i].equals("center_horizontal"))
					setGravity(Gravity.CENTER_HORIZONTAL, vg);
				else if (value[i].equals("bottom"))
					setGravity(Gravity.BOTTOM, vg);
				else if (value[i].equals("center"))
					setGravity(Gravity.CENTER, vg);
				else if (value[i].equals("left"))
					setGravity(Gravity.LEFT, vg);
				else if (value[i].equals("right"))
					setGravity(Gravity.RIGHT, vg);
				else if (value[i].equals("display_clip_horizontal"))
					setGravity(Gravity.DISPLAY_CLIP_HORIZONTAL, vg);
				else if (value[i].equals("display_clip_vertical"))
					setGravity(Gravity.DISPLAY_CLIP_VERTICAL, vg);
				else if (value[i].equals("clip_horizontal"))
					setGravity(Gravity.CLIP_HORIZONTAL, vg);
				else if (value[i].equals("center_vertical"))
					setGravity(Gravity.CLIP_VERTICAL, vg);
				else if (value[i].equals("end"))
					setGravity(Gravity.END, vg);
				else if (value[i].equals("fill"))
					setGravity(Gravity.FILL, vg);
				else if (value[i].equals("fill_horizontal"))
					setGravity(Gravity.FILL_HORIZONTAL, vg);
				else if (value[i].equals("fill_vertical"))
					setGravity(Gravity.FILL_VERTICAL, vg);
				else if (value[i].equals("top"))
					setGravity(Gravity.TOP, vg);
				else if (value[i].equals("start"))
					setGravity(Gravity.START, vg);
				else if (value[i].equals("no_gravity"))
					setGravity(Gravity.NO_GRAVITY, vg);
				else if (value[i].equals("vertical_gravity_mask"))
					setGravity(Gravity.VERTICAL_GRAVITY_MASK, vg);
				else if (value[i].equals("horizonzal_gravity_mask"))
					setGravity(Gravity.HORIZONTAL_GRAVITY_MASK, vg);
				// Relative Layout Layout Params
			} else if (attribute[i].equals("android:layout_alignParentLeft")) {
				if (value[i].equals("true"))
					setAlginParent(RelativeLayout.ALIGN_PARENT_LEFT, vg);
			} else if (attribute[i].equals("android:layout_alignParentTop")) {
				if (value[i].equals("true"))
					setAlginParent(RelativeLayout.ALIGN_PARENT_TOP, vg);
			} else if (attribute[i].equals("android:layout_alignnParentRight")) {
				if (value[i].equals("true"))
					setAlginParent(RelativeLayout.ALIGN_PARENT_RIGHT, vg);
			} else if (attribute[i].equals("android:layout_alignParentBottom")) {
				if (value[i].equals("true"))
					setAlginParent(RelativeLayout.ALIGN_PARENT_BOTTOM, vg);
			} else if (attribute[i].equals("android:backgroundColor")) {
				int c = Color.WHITE;
				if (value[i].contains("#")) {
					try {
						c = Color.parseColor(value[i]);
					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					}
					;
				} else if (value[i].contains("@color")) {
					if (value[i].contains("red"))
						c = Color.RED;
					else if (value[i].contains("black"))
						c = Color.BLACK;
					else if (value[i].contains("blue"))
						c = Color.BLUE;
					else if (value[i].contains("yellow"))
						c = Color.YELLOW;
					else if (value[i].contains("green"))
						c = Color.GREEN;
					else if (value[i].contains("grey"))
						c = Color.GRAY;
					else if (value[i].contains("transparent"))
						c = Color.TRANSPARENT;
					else
						c = Color.WHITE;
				}
				vg.setBackgroundColor(c);
			} else if (attribute[i].equals("android:clipChildren")) {
				if (value[i].equals("true"))
					vg.setClipChildren(true);
				else if (value[i].equals("false"))
					vg.setClipChildren(false);
			} else if (attribute[i].equals("android:clipToPadding")) {
				if (value[i].equals("true"))
					vg.setClipToPadding(true);
				else if (value[i].equals("false"))
					vg.setClipToPadding(false);
			} else if (attribute[i].equals("android:layout_margin")) {
				LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) vg
						.getLayoutParams();
				int margin = getPaddingValue(value[i]);
				lp.setMargins(margin, margin, margin, margin);
				vg.setLayoutParams(lp);
			}

		}
		setPadding(vg);
	}

	public void setPosition(int a, ViewGroup vg, int element) {
		if (layoutTyp == LayoutTyp.RelativeLayout)
			((RelativeLayout.LayoutParams) vg.getLayoutParams()).addRule(a,
					Objectid);
		// guiActivity.t.show();
	}

	public void setPosition(int a, View view, int element) {
		if (layoutTyp == LayoutTyp.RelativeLayout)
			((RelativeLayout.LayoutParams) view.getLayoutParams()).addRule(a,
					Objectid);
		// guiActivity.t.show();

	}

	public void setAlginParent(int a, ViewGroup vg) {
		if (layoutTyp == LayoutTyp.RelativeLayout)
			((RelativeLayout.LayoutParams) vg.getLayoutParams()).addRule(a,
					RelativeLayout.TRUE);
	}

	public void setAlginParent(int a, View v) {
		if (layoutTyp == LayoutTyp.RelativeLayout)
			((RelativeLayout.LayoutParams) v.getLayoutParams()).addRule(a,
					RelativeLayout.TRUE);
	}

	public void setParentAlgin(int a, View view, int id) {
		if (layoutTyp == LayoutTyp.RelativeLayout) {
			RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) view
					.getLayoutParams();
			try {
				lp.addRule(a, id);
			} catch (ArrayIndexOutOfBoundsException e) {
				e.printStackTrace();
			}
			view.setLayoutParams(lp);
		}
	}

	public void setLayoutGravity(int g, View vg) {
		LayoutParams lp = vg.getLayoutParams();

		if (layoutTyp == LayoutTyp.FrameLayout) {
			((FrameLayout.LayoutParams) lp).gravity = g;
		} else if (layoutTyp == LayoutTyp.TableLayout) {
			((TableLayout.LayoutParams) lp).gravity = g;
		} else if (layoutTyp == LayoutTyp.TableRow) {
			((TableRow.LayoutParams) lp).gravity = g;
		} else if (layoutTyp == LayoutTyp.LinearLayout) {
			((LinearLayout.LayoutParams) lp).gravity = g;
			vg.setLayoutParams(lp);
		} else
			return;
	}

	public void setGravity(int g, ViewGroup vg) {
		if (getLayoutTyp() == LayoutTyp.LinearLayout) {
			((LinearLayout) vg).setGravity(g);
		} else if (getLayoutTyp() == LayoutTyp.RelativeLayout) {
			((RelativeLayout) vg).setGravity(g);
		} else if (getLayoutTyp() == LayoutTyp.TableLayout) {
			((TableLayout) vg).setGravity(g);
		} else if (getLayoutTyp() == LayoutTyp.TableRow) {
			((TableRow) vg).setGravity(g);
		} else {
			return;
		}
	}

	@TargetApi(14)
	public void setParentsLayoutParams(View v) {
		Integer[] i = getLayoutParams();
		String root = "LinearLayout";
		if (Objectid != 0)
			root = _xmlmanager.objects.get(_xmlmanager.parent.get(Objectid));

		if (root == "#document") {
			parentLayout = new LinearLayout.LayoutParams(i[0], i[1]);
			v.setLayoutParams(parentLayout);
			layoutTyp = LayoutTyp.LinearLayout;
		} else if (root.equals("LinearLayout")) {
			parentLayout = new LinearLayout.LayoutParams(i[0], i[1]);
			v.setLayoutParams(parentLayout);
			layoutTyp = LayoutTyp.LinearLayout;
			LAYOUT_INDEX = 0;
		} else if (root.equals("RelativeLayout")) {
			parentLayout = new RelativeLayout.LayoutParams(i[0], i[1]);
			v.setLayoutParams(parentLayout);
			layoutTyp = LayoutTyp.RelativeLayout;
			LAYOUT_INDEX = 1;
		} else if (root.equals("GridLayout")) {
			parentLayout = new GridLayout.LayoutParams();
			v.setLayoutParams(parentLayout);
			layoutTyp = LayoutTyp.GridLayout;
			LAYOUT_INDEX = 5;
		} else if (root.equals("FrameLayout")) {
			parentLayout = new FrameLayout.LayoutParams(i[0], i[1]);
			v.setLayoutParams(parentLayout);
			layoutTyp = LayoutTyp.FrameLayout;
			LAYOUT_INDEX = 2;

		} else if (root.equals("TableLayout")) {
			parentLayout = new TableLayout.LayoutParams(i[0], i[1]);
			v.setLayoutParams(parentLayout);
			layoutTyp = LayoutTyp.TableLayout;
			LAYOUT_INDEX = 3;
		} else if (root.equals("TableRow")) {
			parentLayout = new TableRow.LayoutParams(i[0], i[1]);
			v.setLayoutParams(parentLayout);
			layoutTyp = LayoutTyp.TableRow;
			LAYOUT_INDEX = 4;
		} else {
			// TODO
			v.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.MATCH_PARENT));
		}
	}

	@SuppressLint({ "NewApi", "NewApi" })
	public View makeObject(Context c) {
		View v;

		if (getWidgetTyp() == WidgetTyp.SeekBar) {
			sb = new SeekBar(c);
			// sb.setId(Objectid);
			setParentsLayoutParams(sb);
			findAndSetAttributes(sb);
			v = sb;
			return v;
		} else if (getWidgetTyp() == WidgetTyp.Switch) {
			Switch sh = new Switch(c);
			// tb.setId(Objectid);
			setParentsLayoutParams(sh);
			findAndSetAttributes(sh);
			v = sh;
			return sh;
		} else if (getWidgetTyp() == WidgetTyp.ToggleButton) {
			tb = new ToggleButton(c);
			// tb.setId(Objectid);
			setParentsLayoutParams(tb);
			findAndSetAttributes(tb);
			if (!isEmpty())
				tb.setText(getObjectsContent());
			v = tb;
			return v;
		} else if (getWidgetTyp() == WidgetTyp.RatingBar) {
			rb = new RatingBar(c);
			// rb.setId(Objectid);
			setParentsLayoutParams(rb);
			findAndSetAttributes(rb);
			v = rb;
			return v;
		} else if (getWidgetTyp() == WidgetTyp.TimerPicker) {
			tp = new TimePicker(c);

			setParentsLayoutParams(tp);
			findAndSetAttributes(tp);
			v = tp;
			return v;
		}

		else if (getWidgetTyp() == WidgetTyp.AnalogClock) {
			ac = new AnalogClock(c);
			// ac.setId(Objectid);
			setParentsLayoutParams(ac);
			findAndSetAttributes(ac);
			v = ac;
			return v;
		} else if (getWidgetTyp() == WidgetTyp.Spinner) {
			Spinner sp = new Spinner(c);
			// sp.setId(Objectid);
			setParentsLayoutParams(sp);
			findAndSetAttributes(sp);
			v = sp;
			return v;
		} else if (getWidgetTyp() == WidgetTyp.Button) {
			Button b = new Button(c);
			for (int i = 0; i < attribute.length; i++) {
				if (attribute[i].equals("style")) {
					if (value[i].equals("?android:attr/buttonBarButtonStyle")) {
						LayoutInflater inflater = (LayoutInflater) c
								.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
						b = (Button) inflater.inflate(R.layout.barbutton, null);
					}
				}
			}
			// b.setId(Objectid);
			setParentsLayoutParams(b);
			findAndSetAttributes(b);
			if (!isEmpty())
				b.setText(getObjectsContent());
			v = b;
			return v;
		} else if (getWidgetTyp() == WidgetTyp.CheckBox) {
			cb = new CheckBox(c);
			// cb.setId(Objectid);
			setParentsLayoutParams(cb);

			findAndSetAttributes(cb);
			if (!isEmpty())
				cb.setText(getObjectsContent());
			v = cb;
			return v;
		} else if (getWidgetTyp() == WidgetTyp.EditText) {
			et = new EditText(c);
			// et.setId(Objectid);
			setParentsLayoutParams(et);

			findAndSetAttributes(et);
			if (!isEmpty())
				et.setText(getObjectsContent());
			v = et;
			return v;
		} else if (getWidgetTyp() == WidgetTyp.ImageView) {
			iv = new ImageView(c);
			// iv.setId(Objectid);
			setParentsLayoutParams(iv);
			// iv.setImageResource(R.drawable.easygui);
			findAndSetAttributes(iv);
			iv.setImageDrawable(c.getResources().getDrawable(R.drawable.image));
			v = iv;
			return v;
		}

		else if (getWidgetTyp() == WidgetTyp.ProgressBar) {
			ProgressBar pb = new ProgressBar(c);
			// pb.setId(Objectid);
			setParentsLayoutParams(pb);

			findAndSetAttributes(pb);
			v = pb;
			return v;

		} else if (getWidgetTyp() == WidgetTyp.DatePicker) {
			DatePicker dp = new DatePicker(c);
			// iv.setId(Objectid);
			// iv.setImageResource(R.drawable.calendar);
			setParentsLayoutParams(dp);
			// iv.setFocusableInTouchMode(true);

			findAndSetAttributes(dp);
			v = dp;
			return v;
		} else if (getWidgetTyp() == WidgetTyp.TextView) {
			tv = new TextView(c);
			// tv.setId(Objectid);
			setParentsLayoutParams(tv);

			findAndSetAttributes(tv);
			tv.setText(getObjectsContent());
			v = tv;
			return v;
		} else if (getWidgetTyp() == WidgetTyp.RadioButton) {
			rd = new RadioButton(c);
			// rd.setId(Objectid);
			setParentsLayoutParams(rd);
			findAndSetAttributes(rd);
			if (!isEmpty())
				rd.setText(getObjectsContent());
			v = rd;
			return v;
		} else if (getWidgetTyp() == WidgetTyp.ListView) {
			lv = new ListView(c);

			setParentsLayoutParams(lv);
			findAndSetAttributes(lv);
			v = lv;
			return v;
		} else if (getWidgetTyp() == WidgetTyp.ExpandableListView) {
			ExpandableListView elv = new ExpandableListView(c);

			setParentsLayoutParams(elv);
			findAndSetAttributes(elv);
			v = elv;
			return v;
		} else if (getWidgetTyp() == WidgetTyp.ViewStub) {
			ViewStub vs = new ViewStub(c);

			setParentsLayoutParams(vs);
			findAndSetAttributes(vs);
			v = vs;
			return v;
		} else if (getWidgetTyp() == WidgetTyp.ImageButton) {
			ImageButton ib = new ImageButton(c);

			setParentsLayoutParams(ib);
			findAndSetAttributes(ib);
			ib.setImageDrawable(c.getResources().getDrawable(R.drawable.image));
			v = ib;
			return v;
		} else if (getWidgetTyp() == WidgetTyp.NumberPicker) {
			NumberPicker np = new NumberPicker(c);

			setParentsLayoutParams(np);
			findAndSetAttributes(np);
			v = np;
			return v;
		} else if (getWidgetTyp() == WidgetTyp.BarButton) {
			Button bb = new Button(c);

			setParentsLayoutParams(bb);
			findAndSetAttributes(bb);
			v = bb;
			return v;
		} else
			return null;
	}

	@SuppressLint("NewApi")
	@TargetApi(14)
	public ViewGroup makeLayout(Context c) {
		getLayoutParams();
		switch (getLayoutTyp()) {
		case LinearLayout: {
			LinearLayout ll = new LinearLayout(c);
			for (int i = 0; i < attribute.length; i++) {
				if (attribute[i].equals("style")) {
					if (value[i].equals("?android:attr/buttonBarStyle")) {
						ll = new LinearLayout(c, null,
								android.R.attr.buttonBarStyle);
					}
				}
			}
			// ll.setId(Objectid);
			setParentsLayoutParams(ll);
			// ll.setLayoutParams(new LinearLayout.LayoutParams(lp[0],lp[1]));
			// ll.setBackgroundColor(Color.CYAN);
			// setLayoutAttributes(ll);
			findAndSetAttributes(ll);
			return ll;
		}
		case FrameLayout: {
			FrameLayout fl = new FrameLayout(c);
			// fl.setId(Objectid);
			setParentsLayoutParams(fl);
			// fl.setLayoutParams(new FrameLayout.LayoutParams(lp[0],lp[1]));
			findAndSetAttributes(fl);
			return fl;
		}
		/*
		 * case ButtonBar: { LinearLayout ll = new LinearLayout(c, null,
		 * android.R.attr.buttonBarStyle);
		 * 
		 * setParentsLayoutParams(ll); findAndSetAttributes(ll); return ll; }
		 */
		case RelativeLayout: {
			RelativeLayout rl = new RelativeLayout(c);
			// rl.setId(Objectid);
			// rl.setBackgroundColor(Color.RED);
			setParentsLayoutParams(rl);
			// rl.setLayoutParams(new RelativeLayout.LayoutParams(lp[0],lp[1]));
			// setLayoutAttributes(rl);
			findAndSetAttributes(rl);
			return rl;
		}
		case GridLayout: {
			GridLayout gl = new GridLayout(c);
			// gl.setId(Objectid);
			setParentsLayoutParams(gl);
			// gl.setLayoutParams(new GridLayout.LayoutParams());
			findAndSetAttributes(gl);
			return gl;
		}
		case TableLayout: {
			TableLayout tl = new TableLayout(c);
			// tl.setId(Objectid);
			// tl.setBackgroundColor(Color.RED);
			setParentsLayoutParams(tl);
			// tl.setLayoutParams(new TableLayout.LayoutParams(lp[0],lp[1]));
			findAndSetAttributes(tl);
			return tl;
		}
		case TableRow: {
			TableRow tr = new TableRow(c);
			// tr.setId(Objectid);
			// tr.setBackgroundColor(Color.RED);
			// tr.setEnabled(true);
			tr.setVisibility(View.VISIBLE);
			// tr.setX(50);
			setParentsLayoutParams(tr);
			// tr.setLayoutParams(new TableRow.LayoutParams(lp[0], lp[1]));
			findAndSetAttributes(tr);
			return tr;
		}
		case ScrollView: {
			ScrollView sv = new ScrollView(c);

			setParentsLayoutParams(sv);
			findAndSetAttributes(sv);
			return sv;
		}
		case RadioGroup: {
			RadioGroup rg = new RadioGroup(c);

			setParentsLayoutParams(rg);
			findAndSetAttributes(rg);
			return rg;
		}
		case AbsoluteLayout: {
			AbsoluteLayout al = new AbsoluteLayout(c);

			setParentsLayoutParams(al);
			findAndSetAttributes(al);
			return al;
		}
		case HorizontalScrollView: {
			HorizontalScrollView hsv = new HorizontalScrollView(c);

			setParentsLayoutParams(hsv);
			findAndSetAttributes(hsv);
			return hsv;
		}
		case DrawerLayout: {
			LinearLayout drawerLayout = new LinearLayout(c);
			setParentsLayoutParams(drawerLayout);
			findAndSetAttributes(drawerLayout);

			return drawerLayout;
		}
		case ViewPager: {
			LinearLayout viewPager = new LinearLayout(c);
			setParentsLayoutParams(viewPager);
			findAndSetAttributes(viewPager);

			return viewPager;
		}
		case PagerTitleStrip: {
			LinearLayout pagerTitle = new LinearLayout(c);
			setParentsLayoutParams(pagerTitle);
			findAndSetAttributes(pagerTitle);

			return pagerTitle;
		}
		default:
			return null;
		}
	}

}
