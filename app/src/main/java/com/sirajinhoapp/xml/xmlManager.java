package com.sirajinhoapp.xml;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.xml.sax.*;
import org.w3c.dom.*;

import com.mycompany.easyGUI.MainActivity;

import javax.xml.parsers.*;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;

public class xmlManager implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8929370360673297452L;
	// Speicher Objekte mit Bezeichner und Attributsanzahl
	public ArrayList<String> objects;
	// Speichert Attribute mit values
	public ArrayList<String[]> attributes;

	public ArrayList<String[]> values;

	@SuppressLint("UseSparseArrays")
	public HashMap<Integer, Integer> parent = new HashMap<Integer, Integer>();
	// public SparseIntArray parent = new SparseIntArray();

	public transient NodeList ndList;
	// Element element;
	public String Filename;
	public String tmpFilename;
	public String File;
	public transient Document document;
	// AttributeSet XMLattributes;
	public Boolean empty;
	String text;

	public Integer viewStart;
	public Integer viewCounter = 1;
	public HashMap<String, Integer> viewIds;

	public xmlManager(String FileName) {
		try {
			Filename = FileName;
			File = new File(FileName).getName();
			tmpFilename = MainActivity.cacheDir + "/" + File + ".tmp";
			empty = false;

			copyFile(Filename, tmpFilename);

			objects = new ArrayList<String>();
			attributes = new ArrayList<String[]>();
			values = new ArrayList<String[]>();
			viewStart = MainActivity.FilesOpend + 1 * 1000;
			viewIds = new HashMap<String, Integer>();

			// copyFile(Filename, tmpFilename);
			DocumentBuilderFactory factory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			/*
			 * if(text.contains("UTF-8")) { InputStream is = new
			 * ByteArrayInputStream(text.getBytes("UTF-8")); document =
			 * builder.parse(is);
			 * 
			 * } else
			 */

			document = builder.parse(new File(Filename));
			// document = builder.parse(new File(Filename));
			// ---- Get list of nodes to given element tag name ----
			ndList = document.getElementsByTagName("*");

			getStringObjects(ndList);
			MainActivity.PARSE_ERROR.clear();

		} catch (SAXParseException spe) {
			System.out.println("\n** Parsing error, line "
					+ spe.getLineNumber() + ", uri " + spe.getSystemId());
			System.out.println("   " + spe.getMessage());
			String error = "Parsing error at line " + spe.getLineNumber()
					+ " : \n" + "Message : " + spe.getMessage();
			MainActivity.PARSE_ERROR.clear();
			MainActivity.PARSE_ERROR.add(error);
			Exception e = (spe.getException() != null) ? spe.getException()
					: spe;
			e.printStackTrace();
		} catch (SAXException sxe) {
			Exception e = (sxe.getException() != null) ? sxe.getException()
					: sxe;
			e.printStackTrace();
		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (ArrayIndexOutOfBoundsException e) {
			e.printStackTrace();
		} catch (org.w3c.dom.DOMException e) {
			e.printStackTrace();
		}
		;

	}

	/*
	 * public xmlManager(Parcel in) { // this(Filename); objects = new
	 * ArrayList<String>(); attributes = new ArrayList<String[]>(); values = new
	 * ArrayList<String[]>(); viewStart = MainActivity.FilesOpend + 1 * 1000;
	 * viewIds = new HashMap<String, Integer>();
	 * 
	 * empty = true;
	 * 
	 * readFromParcel(in); }
	 */

	public xmlManager() {
		objects = new ArrayList<String>();
		attributes = new ArrayList<String[]>();
		values = new ArrayList<String[]>();
		viewStart = MainActivity.FilesOpend + 1 * 1000;
		viewIds = new HashMap<String, Integer>();

		empty = true;
	}

	public void copyFile(String srFile, String dtFile) {
		InputStream inStream = null;
		OutputStream outStream = null;

		try {

			File afile = new File(srFile);
			File bfile = new File(dtFile);

			if (afile.exists()) {
				inStream = new FileInputStream(afile);
				outStream = new FileOutputStream(bfile);

				byte[] buffer = new byte[1024];

				int length;
				// copy the file content in bytes
				while ((length = inStream.read(buffer)) > 0) {

					outStream.write(buffer, 0, length);

				}

				inStream.close();
				outStream.close();

				System.out.println("File is copied successful!");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void reloadDocument() throws ParserConfigurationException {
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			document = builder.parse(new File(tmpFilename));
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (org.w3c.dom.DOMException e) {
			e.printStackTrace();
		}

		// ---- Get list of nodes to given element tag name ----
		try {
			ndList = document.getElementsByTagName("*");
		} catch (org.w3c.dom.DOMException e) {
			e.printStackTrace();
		} catch (ArrayIndexOutOfBoundsException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
		;

		clearStrings();
		getStringObjects(ndList);
	}

	public void getStringObjects(NodeList ndList) throws NullPointerException {
		String[] as;
		String[] vs;

		clearStrings();
		if (ndList != null) {
			for (int i = 0; i < ndList.getLength(); i++) {
				// pem.put(i,
				// ndList.item(i).getNodeType()+" | "+ndList.item(i).getNodeValue());
				try {
					Node p = ndList.item(i).getParentNode();
					if (p != null) {
						for (int j = 0; j < ndList.getLength(); j++) {
							if (ndList.item(j) == p) {
								parent.put(i, j);
								break;
							}
						}
					}

					objects.add(i, ndList.item(i).getNodeName());
					as = new String[ndList.item(i).getAttributes().getLength()];
					vs = new String[ndList.item(i).getAttributes().getLength()];

					for (int j = 0; j < ndList.item(i).getAttributes()
							.getLength(); j++) {
						as[j] = ndList.item(i).getAttributes().item(j)
								.getNodeName();
						vs[j] = ndList.item(i).getAttributes().item(j)
								.getNodeValue();
						// attributes.g =
						// ndList.item(i).getAttributes().item(j).getNodeName();
						// values[i][j] =
						// ndList.item(i).getAttributes().item(j).getNodeValue();
						// attributes.s
					}
					attributes.add(i, as);
					values.add(i, vs);

					/*
					 * if(isLayoutTyp(ndList.item(i).getNodeName())) { NodeList
					 * childs = ndList.item(i).getChildNodes(); for(int k = 0; k
					 * < childs.getLength(); k++) { parent.put(i+k, i); } }
					 */
				} catch (org.w3c.dom.DOMException e) {
					e.printStackTrace();
				} catch (ArrayIndexOutOfBoundsException e) {
					e.printStackTrace();
				}
				;
			}
		}

	}

	public Boolean isLayoutTyp(String object) {
		if (object.equals("LinearLayout"))
			return true;
		else if (object.equals("FrameLayout"))
			return true;
		else if (object.equals("RelativeLayout"))
			return true;
		else if (object.equals("TableLayout"))
			return true;
		else if (object.equals("GridLayout"))
			return true;
		else if (object.equals("TableRow"))
			return true;
		else
			return false;
	}

	public void addGuiNode(Node element, int id) {
		Node node = document.getElementsByTagName("*").item(id);

		if (node != null) {
			if (hasChild(node)) {

				if (id == objects.size() - 1)
					document.getElementsByTagName("*").item(id)
							.appendChild(element);
				else if (node.getNodeName().equals("ScrollView")
						|| node.getNodeName().equals("HorizontalScrollView"))
					return;
				else {
					try {
						document.getElementsByTagName("*")
								.item(id)
								.insertBefore(
										element,
										document.getElementsByTagName("*")
												.item(id + 1));

					} catch (org.w3c.dom.DOMException e) {
						System.out.println("Error : " + e.getCause()
								+ " Message " + e.code);
						e.printStackTrace();
					}
				}
			} else {
				if (id == objects.size() - 1) {
					if (node.getNodeName().contains("Layout")
							|| node.getNodeName().contains("Row")
							|| node.getNodeName().equals("ViewPager")
							|| node.getNodeName().contains("Scroll")
							|| node.getNodeName().equals("RadioGroup")
							|| node.getNodeName().equals("ButtonBar"))

						node.appendChild(element);
					else
						node.getParentNode().appendChild(element);

				}
				// else
				// if(node.getParentNode().getNodeName().equals("ScrollView"))
				// return;
				else if (node.getParentNode() != document.getElementsByTagName(
						"*").item(parent.get(id + 1)))
					node.getParentNode().appendChild(element);
				else if (node.getNodeName().contains("Layout")
						|| node.getNodeName().contains("Row")
						|| node.getNodeName().equals("ViewPager")
						|| node.getNodeName().contains("Scroll")
						|| node.getNodeName().equals("RadioGroup"))
					node.appendChild(element);
				else {
					if (document.getElementsByTagName("*").item(id + 1) != null)
						node.getParentNode()
								.insertBefore(
										element,
										document.getElementsByTagName("*")
												.item(id + 1));
					else
						node.getParentNode().insertBefore(element,
								document.getElementsByTagName("*").item(id));

				}
			}
			ndList = document.getElementsByTagName("*");
			getStringObjects(ndList);
		}
	}

	public void addNode(int id, String typ) {
		// ndList.item(id).appendChild(document.createElement("Button"));
		Boolean setAttributes = false;
		Element element;
		try {
			if (document == null) {
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory
						.newInstance();
				DocumentBuilder dBuilder = null;
				try {
					dBuilder = dbFactory.newDocumentBuilder();
				} catch (ParserConfigurationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (dBuilder != null) {
					document = dBuilder.newDocument();
				}
			}

			if (typ.equals("Button"))
				element = document.createElement("Button");
			else if (typ.equals("ImageButton"))
				element = document.createElement("ImageButton");
			else if (typ.equals("Small Button")) {
				element = document.createElement("Button");
				setAttr(element);
				element.setAttribute("style", "?android:attr/buttonStyleSmall");
				setAttributes = true;
			} else if (typ.equals("BarButton")) {
				element = document.createElement("Button");
				setAttr(element);
				element.setAttribute("style",
						"?android:attr/buttonBarButtonStyle");
				setAttributes = true;
			} else if (typ.equals("ButtonBar")) {
				element = document.createElement("LinearLayout");
				setAttr(element);
				element.setAttribute("style", "?android:attr/buttonBarStyle");
				setAttributes = true;
			} else if (typ.equals("EditText"))
				element = document.createElement("EditText");
			else if (typ.equals("Text"))
				element = document.createElement("TextView");
			else if (typ.equals("SmallText")) {
				element = document.createElement("TextView");
				setAttr(element);
				element.setAttribute("android:textAppearance",
						"?android:attr/textAppearanceSmall");
				setAttributes = true;
			} else if (typ.equals("MediumText")) {
				element = document.createElement("TextView");
				setAttr(element);
				element.setAttribute("android:textAppearance",
						"?android:attr/textAppearanceMedium");
				setAttributes = true;
			} else if (typ.equals("LargeText")) {
				element = document.createElement("TextView");
				setAttr(element);
				element.setAttribute("android:textAppearance",
						"?android:attr/textAppearanceLarge");
				setAttributes = true;
			} else if (typ.equals("RadioButton"))
				element = document.createElement("RadioButton");
			else if (typ.equals("CheckBox"))
				element = document.createElement("CheckBox");
			else if (typ.equals("SeekBar"))
				element = document.createElement("SeekBar");
			else if (typ.equals("ProgressBar"))
				element = document.createElement("ProgressBar");
			else if (typ.equals("CalendarView"))
				element = document.createElement("CalendarView");
			else if (typ.equals("AnalogClock"))
				element = document.createElement("AnalogClock");
			else if (typ.equals("RatingBar"))
				element = document.createElement("RatingBar");
			else if (typ.equals("ImageView"))
				element = document.createElement("ImageView");
			else if (typ.equals("ToggleButton"))
				element = document.createElement("ToggleButton");
			else if (typ.equals("Spinner"))
				element = document.createElement("Spinner");
			else if (typ.equals("DatePicker"))
				element = document.createElement("DatePicker");
			else if (typ.equals("LinearLayout (horizontal)")) {
				element = document.createElement("LinearLayout");
				element.setAttribute("android:id", " ");
				element.setAttribute("android:layout_width", "wrap_content");
				element.setAttribute("android:layout_height", "wrap_content");
				element.setAttribute("android:orientation", "horizontal");
				setAttributes = true;
			} else if (typ.equals("LinearLayout (vertical)")) {
				element = document.createElement("LinearLayout");
				element.setAttribute("android:id", " ");
				element.setAttribute("android:layout_width", "wrap_content");
				element.setAttribute("android:layout_height", "wrap_content");
				element.setAttribute("android:orientation", "vertical");
				setAttributes = true;
			} else if (typ.equals("RelativeLayout"))
				element = document.createElement("RelativeLayout");
			else if (typ.equals("GridLayout"))
				element = document.createElement("GridLayout");
			else if (typ.equals("FrameLayout"))
				element = document.createElement("FrameLayout");
			else if (typ.equals("TableLayout"))
				element = document.createElement("TableLayout");
			else if (typ.equals("AbsoluteLayout"))
				element = document.createElement("AbsoluteLayout");
			else if (typ.equals("TableRow"))
				element = document.createElement("TableRow");
			else if (typ.equals("ImageView"))
				element = document.createElement("ImageView");
			else if (typ.equals("Switch"))
				element = document.createElement("Switch");
			else if (typ.equals("TimePicker"))
				element = document.createElement("TimePicker");
			else if (typ.equals("RatingBar"))
				element = document.createElement("RatingBar");
			else if (typ.equals("NumberPicker"))
				element = document.createElement("NumberPicker");
			else if (typ.equals("ListView"))
				element = document.createElement("ListView");
			else if (typ.equals("ExpandableListView"))
				element = document.createElement("ExpandableListView");
			else if (typ.equals("RadioGroup"))
				element = document.createElement("RadioGroup");
			else if (typ.equals("ScrollView"))
				element = document.createElement("ScrollView");
			else if (typ.equals("Switch"))
				element = document.createElement("Switch");
			else if (typ.equals("ButtonBar"))
				element = document.createElement("ButtonBar");
			else if (typ.equals("HorizontalScrollView"))
				element = document.createElement("HorizontalScrollView");
			else if (typ.equals("DrawerLayout"))
				element = document
						.createElement("android.support.v4.widget.DrawerLayout");
			else if (typ.equals("ViewPager"))
				element = document
						.createElement("android.support.v4.view.ViewPager");
			else if (typ.equals("PagerTitleStrip"))
				element = document
						.createElement("android.support.v4.view.PagerTitleStrip");
			else
				element = document.createElement("View");

			if (element != null && setAttributes == false) {
				setAttr(element);
			}

			if (id == -1) {
				document.appendChild(element);

				ndList = document.getElementsByTagName("*");
				getStringObjects(ndList);
				return;
			}

			Node node = document.getElementsByTagName("*").item(id);
			if (node != null) {
				if (hasChild(node)) {

					if (id == objects.size() - 1)
						document.getElementsByTagName("*").item(id)
								.appendChild(element);
					else if (node.getNodeName().equals("ScrollView")
							|| node.getNodeName()
									.equals("HorizontalScrollView"))
						return;
					else {
						try {
							document.getElementsByTagName("*")
									.item(id)
									.insertBefore(
											element,
											document.getElementsByTagName("*")
													.item(id + 1));

						} catch (org.w3c.dom.DOMException e) {
							System.out.println("Error : " + e.getCause()
									+ " Message " + e.code);
							e.printStackTrace();
						}
					}
				} else {
					if (id == objects.size() - 1) {
						if (node.getNodeName().contains("Layout")
								|| node.getNodeName().contains("Row")
								|| node.getNodeName().equals("ViewPager")
								|| node.getNodeName().contains("Scroll")
								|| node.getNodeName().equals("RadioGroup")
								|| node.getNodeName().equals("ButtonBar"))

							node.appendChild(element);
						else
							node.getParentNode().appendChild(element);

					}
					// else
					// if(node.getParentNode().getNodeName().equals("ScrollView"))
					// return;
					else if (node.getParentNode() != document
							.getElementsByTagName("*").item(parent.get(id + 1)))
						node.getParentNode().appendChild(element);
					else if (node.getNodeName().contains("Layout")
							|| node.getNodeName().contains("Row")
							|| node.getNodeName().equals("ViewPager")
							|| node.getNodeName().contains("Scroll")
							|| node.getNodeName().equals("RadioGroup"))
						node.appendChild(element);
					else
						node.getParentNode()
								.insertBefore(
										element,
										document.getElementsByTagName("*")
												.item(id + 1));
				}
				ndList = document.getElementsByTagName("*");
				getStringObjects(ndList);
			}

		} catch (org.w3c.dom.DOMException e) {
			System.out.println("Error : " + e.getCause() + " Message "
					+ e.getMessage());
			e.printStackTrace();
		} catch (ArrayIndexOutOfBoundsException e) {
			e.printStackTrace();
		}

	}

	public Boolean hasChild(Node node) {
		NodeList childs;
		if (node.hasChildNodes()) {
			childs = node.getChildNodes();
			for (int i = 0; i < childs.getLength(); i++) {
				if (childs.item(i).getNodeType() == Node.ELEMENT_NODE)
					return true;
			}
		}
		return false;
	}

	public void surroundElement(int id, String typ) {
		Element element = null;
		Boolean setAttributes = false;

		if (typ.equals("LinearLayout (horizontal)")) {
			element = document.createElement("LinearLayout");
			element.setAttribute("android:id", " ");
			element.setAttribute("android:layout_width", "wrap_content");
			element.setAttribute("android:layout_height", "wrap_content");
			element.setAttribute("android:orientation", "horizontal");
			setAttributes = true;
		} else if (typ.equals("LinearLayout (vertical)")) {
			element = document.createElement("LinearLayout");
			element.setAttribute("android:id", " ");
			element.setAttribute("android:layout_width", "wrap_content");
			element.setAttribute("android:layout_height", "wrap_content");
			element.setAttribute("android:orientation", "vertical");
			setAttributes = true;
		} else if (typ.equals("RelativeLayout"))
			element = document.createElement("RelativeLayout");
		else if (typ.equals("GridLayout"))
			element = document.createElement("GridLayout");
		else if (typ.equals("FrameLayout"))
			element = document.createElement("FrameLayout");
		else if (typ.equals("TableLayout"))
			element = document.createElement("TableLayout");
		else if (typ.equals("AbsoluteLayout"))
			element = document.createElement("AbsoluteLayout");
		else if (typ.equals("TableRow"))
			element = document.createElement("TableRow");
		else if (typ.equals("ScrollView"))
			element = document.createElement("ScrollView");
		else if (typ.equals("RadioGroup"))
			element = document.createElement("RadioGroup");
		else if (typ.equals("HorizontalScrollView"))
			element = document.createElement("HorizontalScrollView");

		if (setAttributes == false)
			setAttr(element);

		Node node = document.getElementsByTagName("*").item(id);

		if (element != null && node != null) {
			element.appendChild(node);

			// removeObject(id);
			if (id == 0) {
				document.appendChild(element);
				ndList = document.getElementsByTagName("*");
				getStringObjects(ndList);
			} else
				addGuiNode(element, id - 1);

		} else
			System.out.println("element null");

	}

	public void setAttr(Element element) {
		element.setAttribute("android:id", " ");
		element.setAttribute("android:layout_width", "wrap_content");
		element.setAttribute("android:layout_height", "wrap_content");
		// element.setAttribute("android:text", "");
	}

	public String getStringParent(int id) {
		String parentString = null;
		try {
			parentString = document.getElementsByTagName("*")
					.item(parent.get(id) == null ? 0 : parent.get(id))
					.getNodeName();
		} catch (org.w3c.dom.DOMException e) {
			e.printStackTrace();
		}
		;
		if (parentString != null)
			return parentString;
		else
			return parentString;
	}

	public Node getNodeFromDocument(int id) {
		if (id > document.getElementsByTagName("*").getLength())
			return null;
		return document.getElementsByTagName("*").item(id);
	}

	public void removeObject(int id) {
		Element element = (Element) document.getElementsByTagName("*").item(id);

		document.getElementsByTagName("*").item(id).getParentNode()
				.removeChild(element);

		// MainActivity.xm.saveTmpFile(editorActivity.content);
		/*
		 * 
		 * try { reloadDocument(); } catch (ParserConfigurationException e) TODO
		 * Auto-generated catch block e.printStackTrace();
		 */
		ndList = document.getElementsByTagName("*");
		getStringObjects(ndList);

	}

	public void clearStrings() {
		objects.clear();
		attributes.clear();
		values.clear();
		parent.clear();
	}

	public void changeNodeAttribute(int id, String attribute, String value) {
		Element element = (Element) document.getElementsByTagName("*").item(id);
		if (element != null) {
			try {
				element.setAttribute(attribute, value);
			} catch (org.w3c.dom.DOMException e) {
				System.out.println("Error Code : " + e.code);
			}
		}
		clearStrings();
		getStringObjects(ndList);

	}

	public void deleteFile(String Filename) {
		File file = new File(Filename);
		if (file.exists())
			file.delete();
		clearStrings();
	}

	public String getXMLInput() {
		StreamResult re = new StreamResult();
		Transformer transformer;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DOMSource source = new DOMSource(document);
		String result = "";

		// if (!empty) {
		try {
			transformer = TransformerFactory.newInstance().newTransformer();

			re.setOutputStream(baos);
			transformer.transform(source, re);
		} catch (TransformerConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerFactoryConfigurationError e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ArrayIndexOutOfBoundsException e) {
			e.printStackTrace();
		}
		;

		try {
			baos.write("\n".getBytes());
			result = baos.toString();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// }

		return result;
	}

	public void saveNodeModification(String Filename) {
		try {
			File file = new File(Filename);
			file.createNewFile();

			Transformer transformer = TransformerFactory.newInstance()
					.newTransformer();
			DOMSource source = new DOMSource(document);
			FileOutputStream os;
			os = new FileOutputStream(file);

			StreamResult result = new StreamResult(os);
			transformer.transform(source, result);

		} catch (TransformerConfigurationException tce) {
			System.out.println("\n** Transformer Factory error");
			System.out.println("   " + tce.getMessage());
			Throwable e = (tce.getException() != null) ? tce.getException()
					: tce;
			e.printStackTrace();
		} catch (TransformerException tfe) {
			System.out.println("\n** Transformation error");
			System.out.println("   " + tfe.getMessage());
			Throwable e = (tfe.getException() != null) ? tfe.getException()
					: tfe;
			e.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}

	public void getDocumentAndNodeList() {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		MainActivity.PARSE_ERROR.clear();

		try {
			builder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
		try {
			// System.out.println("File :" + loadUTF8(tmpFilename));
			File file = new File(this.tmpFilename);
			document = builder.parse(file);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SAXParseException spe) {
			System.out.println("\n** Parsing getDoc error, line "
					+ spe.getLineNumber() + ", uri " + spe.getSystemId());
			System.out.println("   " + spe.getMessage());
			String error = "Parsing error at line " + spe.getLineNumber()
					+ " : \n" + "Message : " + spe.getMessage();
			MainActivity.PARSE_ERROR.clear();
			MainActivity.PARSE_ERROR.add(error);

			Exception e = (spe.getException() != null) ? spe.getException()
					: spe;
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (org.w3c.dom.DOMException e) {
			e.printStackTrace();
		}

		// ---- Get list of nodes to given element tag name ----
		if (document != null) {
			ndList = document.getElementsByTagName("*");

			clearStrings();
			getStringObjects(ndList);
		} else {
			System.out.println("Fehler document");
		}

	}

	public void saveFile(String Filename, String str) {
		/*
		 * File newTextFile = new File(tmpFilename);
		 * newTextFile.createNewFile();
		 * 
		 * FileWriter fw = new FileWriter(newTextFile); fw.write(str);
		 * fw.flush(); fw.close();
		 */
		PrintWriter writer = null;
		try {
			writer = new PrintWriter(Filename, "UTF-8");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (writer != null) {
			writer.print(str);
			writer.close();
		}
		// System.out.println("File existed!");
		// System.out.println("ndlist empty "+tmpFilename);

	}

	/*
	 * @Override public int describeContents() { return 0; }
	 * 
	 * public static final Parcelable.Creator<xmlManager> CREATOR = new
	 * Parcelable.Creator<xmlManager>() {
	 * 
	 * public xmlManager createFromParcel(Parcel in) { return new
	 * xmlManager(in); }
	 * 
	 * @Override public xmlManager[] newArray(int size) { // TODO Auto-generated
	 * method stub return new xmlManager[size]; }
	 * 
	 * };
	 * 
	 * @Override public void writeToParcel(Parcel dest, int flags) {
	 * List<String[]> attr = new ArrayList<String[]>(); List<String[]> val = new
	 * ArrayList<String[]>(); List<String> obj = new ArrayList<String>();
	 * 
	 * if (objects != null && attributes != null && values != null) { for (int i
	 * = 0; i <= objects.size() - 1; i++) { obj.add(i, objects.get(i)); } for
	 * (int i = 0; i <= attributes.size() - 1; i++) { attr.add(i,
	 * attributes.get(i)); } for (int i = 0; i <= values.size() - 1; i++) {
	 * val.add(i, values.get(i)); }
	 * 
	 * dest.writeList(obj); dest.writeList(attr); dest.writeList(val);
	 * dest.writeMap(parent); // dest.writeValue(document);
	 * dest.writeString(Filename); dest.writeString(tmpFilename); if(empty ==
	 * true) dest.writeInt(0); else dest.writeInt(1); }
	 * 
	 * }
	 * 
	 * public void readFromParcel(Parcel in) { List<String[]> attr = new
	 * ArrayList<String[]>(); List<String[]> val = new ArrayList<String[]>();
	 * List<String> obj = new ArrayList<String>();
	 * 
	 * // xmlManager xmlmanager = new xmlManager(in.readString());
	 * 
	 * in.readList(obj, null); in.readList(attr, null); in.readList(val, null);
	 * in.readMap(parent, null); this.Filename = in.readString();
	 * this.tmpFilename = in.readString(); this.File = new
	 * File(this.Filename).getName(); int empty = in.readInt(); if(empty == 0)
	 * this.empty = true; else this.empty = false;
	 * 
	 * if (objects != null && attributes != null && values != null) { for (int i
	 * = 0; i <= obj.size() - 1; i++) { if (this.objects != null)
	 * this.objects.add(i, obj.get(i)); } for (int i = 0; i <= attr.size() - 1;
	 * i++) { if (this.attributes != null) this.attributes.add(i, attr.get(i));
	 * } for (int i = 0; i <= val.size() - 1; i++) { if (this.values != null)
	 * this.values.add(i, val.get(i)); } } // getDocumentAndNodeList(); }
	 */

	public String loadUTF8(String Filename) {
		String strLine[] = new String[500];
		String text = "";
		try {
			int i = 0;
			FileInputStream fstream = new FileInputStream(Filename);

			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));

			while ((strLine[i] = br.readLine()) != null) {
				text = text + "\n" + strLine[i];
				i++;
			}
			in.close();
		} catch (Exception file) {// Catch exception if any
			System.err.println("Error: " + file.getMessage());
		}

		return text;
	}

}
